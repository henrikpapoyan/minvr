<?php
__IncludeLang($_SERVER["DOCUMENT_ROOT"]."/local/lang/".LANGUAGE_ID."/init.php");
define('SITE_TEMPLATE_PATH_DEFAULT', '/local/templates/.default');
require_once($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/include/include.inc.php");
require_once($_SERVER["DOCUMENT_ROOT"]. '/local/php_interface/include/imageMagick.php');

$arLibs = array(
	$_SERVER['DOCUMENT_ROOT'].'/local/php_interface/include/defines.php',
	$_SERVER['DOCUMENT_ROOT'].'/local/php_interface/include/global-storage.php',
	$_SERVER['DOCUMENT_ROOT'].'/local/php_interface/include/tools.php',
	$_SERVER['DOCUMENT_ROOT'].'/local/php_interface/include/events.php',
);

foreach($arLibs as $lib){
	if(file_exists($lib)){
		require_once($lib);
	}
}

/********/
require_once $_SERVER['DOCUMENT_ROOT'].'/local/app/app.php';
require_once 'options/uf_element.php'; 

function rssFeed()
{
	file_put_contents($_SERVER['DOCUMENT_ROOT']. "/rss.txt",
					111 . "\n" .file_get_contents($_SERVER['DOCUMENT_ROOT']. "/rss.txt"));
	app::rssFeed();
	file_put_contents($_SERVER['DOCUMENT_ROOT']. "/rss.txt",
					222 . "\n" .file_get_contents($_SERVER['DOCUMENT_ROOT']. "/rss.txt"));
	return "rssFeed();";	
}

AddEventHandler('main', 'OnBeforeEventSend', Array("MyForm", "my_OnBeforeEventSend"));
class MyForm
{
   function my_OnBeforeEventSend(&$arFields, &$arTemplate, &$arParams)
   {
       if ( $arFields[RS_FORM_ID] == 3 || $arFields[RS_FORM_ID] == 6 )
	   {
		   CForm::GetResultAnswerArray($arFields[RS_FORM_ID], 
			$arrColumns, 
			$arrAnswers, 
			$arrAnswersVarname, 
			array("RESULT_ID" => $arFields[RS_RESULT_ID]));

			if( is_array($arrAnswersVarname[$arFields[RS_RESULT_ID]]))
			{
				foreach($arrAnswersVarname[$arFields[RS_RESULT_ID]] as $varCode => $varValue)
				{
					$tmpVar = current($varValue);
					if( $tmpVar[FIELD_TYPE] == "file" && $tmpVar[USER_FILE_ID] > 1 )
						$varReplace[$varCode] = current($varValue);
				}
			}



			 //$mess = $arTemplate["MESSAGE"];

			 foreach($varReplace as $keyField => $arField)
			 {
				$file = cfile::getfilearray($arField[USER_FILE_ID]);
				 $arFields[$keyField] = $file[ORIGINAL_NAME] ." - Файл: <a href=\"https://minvr.ru" . str_replace(" ", "%20", cfile::getpath($arField[USER_FILE_ID]) ."\">Скачать</a><br>");
			 }
	   
	   }
   }
}

CModule::IncludeModule("form");
function my_onAfterResultAdd($WEB_FORM_ID, $RESULT_ID)
{
  // действие обработчика распространяется только на форму с ID=6
  if ($WEB_FORM_ID == 3) 
  {
    // запишем в дополнительное поле 'user_ip' IP-адрес пользователя
    $arValues = CFormResult::GetDataByIDForHTML($RESULT_ID, "Y");
	$arFields= [
		"CLIENT_EMAIL" => $arValues['form_email_11'],
		"RESULT_ID" => $RESULT_ID
	];
	  CEvent::send("FORM_FILLING_SIMPLE_FORM_3", "s1", $arFields, "42");
  }
}

// зарегистрируем функцию как обработчик двух событий
AddEventHandler('form', 'onAfterResultAdd', 'my_onAfterResultAdd');

//Добавляем js в админ панель.
AddEventHandler('main', 'OnEpilog', 'addIblockPageJsHelper', 2);
function addIblockPageJsHelper()
{
    if (defined('ADMIN_SECTION') && $_SERVER['PHP_SELF'] == '/bitrix/admin/iblock_element_edit.php' && $_GET['IBLOCK_ID'] == 110)
    {
        \Bitrix\Main\Page\Asset::getInstance()->addJs('/local/templates/.default/js/admin_iblock.js?hash=' . md5(time()));
        \Bitrix\Main\Page\Asset::getInstance()->addJs('/local/templates/.default/js/tinymce/tinymce.min.js?hash=' . md5(time()));
    }
}


function drawCanvas() {
    global $APPLICATION;
    if (!$APPLICATION->GetPageProperty("canvas")) {
        return "<div class='canvas canvas--white'>";
    } else {
        return "<div class='canvas'>";
    }
}