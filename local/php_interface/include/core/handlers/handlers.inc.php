<?
require_once("handlers.admin.php");
require_once("handlers.main.php");
require_once("handlers.mail.php");
require_once("handlers.iblock.php");

class CALLEventHandlers
{
	public static function pushError($str)
	{
		$GLOBALS["APPLICATION"]->throwException(DFA_CORE_NAME.": ".$str);
		return false;
	}
}
?>