<?
AddEventHandler("main", "onAfterUserAdd",		array("CMailEventHandlers", "ClearUserToGroupsCache"));
AddEventHandler("main", "OnAfterUserUpdate",	array("CMailEventHandlers", "ClearUserToGroupsCache"));
AddEventHandler("main", "OnUserDelete",			array("CMailEventHandlers", "ClearUserToGroupsCache"));

AddEventHandler("main", "OnAfterGroupAdd",		array("CMailEventHandlers", "ClearUserToGroupsCache"));
AddEventHandler("main", "OnAfterGroupUpdate",	array("CMailEventHandlers", "ClearUserToGroupsCache"));
AddEventHandler("main", "OnGroupDelete",		array("CMailEventHandlers", "ClearUserToGroupsCache"));

AddEventHandler("main", "OnBeforeEventSend",	array("CMailEventHandlers", "OnBeforeEventSendHandler_AddUserGroupsField"), 1);
AddEventHandler("main", "OnBeforeEventSend",	array("CMailEventHandlers", "OnBeforeEventSendHandler"));

class CMailEventHandlers extends CALLEventHandlers
{
	public static function ClearUserToGroupsCache()
	{
		$cache = new CARTCache(array(), 86400, "mail_tools_SendIBlockItem_user_groups");
		$cache->clean();
	}

	public static function OnBeforeEventSendHandler_AddUserGroupsField(&$arFields, &$arTemplate)
	{
		$bFound = false;

		foreach (array("EMAIL_TO", "CC", "BCC") as $code)
		{
			if (preg_match("/\#GROUP_[0-9]+\#/", $arTemplate[$code]) > 0)
			{
				$bFound = true;
				break;
			}
		}

		if ($bFound)
		{
			$cache = new CARTCache(array(), 86400, "mail_tools_SendIBlockItem_user_groups");
			$arEventUserGroupsFields = $cache->Init();
			if(null == $arEventUserGroupsFields)
			{
				$arEventUserGroupsFields = array();

				$rsUserGroups = CGroup::GetList(
						$by = "c_sort",
						$order = "asc",
						array(
							"ACTIVE" => "Y",
							"ANONYMOUS" => "N"
						),
						"Y"
				);
				while ($resUserGroup = $rsUserGroups->Fetch())
				{
					$arEventUserGroupsFields["GROUP_".$resUserGroup["ID"]] = COption::GetOptionString("main", "email_from", "admin@".$GLOBALS["SERVER_NAME"]);

					if ($resUserGroup["USERS"] > 0)
					{
						$arGroupUsersIDs = CGroup::GetGroupUser($resUserGroup["ID"]);

						if (!empty($arGroupUsersIDs))
						{
							$rsGroupUsers = CUser::GetList(
								$by="id",
								$order="desc",
								array(
									"ACTIVE" => "Y",
									"ID" => implode(" | ", $arGroupUsersIDs)
								),
								array("FIELDS" => array("EMAIL"))
							);

							$arGroupUsers = array();
							while ($resGroupUser = $rsGroupUsers->Fetch())
							{
								if (!empty($resGroupUser["EMAIL"]))
								{
									$arGroupUsers[] = $resGroupUser["EMAIL"];
								}
							}

							if (!empty($arGroupUsers))
							{
								$arGroupUsers = array_unique($arGroupUsers);

								$arEventUserGroupsFields["GROUP_".$resUserGroup["ID"]] = implode(",", $arGroupUsers);
							}
						}
					}
				}

				$cache->set($arEventUserGroupsFields);
			}

			if (null != $arEventUserGroupsFields)
				$arFields = array_merge($arFields, $arEventUserGroupsFields);
		}
	}

	public static function OnBeforeEventSendHandler($___vars, &$___message)
	{
		foreach (array("EMAIL_FROM", "EMAIL_TO", "BCC", "CC", "REPLY_TO") as $field)
		{
			preg_match_all("/#([^\#]+)#/i", $___message[$field], $matches);

			if (!empty($matches[1]))
			{
				foreach ($matches[1] as $match)
				{
					if (!is_set($___vars, $match) || strlen(trim($___vars[$match])) == 0)
					{
						$___vars[$match] = $___vars["DEFAULT_EMAIL_FROM"];
					}
				}
			}
		}
		unset($field);

		if (strpos(implode(" ", $___message), "<?") === false)
			return true;

		$___display_errors = ini_get("display_errors");
		ini_set("display_errors", 0);
		$___vars = array_merge($___vars, CEvent::GetSiteFieldsArray($___message["LID"]));

		extract($___vars, EXTR_OVERWRITE, null);
		// Для поддержки MESSAGE_PHP
		$arParams = $___vars;

		foreach (array_keys($___message) as $key)
		{
			ob_start();

			if ($key == "MESSAGE_PHP" && defined("SM_VERSION") && version_compare(SM_VERSION, "15.0.11")<=0)
			{
				// будем надеяться, что в этой версии Битркис уже поправит баг с
				// последним перенесом в строке, стоящим после закрывающего php-тега
				// и уничтожающего этот перенос
				$___message[$key] = str_replace("?>\r\n", "?>\r\n\r\n", $___message[$key]);
			}

			if (false !== eval("?>".$___message[$key]."<?"))
			{
				$___message[$key] = ob_get_clean();
			}
			else
			{
				$___message[$key] = "";
				ob_clean();
			}

		}

		ini_set("display_errors", $___display_errors);
	}
}
?>