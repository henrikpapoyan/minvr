<?
AddEventHandler("main", "OnAdminCustomContextGenerate", array("CAdminEventHandlers_IBLOCK", "OnAdminCustomContextGenerate"), 1);

class CAdminEventHandlers
{
	public static function _GenerateMenuItem($name, $action, $icon = "copy", $title = false)
	{
		if ($title === false)
			$title = $name;

		$title = strip_tags($title);

		return array(
			"TEXT" => $name,
			"TITLE" => $title,
			"ACTION" => "javascript:".str_replace("\r\n", "", $action),
			"ICON" => $icon,
		);
	}
}

class CAdminEventHandlers_IBLOCK extends CAdminEventHandlers
{

	public static function OnAdminCustomContextGenerate($url)
	{
		$IBLOCK_ID = intval($_REQUEST["IBLOCK_ID"]);

		if ($IBLOCK_ID > 0 && in_array($url, array("iblock_list_admin.php")))
		{
			$strEventType = CIblockEventHandlers::$sendEventIBlockItemPrefix.$IBLOCK_ID;
			$bTemplateExists = CEventType::GetListEx(array(), array("TYPE_ID" => $strEventType))->SelectedRowsCount()>0;

			if ($bTemplateExists)
			{
				$href = "window.location='/bitrix/admin/type_edit.php?EVENT_NAME=".$strEventType."&lang=".LANGUAGE_ID."'";
			}
			else
			{

				IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/type_edit.php");

				if (CModule::IncludeModule("iblock"))
				{

					$rsIblock = CIBlock::GetById($IBLOCK_ID);

					$arFields = $arFieldsIblock = array();

					if ($resIblock = $rsIblock->GetNext())
					{

						foreach (array("ID", "CODE", "XML_ID", "ACTIVE", "NAME", "ACTIVE_FROM", "ACTIVE_TO", "CREATED_BY", "PREVIEW_TEXT", "DETAIL_TEXT", "TAGS") as $code)
						{
							$arFieldsIblock[$code] = GetMessage("IBLOCK_FIELD_".$code);
						}

						$arFields = $arFieldsIblock;

						$rsIblockProperties = CIBlock::GetProperties($IBLOCK_ID, array("SORT" => "ASC", array("ACTIVE" => "Y")));

						while ($resIblockProperties = $rsIblockProperties->GetNext())
						{
							$pid = $resIblockProperties["CODE"]?$resIblockProperties["CODE"]:$resIblockProperties["ID"];

							$arFields["PROP_".$pid] = $resIblockProperties["NAME"];

							if ($resIblockProperties["PROPERTY_TYPE"] == "E")
							{
								foreach ($arFieldsIblock as $code => $name)
								{
									$arFields["PROP_".$pid."_".$code] = $resIblockProperties["NAME"].": ".$name;
								}
								if (!empty($resIblockProperties["LINK_IBLOCK_ID"]))
								{
									$rsIblockPropertiesLink = CIBlock::GetProperties($resIblockProperties["LINK_IBLOCK_ID"], array("SORT" => "ASC", array("ACTIVE" => "Y")));

									while ($resIblockPropertiesLink = $rsIblockPropertiesLink->GetNext())
									{
										$arFields["PROP_".$pid."_PROP_".$resIblockPropertiesLink["CODE"]] = $resIblockProperties["NAME"].": ".$resIblockPropertiesLink["NAME"];
									}
								}
							}
						}

						function __prepareFieldsForTextarea(&$value, $key)
						{
							$value = "#".$key."# - ".$value;
						}

						array_walk($arFields, "__prepareFieldsForTextarea");

						$href = "(new BX.CDialog({
							width: 700,
							height: 400,
							resizable: false,
							draggable: false,
							title: '".GetMessage("NEW_TITLE")."',
							content_url: '/bitrix/admin/type_edit.php?lang=".LANGUAGE_ID."&EVENT_NAME=".$strEventType."',
							content: '	<form id=\"defacore_addEventType\" method=\"POST\" action=\"/bitrix/admin/type_edit.php?lang=".LANGUAGE_ID."&EVENT_NAME=".$strEventType."\">
											<input type=\"hidden\" name=\"".LANGUAGE_ID."\" value=\"Y\">
											<input type=\"hidden\" name=\"apply\" value=\"Y\">
											".bitrix_sessid_post()."
											<div class=\"adm-workarea\">
												<table class=\"adm-detail-content-table edit-table\">
													<tr><td class=\"adm-detail-content-cell-l\" style=\"width: 140px;\">".GetMessage("EVENT_NAME").":</td><td class=\"adm-detail-content-cell-r\"><div class=\"adm-input-wrap\"><input type=\"text\" class=\"adm-input\" name=\"EVENT_NAME\" value=\"".$strEventType."\"></div></td></tr>
													<tr><td class=\"adm-detail-content-cell-l\">".GetMessage("EVENT_NAME_LANG").":</td><td class=\"adm-detail-content-cell-r\"><div class=\"adm-input-wrap\"><input type=\"text\" class=\"adm-input\" name=\"FIELDS[".LANGUAGE_ID."][NAME]\" value=\"".$resIblock["NAME"].": Отправка элементов по почте\"></div></td></tr>
													<tr><td class=\"adm-detail-content-cell-l\">".GetMessage("EVENT_DESCR_LANG").":</td><td class=\"adm-detail-content-cell-r\"><div class=\"adm-input-wrap\"><textarea class=\"adm-input\" style=\"height: 276px; resize: none;\" name=\"FIELDS[".LANGUAGE_ID."][DESCRIPTION]\">".implode("\\r", $arFields)."</textarea></div></td></tr>
												</table>
											</div>
										</form>
							',
							buttons: ['<input onclick=\"BX(\'defacore_addEventType\').submit();\" name=\"savebtn\" type=\"button\" value=\"".GetMessage("MAIN_ADMIN_MENU_CREATE")." ".ToLower(GetMessage("EVENT_NAME"))."\" class=\"adm-btn-save\">', BX.CDialog.btnCancel]
						})).Show()";
					}
				}
			}

			return self::_GenerateMenuItem(
				"Отправка письма при создании элемента (создан - <strong>".(GetMessage($bTemplateExists?"MAIN_YES":"MAIN_NO"))."</strong>)",
				$href
			);
		}
	}


}

?>