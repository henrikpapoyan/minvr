<?
AddEventHandler("main", "OnEpilog", array("CMainEventHandlers", "showError404page"), 10000);
AddEventHandler("main", "OnAdminContextMenuShow", array("CMainEventHandlers", "OnAdminContextMenuShow"), 10000);

class CMainEventHandlers extends CALLEventHandlers
{
	public static function OnAdminContextMenuShow(&$items)
	{
		$arMenuItems = array();

		$events = GetModuleEvents("main", "OnAdminCustomContextGenerate");
		while($arEvent = $events->Fetch())
		{
			$arMenuItem = ExecuteModuleEventEx($arEvent, array(substr($GLOBALS["APPLICATION"]->GetCurPage(), strlen(BX_ROOT."/admin/"))));
			if (is_array($arMenuItem) && !empty($arMenuItem))
				$arMenuItems[] = $arMenuItem;
		}

		if (!empty($arMenuItems))
		{
			$items[] = array(
					"TEXT" => ART_CORE_NAME,
					"TITLE" => ART_CORE_NAME,
					"ICON" => "btn_new_defacore",
					"MENU" => $arMenuItems
			);
		}
	}


	public static function showError404page()
	{
		static $bShow404 = false;

		if ($bShow404) return;

//ну как-же, нельзя это тут оставлять - не рабоатет на ЧПУ-компонентах тогда
//		if (method_exists("\Bitrix\Iblock\Component\Tools", "process404")) return; // fires error with our handler

		global $APPLICATION, $USER;
		if(
			(defined("ERROR_404") && ERROR_404 == "Y")
			&& ($APPLICATION->GetCurPage(true) <> '/404.php')
			&& defined("SITE_TEMPLATE_ID")
			&& defined("SITE_ID")
		)
		{
			if($APPLICATION->GetFileAccessPermission("/404.php") < "R")
				return;
			$sTemplateID = SITE_TEMPLATE_ID;

			$events = GetModuleEvents("defa", "OnBeforeShowError404page");
			while($arEvent = $events->Fetch())
			{
				$bEventRes = ExecuteModuleEventEx($arEvent, array(&$sTemplateID));
				if($bEventRes === false)
					return;
			}

			$bShow404 = true;
			$APPLICATION->RestartBuffer();
			$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");

			if(file_exists($_SERVER['DOCUMENT_ROOT']."/local/templates/".$sTemplateID."/header.php"))
				include($_SERVER['DOCUMENT_ROOT']."/local/templates/".$sTemplateID."/header.php");
			else
				include($_SERVER['DOCUMENT_ROOT']."/bitrix/templates/".$sTemplateID."/header.php");

			include($_SERVER["DOCUMENT_ROOT"]."/404.php");

			if(file_exists($_SERVER['DOCUMENT_ROOT']."/local/templates/".$sTemplateID."/footer.php"))
				include($_SERVER['DOCUMENT_ROOT']."/local/templates/".$sTemplateID."/footer.php");
			else
				include($_SERVER['DOCUMENT_ROOT']."/bitrix/templates/".$sTemplateID."/footer.php");

			include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
			die();
		}
	}
}
?>