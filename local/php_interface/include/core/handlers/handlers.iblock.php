<?
AddEventHandler("iblock", "OnBeforeIBlockAdd", array("CIblockEventHandlers", "OnBeforeIBlockAdd"), 1);
AddEventHandler("iblock", "OnBeforeIBlockPropertyAdd", array("CIblockEventHandlers", "OnBeforeIBlockPropertyAdd"), 1);
AddEventHandler("iblock", "OnAfterIblockElementAdd", array("CIblockEventHandlers", "sendEventIBlockItem"), 10000);

class CIblockEventHandlers extends CALLEventHandlers
{
	static $sendEventIBlockItemPrefix = "ART_IBLOCK_ITEM_";

	public static function OnBeforeIBlockAdd(&$arFields)
	{
		if ($arFields["RIGHTS_MODE"] == "S")
		{
			$arFields["GROUP_ID"][2] = "R";
		}


		if (!is_array($arFields["FIELDS"]))
		{
			foreach(array("LOG_SECTION_ADD", "LOG_SECTION_EDIT", "LOG_SECTION_DELETE", "LOG_ELEMENT_ADD", "LOG_ELEMENT_EDIT", "LOG_ELEMENT_DELETE") as $code)
				$arFields["FIELDS"][$code] = array("IS_REQUIRED" => "Y");
		}

		//$arFields["FIELDS"]["CODE"]["IS_REQUIRED"] = "Y";

		foreach ($arFields["FIELDS"] as $key => $value)
		{
			if (substr($key, 0, 4) == "LOG_")
			{
				$arFields["FIELDS"][$key]["IS_REQUIRED"] = "Y";
			}
		}
	}

	public static function OnBeforeIBlockPropertyAdd(&$arFields)
	{
		if (empty($arFields["CODE"]))
		{
			return self::pushError("Не рекомендуется создавать свойства с пустым символьным кодом");
		}

		if (substr($arFields["CODE"], 0, 1) != "_" && !in_array($arFields["CODE"], array("vote_count", "vote_sum", "rating")) /* iblock.vote crashes */)
		{
			$arFields["CODE"] = ToUpper($arFields["CODE"]);
		}

	}

	public static function sendEventIBlockItem($arFields)
	{
		if(!$arFields["ID"] || (defined("ADMIN_SECTION") && ADMIN_SECTION == true))
			return;

		if(CModule::IncludeModule("workflow") && (CIBlock::GetArrayByID($arFields["IBLOCK_ID"], "WORKFLOW") != "N"))
		{
			/**
			 * если сталкиваемся с документооборотом, то
			 * отправляем только последние изменённые данные
			 * */
			$wfLastID = CIBlockElement::WF_GetLast($arFields["ID"]);
			if($wfLastID != $arFields["ID"])
				$arFields["ID"] = $wfLastID;
			else
				return;
		}

		$sEventName = self::$sendEventIBlockItemPrefix.$arFields["IBLOCK_ID"];
		$rs = CEventType::GetList(array("EVENT_NAME" => $sEventName), array());
		if($ar = $rs->Fetch())
			CDFAMailTools::SendIBlockItem($arFields["ID"], $sEventName, SITE_ID);
	}
}



?>