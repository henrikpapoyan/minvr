<?
/**
 * edit iblock field
 * @author ivanko
 * @package iblock
 */
class CDFAIblockFormField
{
	private $arProperty;
	private $value = null;
	private $name = "";

	public function __construct(array $arProperty, $name, $value)
	{
		if(is_set($arProperty, "ENUM"))
		{
			$arEnums = array();
			foreach($arProperty["ENUM"] as $key=>$val)
			{
				$arEnums[$key] = is_array($val)
						? $val
						: array("VALUE" => $val);
			}
			$arProperty["ENUM"] = $arEnums;
		}

		$this->arProperty = $arProperty;
		$this->setValue($value);
		$this->setName($name);
	}
	public function setValue($value)
	{
		$this->value = array();
		if(is_array($value)) $this->value = $value;
		elseif(strlen(trim($value)) > 0) $this->value = array(trim($value));
	}
	public function setName($name)
	{
		$this->name = trim($name);
	}
	public function getName()
	{
		return $this->name;
	}
	public function isHidden()
	{
		return $this->arProperty["PROPERTY_TYPE"] == 'H';
	}
	public function getInputField($bFromForm, $bShowEmptyOption=true)
	{
		if($this->isHidden())
			return $this->getHiddenInputField($bFromForm);
		else
			return $this->getUserInputField($bFromForm, $bShowEmptyOption);
	}
	public function getHiddenInputField($bFromForm)
	{
		if(!$this->isHidden())
			return null;

		ob_start();
		$arProperty = $this->arProperty; 
		$propertyID = $arProperty["ID"];

		$value = array();
		foreach($this->value as $val)
			$value[] = is_array($val) ? $val["VALUE"] : $val;

		if(is_array($arProperty["ENUM"]) && count($arProperty["ENUM"]))
		{
			if(!is_array($arProperty["DEFAULT_VALUE"]))
				$arProperty["DEFAULT_VALUE"] = array($arProperty["DEFAULT_VALUE"]);
			$i = 0;
			foreach($arProperty["ENUM"] as $key => $arEnum)
			{
				if( (!$bFromForm && (($arEnum["DEF"] == 'Y') || in_array($arEnum["ID"], $arProperty["DEFAULT_VALUE"])) ) || ($bFromForm && in_array($arEnum["ID"], $value)) )
				{
					?><input type="hidden" name="<? echo $this->getName(); ?>[n<? echo ($i++); ?>]" value="<? echo $arEnum["ID"] ?>" /><?
				}
			}
		}
		else
		{
			if ($bMultiple)
			{
				$inputNum = ($bFromForm) ? count($value) : 0;
				$inputNum += $arProperty["MULTIPLE_CNT"];
			}
			else
			{
				$inputNum = 1;
			}

			for ($i = 0; $i<$inputNum; $i++)
			{
				$val = "";
				if ($bFromForm)
				{
					$val = /*intval($propertyID) > 0 ? $value[$i]["VALUE"] : */$value[$i];
				}
				elseif(intval($propertyID) > 0 && $bMultiple && is_array($arProperty[$propertyID]["DEFAULT_VALUE"]))
				{
					$val = $arProperty["DEFAULT_VALUE"][$i]["VALUE"];
					$description = $arProperty["DEFAULT_VALUE"][$i]["DESCRIPTION"];
				}
				elseif ($i == 0)
				{
					$val = intval($propertyID) <= 0 ? "" : $arProperty["DEFAULT_VALUE"];
				}

				?><input type="hidden" id="p<? echo $propertyID ?>" name="<? echo $this->getName(); ?>[<? echo (intval($propertyID) <= 0 ? '' : 'n'); ?><? echo ($i++); ?>]" value="<? echo $val; ?>" /><?
			}
		}
		return ob_get_clean();
	}
	public function getUserInputField($bFromForm=false, $bShowEmptyOption=true)
	{
		if($this->isHidden())
			return null;

		global $APPLICATION;

		$arProperty = $this->arProperty;
		$bMultiple = ($arProperty["MULTIPLE"] == "Y");

		if($arProperty["PROPERTY_TYPE"] == "T" && $arProperty["ROW_COUNT"] == "1")
			$arProperty["PROPERTY_TYPE"] = "S";
		elseif(in_array($arProperty["PROPERTY_TYPE"], array("S", "N")) && $arProperty["ROW_COUNT"] > "1")
			$arProperty["PROPERTY_TYPE"] = "T";

		$value = $this->value;

		$propertyID = $arProperty["ID"];
		$INPUT_TYPE = ($arProperty["GetPublicEditHTML"]) ? "USER_TYPE" : $arProperty["PROPERTY_TYPE"];

		$inputNum = 1;
		if ($bMultiple)
		{
			$inputNum = $bFromForm ? count($value) : 0;
			$inputNum += $arProperty["MULTIPLE_CNT"];
		}

		ob_start();
		switch ($INPUT_TYPE)
		{
			case "USER_TYPE":

			if($bMultiple)
			{
				?><table cellspacing="0" cellpadding="0" border="0" id="tb<? echo $propertyID ?>" class="from_field"><?
			}

								for ($i = 0; $i<$inputNum; $i++)
								{
									if($bMultiple)
									{
	                                                                        ?><tr><td><?
									}

									$val = "";
									if ($bFromForm)
									{
										$val = intval($propertyID) > 0 ? $value[$i]["VALUE"] : $value[$i];
									}
									elseif(intval($propertyID) > 0 && $bMultiple && is_array($arProperty[$propertyID]["DEFAULT_VALUE"]))
									{
                                                                                $val = $arProperty["DEFAULT_VALUE"][$i]["VALUE"];
                                                                                $description = $arProperty["DEFAULT_VALUE"][$i]["DESCRIPTION"];
									}
									elseif ($i == 0)
									{
										$val = is_array($arProperty["DEFAULT_VALUE"]) ? $arProperty["DEFAULT_VALUE"][$i] : $arProperty["DEFAULT_VALUE"];
									}

									if($arProperty["GetPublicEditHTML"][0] == "CIBlockPropertyHTML")
									{

											$val = array(
												"VALUE" => $val,
												"DESCRIPTION" => $description,
        									);

											$strHTMLControlName = array(
												"VALUE" => "PROPERTY[".$propertyID."][n".$i."][VALUE]",
												"DESCRIPTION" => "PROPERTY[".$propertyID."][n".$i."][DESCRIPTION]",
												"FORM_NAME"=>"iblock_add",
        									);


											if (!CModule::IncludeModule("fileman"))
												die(GetMessage("IBLOCK_PROP_HTML_NOFILEMAN_ERROR"));

											if (!is_array($val["VALUE"]))
												$val = CIBlockPropertyHTML::ConvertFromDB($arProperty, $val);

											$settings = CIBlockPropertyHTML::PrepareSettings($arProperty);

											$id = preg_replace("/[^a-z0-9]/i", '', $strHTMLControlName['VALUE']);
											$LHE = new CLightHTMLEditor;
											$LHE->Show(array(
												'bInitByJS' => false,
												'id' => $id,
												'width' => '100%',
												'height' => $settings['height'].'px',
												'inputName' => $strHTMLControlName["VALUE"].(substr($strHTMLControlName["VALUE"], - 7)!='[VALUE]'? '[VALUE]': '').'[TEXT]',
												'content' => htmlspecialcharsback($val["VALUE"]['TEXT']),
												'bUseFileDialogs' => false,
												'bFloatingToolbar' => false,
												'bArisingToolbar' => true,
												'toolbarConfig' => array(
													'Bold', 'Italic', 'Underline', 'RemoveFormat',
													'CreateLink', 'DeleteLink', 'Image', 'Video',
													'BackColor', 'ForeColor',
													'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
													'InsertOrderedList', 'InsertUnorderedList', 'Outdent', 'Indent',
													'StyleList', 'HeaderList',
													'FontList', 'FontSizeList',
											),
											));
                                                                		/*?><script>winOnLoad_<?echo $id ?>();</script><?*/
									}
									else
									{
        									echo call_user_func_array($arProperty["GetPublicEditHTML"],
        										array(
        											$arProperty,
        											array(
        												"VALUE" => $val,
        												"DESCRIPTION" => $description,
        											),
        											array(
        												"VALUE" => "PROPERTY[".$propertyID."][n".$i."][VALUE]",
        												"DESCRIPTION" => "PROPERTY[".$propertyID."][n".$i."][DESCRIPTION]",
        												"FORM_NAME"=>"iblock_add",
													"MODE" => "FORM_FILL"
        											),
        										));
                                                                        }

                                                                        if($bMultiple)
                                                                        {
								        	?></td></tr><?
                                                                        }
								}

								if($bMultiple)
								{
									        ?><tr><td><input type="button" value="<? echo GetMessage("IBLOCK_FORM_PROP_ADD")?>" onclick="addNewRow('tb<? echo $propertyID ?>')"></td></tr><?
									?></table><?
								}

								break;

						        case "ACTIVE":
                                                                        if ($bFromForm)
                                                                                $val = $value[0];
                                                                        else
                                                                                $val = $arProperty["DEFAULT_VALUE"];

                                                                        $checked = $val == "Y";
                                                                        echo SelectBoxFromArray($this->getName(), array("REFERENCE" => array(GetMessage("MAIN_YES"), GetMessage("MAIN_NO")), "REFERENCE_ID" => array("Y", "N")), $val);
                                                                break;

							case "TAGS":
								$APPLICATION->IncludeComponent(
									"bitrix:search.tags.input",
									"",
									array(
										"VALUE" => $arResult["ELEMENT"][$propertyID],
										"NAME" => "PROPERTY[".$propertyID."][0]",
										"TEXT" => 'size="'.$arProperty["COL_COUNT"].'"',
									), null, array("HIDE_ICONS"=>"Y")
								);
								break;

							case "HTML":

								$id = preg_replace("/[^a-z0-9]/i", '', $this->getName()."[0]");

								?><script>if (window.JCLightHTMLEditor) window.JCLightHTMLEditor.items['<?=$id?>'] = undefined;</script><?

								$LHE = new CLightHTMLEditor;
								$LHE->Show(array(
									'bInitByJS' => false,
									'id' => $id,
									'width' => '100%',
									'height' => '200px',
									'inputName' => $this->getName()."[0]",
									'content' => $value[0],
									'bUseFileDialogs' => false,
									'bFloatingToolbar' => false,
									'bArisingToolbar' => false,
									'toolbarConfig' => array(
										'Bold', 'Italic', 'Underline', 'RemoveFormat',
										'CreateLink', 'DeleteLink', 'Image', 'Video',
										'BackColor', 'ForeColor',
										'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
										'InsertOrderedList', 'InsertUnorderedList', 'Outdent', 'Indent',
										'StyleList', 'HeaderList',
										'FontList', 'FontSizeList',
									),
								));
								break;

							case "T":

								for ($i = 0; $i<$inputNum; $i++)
								{

									$val = "";
									if ($bFromForm)
									{
										$val = intval($propertyID) > 0 ? $value[$i]["VALUE"] : $value[$i];
									}
									elseif ($i == 0)
									{
										if(is_array($arProperty["DEFAULT_VALUE"]))
											$val = $arProperty["DEFAULT_VALUE"][$i];
										else
											$val = $arProperty["DEFAULT_VALUE"];
									}

									$enumName = $i;
									if(intval($propertyID) > 0)
                                                                                $enumName = 'n'.$enumName;

									?><textarea cols="<?=$arProperty["COL_COUNT"]?>" rows="<?=$arProperty["ROW_COUNT"]?>"<? if($arProperty["MAX_LENGTH"]): ?> maxlength="<?= $arProperty["MAX_LENGTH"] ?>"<? endif; ?> name="<? echo $this->getName(); ?>[<?=$enumName?>]"><?=$val?></textarea><?

								}
							break;

							case "S":
							case "N":

								if($bMultiple)
								{
									?><table cellspacing="0" cellpadding="0" border="0" id="tb<? echo $propertyID ?>" class="from_field"><?
								}

								for ($i = 0; $i<$inputNum; $i++)
								{
									if($bMultiple)
									{
										?><tr><td><?
									}

									$val = "";
									if ($bFromForm)
									{
										$val = intval($propertyID) > 0 ? $value[$i]["VALUE"] : $value[$i];
									}
									elseif(intval($propertyID) > 0 && $bMultiple && is_array($arProperty["DEFAULT_VALUE"]))
									{
                                                                                $val = $arProperty["DEFAULT_VALUE"][$i]["VALUE"];
                                                                                $description = $arProperty["DEFAULT_VALUE"][$i]["DESCRIPTION"];
									}
									elseif ($i == 0)
									{
										$val = is_array($arProperty["DEFAULT_VALUE"]) ? $arProperty["DEFAULT_VALUE"][$i] : $arProperty["DEFAULT_VALUE"];
									}

									$COL_COUNT = $arProperty["COL_COUNT"];

									$enumName = $i;
									if(intval($propertyID) > 0)
                                                                                $enumName = 'n'.$enumName;

									?><input type="text" name="<? echo $this->getName(); ?>[<?=$enumName?>]" size="<?=$COL_COUNT?>" value="<?=$val;?>"<? if(isset($arProperty["MAX_LENGTH"]) && (int)$arProperty["MAX_LENGTH"]): ?> maxlength="<?= $arProperty["MAX_LENGTH"] ?>"<? endif; ?> /><?

									if($arProperty["USER_TYPE"] == "DateTime"):?><?

										$APPLICATION->IncludeComponent(
											'bitrix:main.calendar',
											'',
											array(
												'FORM_NAME' => 'iblock_add',
												'INPUT_NAME' => "PROPERTY[".$propertyID."][".$enumName."]",
												'INPUT_VALUE' => $val,
											),
											null,
											array('HIDE_ICONS' => 'Y')
										);
									endif;

									if($bMultiple)
									{
										?></td></tr><?
									}
								}

								if($bMultiple)
								{
										?><tr><td><input type="button" value="<? echo GetMessage("IBLOCK_FORM_PROP_ADD")?>" onclick="addNewRow('tb<? echo $propertyID ?>')"></td></tr><?
									?></table><?
								}

							break;
							case "F":

								if($bMultiple)
								{
									?><ul id="ul<? echo $propertyID ?>" class="form_field"><?
								}

								for ($i = 0; $i<$inputNum; $i++)
								{
									if($bMultiple)
									{
										?><li><?
									}

									$val = "";
									if ($bFromForm)
									{
										$val = intval($propertyID) > 0 ? $value[$i]["VALUE"] : $value[$i];
									}
									elseif($bMultiple && is_array($arProperty["DEFAULT_VALUE"]))
									{
										$description = '';
										if(($arProperty["WITH_DESCRIPTION"] == "Y"))
										{
	                                                                                $val = $arProperty["DEFAULT_VALUE"][$i]["VALUE"];
	                                                                                $description = $arProperty["DEFAULT_VALUE"][$i]["DESCRIPTION"];
										}
										else
										{
											$val = $arProperty["DEFAULT_VALUE"][$i];
										}
									}
									elseif ($i == 0)
									{
										$val = is_array($arProperty["DEFAULT_VALUE"]) ? $arProperty["DEFAULT_VALUE"][$i] : $arProperty["DEFAULT_VALUE"];
									}
									?><input type="hidden" name="<? echo $this->getName(); ?>[<?=$value[$i]["VALUE_ID"] ? $value[$i]["VALUE_ID"] : 'n'.$i?>]<? echo ($arProperty["WITH_DESCRIPTION"] == "Y") ? "[VALUE]" : ""; ?>" value="<?=$val?>" />
                                					<div class="file_input"><input type="file" size="<?=$arProperty["COL_COUNT"]?>"  name="PROPERTY_FILE_<?=$propertyID?>_<?=$value[$i]["VALUE_ID"] ? $value[$i]["VALUE_ID"] : 'n'.$i?>" /></div><?

                                					if($arProperty["WITH_DESCRIPTION"] == "Y")
                                					{
                                						?><input type="text" name="<? echo $this->getName(); ?>[<?=$value[$i]["VALUE_ID"] ? $value[$i]["VALUE_ID"] : 'n'.$i?>][DESCRIPTION]" value="<?=$value[$i]["DESCRIPTION"]?>" /><?
                                					}
									if(!empty($val) && ($arFile = CFile::GetByID($val)->Fetch()))
									{
										$arFile["SRC"] = CFile::GetPath($arFile["ID"]);

										?><div class="file_description">
											<?=$arFile["ORIGINAL_NAME"]?> (<?=DSiteTemplate::GetDisplayFileSize($arFile)?>)<br />
                                                					<input type="checkbox" name="DELETE_FILE[<?=$propertyID?>][<?=$value[$i]["VALUE_ID"] ? $value[$i]["VALUE_ID"] : 'n'.$i?>]" id="file_delete_<?=$propertyID?>_<?=$i?>" value="Y" /><label for="file_delete_<?=$propertyID?>_<?=$i?>"><?=GetMessage("IBLOCK_FORM_FILE_DELETE")?></label>
										</div><?
										if ($arProperty["SHOW_IMAGE"] == "Y") {
											?><div class="file_description"><?= CFile::ShowImage($arFile["ID"], ((int)$arProperty["IMAGE_WIDTH"] ? (int)$arProperty["IMAGE_WIDTH"] : "300"), ((int)$arProperty["IMAGE_HEIGHT"] ? (int)$arProperty["IMAGE_HEIGHT"] : "300"), $arProperty["IMAGE_PARAMS"], (strlen($arProperty["IMAGE_POPUP"]) && $arProperty["IMAGE_POPUP"] !== "Y" ? $arProperty["IMAGE_POPUP"] : ""), strlen($arProperty["IMAGE_POPUP"])) ?></div><?
										}
									}
									if($bMultiple)
									{
										?></li><?
									}
								}

								if($bMultiple)
								{
										?><li class="buttons"><input type="button" value="<? echo GetMessage("IBLOCK_FORM_PROP_ADD")?>" onclick="addNewRow('ul<? echo $propertyID ?>')"></li><?
									?></ul><div class="cb"></div><? 
								} 

							break;

							case "G":

                                                                ?><select name="<? echo $this->getName(); ?><?=$bMultiple ? "[]\" size=\"".$size."\" multiple=\"multiple" : ""?>">
                                                                	<?if($bShowEmptyOption):?><option value=""><?=GetMessage("IBLOCK_FORM_NOT_SET")?></option><?endif;?><?
								foreach($arProperty["ENUM"] as $key => $arEnum)
								{
									$checked = false;
									if($bFromForm)
									{
										foreach ($value as $elKey => $arElEnum)
										{
											if ($key == $arElEnum["VALUE"]) {
												$checked = true; break;
											}
										}
									}
									else
									{
										if ($arEnum["DEF"] == "Y") $checked = true;
									}

									?><option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=(strlen($arEnum["VALUE"]) ? $arEnum["VALUE"] : (strlen($arEnum["~NAME"]) ? $arEnum["~NAME"] : $arEnum["NAME"]))?></option><?
								}

                                                                ?></select><?
							break;

							case "L":

								if($arProperty["LIST_TYPE"] == "C")
									$type = ($bMultiple || count($arProperty["ENUM"]) == 1)? "checkbox" : "radio";
								else
									$type = $bMultiple ? "multiselect" : "dropdown";

								switch($type)
								{
									case "checkbox":
									case "radio":

										foreach($arProperty["ENUM"] as $key => $arEnum)
										{
											$sInputName = ($type == "checkbox") ? $this->getName().'['.$key.']' : $this->getName();

											$checked = false;
											if($bFromForm)
											{
												foreach ($value as $elKey => $arElEnum)
												{
													if ($key == $arElEnum["VALUE"]) {
														$checked = true; break;
													}
												}
											}
											else
											{
												if ($arEnum["DEF"] == "Y") $checked = true;
											}

											if($type == "checkbox")
											{
												?><div class="field_option"><?

															?><input type="checkbox" name="<?=$sInputName?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><?

															if(count($arProperty["ENUM"]) > 1)
															{
																?><label for="property_<?=$key?>"><?=$arEnum["VALUE"]?></label><?
															}
															?><div class="cb"></div></div><?
											}
											else
											{
															?><input type="<?=$type?>" name="<?=$sInputName?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><?

															if(count($arProperty["ENUM"]) > 1)
															{
																?><label for="property_<?=$key?>"><?=$arEnum["VALUE"]?></label><?
															}
											}
										}

										break;

												case "dropdown":
												case "multiselect":

													$sInputAttrs = ($type == "multiselect") ? 'name="'.$this->getName().'[]" size="'.$arProperty["ROW_COUNT"].'" multiple="multiple"' : 'name="'.$this->getName().'"';
													?><select <?=$sInputAttrs?>>
														<?if($bShowEmptyOption):?><option value=""><?=GetMessage("IBLOCK_FORM_NOT_SET")?></option><?endif;?><?

														foreach($arProperty["ENUM"] as $key => $arEnum)
														{
															$checked = false;
															if($bFromForm)
															{
																foreach ($value as $elKey => $arElEnum)
																{
																	if ($key == $arElEnum["VALUE"]) {
																		$checked = true; break;
																	}
																}
															}
															else
															{
																if ($arEnum["DEF"] == "Y") $checked = true;
															}

															?><option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option><?
														}
													?></select><?
												break;
											}
										break;
		}

		return ob_get_clean();
	}
}
?>