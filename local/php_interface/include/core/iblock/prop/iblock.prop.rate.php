<?
AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("CDFAIBlockPropertyNumberRateList", "GetUserTypeDescription"));

class CDFAIBlockPropertyNumberRateList extends CDFAIBlockPropertyString
{
        public static function GetUserTypeDescription()
        {
                return array(
                        "PROPERTY_TYPE"              =>	"N",
                        "USER_TYPE"                  =>	"DefaNumberRate",
                        "DESCRIPTION"                =>	"Рейтинг: Число от 1 до N",
                        "ConvertToDB"                =>	array("CDFAIBlockPropertyNumberRateList", "ConvertToDB"),
                        "PrepareSettings"            =>	array("CDFAIBlockPropertyNumberRateList", "PrepareSettings"),
                        "GetPublicFilterHTML"        =>	array("CDFAIBlockPropertyNumberRateList", "GetPublicFilterHtml"),
                        "GetSettingsHTML"            =>	array("CDFAIBlockPropertyNumberRateList", "GetSettingsHTML"),
                        "GetPropertyFieldHtml"       =>	array("CDFAIBlockPropertyNumberRateList", "GetPropertyFieldHtml"),
                        "GetPublicEditHTML"      	 =>	array("CDFAIBlockPropertyNumberRateList", "GetPublicEditHTML"),
                );
        }

        public static function GetPublicFilterHtml($arProperty, $value, $strHTMLControlName)
        {
        	return self::GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName);
        }

        public static function __FormatSettings(&$arProperty)
        {
                $arProperty["USER_TYPE_SETTINGS"]["RATE_FROM"] = intval($arProperty["USER_TYPE_SETTINGS"]["RATE_FROM"]);
                if(!$arProperty["USER_TYPE_SETTINGS"]["RATE_FROM"])
                        $arProperty["USER_TYPE_SETTINGS"]["RATE_FROM"] = 1;

                $arProperty["USER_TYPE_SETTINGS"]["RATE_TO"] = intval($arProperty["USER_TYPE_SETTINGS"]["RATE_TO"]);
                if(!$arProperty["USER_TYPE_SETTINGS"]["RATE_TO"])
                        $arProperty["USER_TYPE_SETTINGS"]["RATE_TO"] = 5;

                return $arProperty["USER_TYPE_SETTINGS"];
        }

        function PrepareSettings($arProperty)        {
        	$arFields = self::__FormatSettings($arProperty);
        	return $arProperty["USER_TYPE_SETTINGS"];        }

        public static function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
        {
                $arSettings = self::__FormatSettings($arProperty);

				$html  .= '<tr>
                		<td>Диапазон:</td>
                		<td>
							<input type="text" value="'.htmlspecialchars($arSettings["RATE_FROM"]).'" size="3" name="'.$strHTMLControlName["NAME"].'[RATE_FROM]" /> ...
							<input type="text" value="'.htmlspecialchars($arSettings["RATE_TO"]).'" size="3" name="'.$strHTMLControlName["NAME"].'[RATE_TO]" />
						</td>';
				$html .= '
		      </tr>';

		return $html;
        }

        public static function __GetListArray($arProperty)
        {
                $arSettings = self::__FormatSettings($arProperty);

                $arr = array();
                for($i = $arSettings["RATE_FROM"]; $i <= $arSettings["RATE_TO"]; $i++)
                        $arr[$i] = $i;

                return $arr;
        }

	public static function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
	{
		$html = "";
		foreach (self::__GetListArray($arProperty) as $item)
		{
			$html .= '<div><label><input type="radio" name="'.$strHTMLControlName["VALUE"].'" value="'.$item.'" '.(($value["VALUE"]==$item)?"checked":"").'> '.$item.'</label></div>';
		}
		return $html;
	}

	public static function GetPublicEditHTML($arProperty, $value, $strHTMLControlName)
	{

		$html = '<div class="radio">';
		foreach (self::__GetListArray($arProperty) as $item)
		{
			$html .= '<div class="box'.(($value["VALUE"]==$item)?" active":"").'"><label><input type="radio" name="'.$strHTMLControlName["VALUE"].'" value="'.$item.'" '.(($value["VALUE"]==$item)?"checked":"").'><br><span>'.$item.'</span></label></div>';
		}
		$html .= '</div>';
		return $html;
	}

	public static function ConvertToDB($arProperty, $value)
	{
		return $value;
	}
}
?>