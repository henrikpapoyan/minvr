<?
AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("CIBlockPropertyGoogleMapsPolygon", "GetUserTypeDescription"));

class CIBlockPropertyGoogleMapsPolygon
{
	function GetUserTypeDescription()
	{
		return array(
			"PROPERTY_TYPE" => "S",
			"USER_TYPE" => "ARTGoogle_maps_polygon",
			"DESCRIPTION" => "ART: Google.Maps. Полигон",
			"GetPropertyFieldHtml" => array("CIBlockPropertyGoogleMapsPolygon","GetPropertyFieldHtml"),
			"GetPublicViewHTML" => array("CIBlockPropertyGoogleMapsPolygon","GetPublicViewHTML"),
			"ConvertToDB" => array("CIBlockPropertyGoogleMapsPolygon","ConvertToDB"),
			"ConvertFromDB" => array("CIBlockPropertyGoogleMapsPolygon","ConvertFromDB"),
		);
	}

	function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
	{

		global $APPLICATION;

		if(isset($GLOBALS['googleMapLastNumber']))
			$GLOBALS['googleMapLastNumber']++;
		else
			$GLOBALS['googleMapLastNumber']=0;

		if ($strHTMLControlName["MODE"] != "FORM_FILL")
			return '<input type="text" name="'.htmlspecialcharsbx($strHTMLControlName['VALUE']).'" value="'.htmlspecialcharsbx($value['VALUE']).'" />';

		$arReadyValue = false;

		if (strlen($value['VALUE']) > 0)
		{
			$arReadyValue = CUtil::JsObjectToPhp($value['VALUE']);
		}

		$MAP_ID = 'map_google_point_'.$arProperty['CODE'].'_'.$arProperty['ID'].'_'.$GLOBALS['googleMapLastNumber'];

		?>
		<div id="bx_map_hint_<?echo $MAP_ID?>">
			<button type="button" id="reset_<?=$MAP_ID?>">Очистить полигон</button>
		</div>
		<br/>
		<div style="width:100%; height:400px;" id="<?=$MAP_ID?>" ></div>
		<input type="hidden" id="value_<?echo $MAP_ID;?>" name="<?=htmlspecialcharsbx($strHTMLControlName["VALUE"])?>" value="<?=htmlspecialcharsEx($value["VALUE"])?>" />

		<?if ($_REQUEST["bxsender"] !== 'core_window_cdialog')
		{
			$APPLICATION->AddHeadScript('//ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js');
			$APPLICATION->AddHeadScript(str_replace($_SERVER['DOCUMENT_ROOT'], '', __DIR__).'/js/jquery.googlemaps.polygon_creator.js');

			if (!defined('BX_GMAP_SCRIPT_LOADED'))			{				CUtil::InitJSCore();				$scheme = (CMain::IsHTTPS() ? "https" : "http");				$APPLICATION->AddHeadString('<script src="'.CSiteTemplateTools::GetProtocolPrefix().'maps.google.com/maps/api/js?sensor=false&language='.LANGUAGE_ID.'" charset="utf-8"></script>');
				define('BX_GMAP_SCRIPT_LOADED', 1);			}

			$mapCenter = array(
// 				"lat" => "55.7383",
// 				"lng" => "37.5946"
				"lat" => "63.93737247",
				"lng" => "144.66796875"
			);

			if ($arReadyValue)
			{
				$mapCenter = $arReadyValue[0];
			}
			?>

			<script>
			
				var map;
				var oPolygonMapConfig = {
					startCenter	: new google.maps.LatLng(<?=implode(',', $mapCenter)?>),
					startZoom	: 4
				}

				//var canvasProjection = new google.maps.MapCanvasProjection;

				function initGoogleMapsMap(mapID) {
					var mapOptions = {
						zoom: oPolygonMapConfig.startZoom,
						center: oPolygonMapConfig.startCenter,
						streetViewControl: false,
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						disableDoubleClickZoom : true
					};
					map = new google.maps.Map(document.getElementById(mapID),mapOptions);

					return map;
				}
			</script>
			<?
		}
		?>
		<script type="text/javascript">
		var map_<?=$MAP_ID?>;
		var creator_<?=$MAP_ID?>;

		/*google.maps.event.addDomListener(window, 'load', function(){
			
			map_<?=$MAP_ID?> = initGoogleMapsMap("<?=$MAP_ID?>");
			creator_<?=$MAP_ID?> = new PolygonCreator(map_<?=$MAP_ID?>);
			creator_<?=$MAP_ID?>.OnDrawPolygon = function()
			{
				var path = [];
				data = creator_<?=$MAP_ID?>.getData();

				for (var i=0; i<data.length; i++)
				{
					var xy = data.getAt(i);
					path.push({"lat": xy.lat(), "lng": xy.lng()});
				}

				BX('value_<?echo $MAP_ID;?>').value = JSON.stringify(path);
			}

			<?if ($arReadyValue):?>
				var listOfDots = new Array;
				var arLatLng = JSON.parse('<?=$value['VALUE']?>');

				$.each(arLatLng,function(index, value){
					var dot = new Dot(new google.maps.LatLng(value.lat,value.lng), creator_<?=$MAP_ID?>.pen.map, creator_<?=$MAP_ID?>.pen);
					listOfDots.push(dot);
				})
				creator_<?=$MAP_ID?>.pen.listOfDots = listOfDots;
				creator_<?=$MAP_ID?>.pen.drawPloygon(creator_<?=$MAP_ID?>.pen.listOfDots);
			<?endif;?>

			$('#reset_<?=$MAP_ID?>').on("click", function(){
				creator_<?=$MAP_ID?>.destroy();
				BX('value_<?echo $MAP_ID;?>').value = "";
				return false;
			});

		});*/
		
		map_<?=$MAP_ID?> = initGoogleMapsMap("<?=$MAP_ID?>");
		creator_<?=$MAP_ID?> = new PolygonCreator(map_<?=$MAP_ID?>);
		creator_<?=$MAP_ID?>.OnDrawPolygon = function()
		{
			var path = [];
			data = creator_<?=$MAP_ID?>.getData();

			for (var i=0; i<data.length; i++)
			{
				var xy = data.getAt(i);
				path.push({"lat": xy.lat(), "lng": xy.lng()});
			}

			BX('value_<?echo $MAP_ID;?>').value = JSON.stringify(path);
		}

		<?if ($arReadyValue):?>
			var listOfDots = new Array;
			var arLatLng = JSON.parse('<?=$value['VALUE']?>');

			$.each(arLatLng,function(index, value){
				var dot = new Dot(new google.maps.LatLng(value.lat,value.lng), creator_<?=$MAP_ID?>.pen.map, creator_<?=$MAP_ID?>.pen);
				listOfDots.push(dot);
			})
			creator_<?=$MAP_ID?>.pen.listOfDots = listOfDots;
			creator_<?=$MAP_ID?>.pen.drawPloygon(creator_<?=$MAP_ID?>.pen.listOfDots);
		<?endif;?>

		$('#reset_<?=$MAP_ID?>').on("click", function(){
			creator_<?=$MAP_ID?>.destroy();
			BX('value_<?echo $MAP_ID;?>').value = "";
			return false;
		});
		
		function removePoint_<?echo $MAP_ID?>()
		{
			window.obPoint_<?echo $MAP_ID?>.setMap(null);
			window.obPoint_<?echo $MAP_ID?> = null;

			BX('bx_map_hint_novalue_<?echo $MAP_ID?>').style.display = 'block';
			BX('bx_map_hint_value_<?echo $MAP_ID?>').style.display = 'none';

			updatePointPosition_<?echo $MAP_ID?>();
		}

		function setPointValue_<?echo $MAP_ID?>(obPoint)
		{
			if (null == window.obPoint_<?echo $MAP_ID?>)
			{
				//google.maps.Marker
				window.obPoint_<?echo $MAP_ID?> = new google.maps.Marker({
					position: obPoint.latLng,
					map: window.map_<?=$MAP_ID?>,
					draggable:true
				});

				google.maps.event.addListener(window.obPoint_<?echo $MAP_ID?>, "dragend", updatePointPosition_<?echo $MAP_ID?>);
			}
			else
			{
				window.obPoint_<?echo $MAP_ID?>.setPosition(obPoint.latLng);
			}


			BX('bx_map_hint_novalue_<?echo $MAP_ID?>').style.display = 'none';
			BX('bx_map_hint_value_<?echo $MAP_ID?>').style.display = 'block';

			updatePointPosition_<?echo $MAP_ID?>(obPoint);

		}

		function updatePointPosition_<?echo $MAP_ID?>(obPoint)
		{
			var val = '';
			if (null != obPoint)
			{
				val = obPoint.latLng.lat() + ',' + obPoint.latLng.lng();
			}

			BX('value_<?echo $MAP_ID?>').value = val;
		}
		</script>
		<?
	}

	function GetPublicViewHTML($arProperty, $value, $arParams)
	{
		return "";
	}

	function ConvertFromDB($arProperty, $value)
	{
		$arResult['VALUE'] = $value['VALUE'];
		return $arResult;
	}

	function ConvertToDB($arProperty, $value)
	{
		$arResult['VALUE'] = $value['VALUE'];
		return $arResult;
	}
}

