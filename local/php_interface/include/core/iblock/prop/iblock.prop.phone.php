<?
/**
* phone iblock property
* @author ivanko
* @package iblock
* @subpackage prop
*/
class CDFAIBlockPropertyPhone
{
	const DEFAULT_MASK = '(999) 999-99-99';
	const DEFAULT_PLACEHOLDER = '_';
	const DEFAULT_REPLACE = '9';

	public static function GetUserTypeDescription()
	{
		return array(
			"PROPERTY_TYPE"	=> "S",
			"USER_TYPE" => "ARTPhone",
			"DESCRIPTION" => "ART: Номер телефона",
			"CheckFields" => array("CDFAIBlockPropertyPhone", "CheckFields"),
			"GetLength" => array("CDFAIBlockPropertyPhone", "GetLength"),
			"ConvertToDB" => array("CDFAIBlockPropertyPhone", "ConvertToDB"),
			"ConvertFromDB" => array("CDFAIBlockPropertyPhone", "ConvertFromDB"),
			"GetPropertyFieldHtml" => array("CDFAIBlockPropertyPhone", "GetPropertyFieldHtml"),
			"GetAdminListViewHTML" => array("CDFAIBlockPropertyPhone", "GetAdminListViewHTML"),
			"GetPublicViewHTML" => array("CDFAIBlockPropertyPhone", "GetPublicViewHTML"),
			"PrepareSettings" => array("CDFAIBlockPropertyPhone", "PrepareSettings"),
			"GetSettingsHTML" => array("CDFAIBlockPropertyPhone", "GetSettingsHTML")
		);
	}

	public static function CheckFields($arProperty, $value)
	{
		if(!empty($value["VALUE"]))
			$value["VALUE"] = trim($value["VALUE"], "\n\r\t ");

		self::_FormatGetSettings($arProperty);

		$sMask = str_replace(
			array('{SLASH}', $arProperty["USER_TYPE_SETTINGS"]["REPLACE"]),
			array('\\', '\d'),
			preg_replace('/([^\d\wа-яА-Я])/i', '{SLASH}$1', $arProperty["USER_TYPE_SETTINGS"]["MASK"])
		);

		$arError = array();
		if($value["VALUE"] && !preg_match('/^'.$sMask.'$/', $value["VALUE"]))
			$arError[] = 'Значение поля "'.$arProperty["NAME"].'" заполнено некорректно. Пример заполнения - '.$arProperty["USER_TYPE_SETTINGS"]["MASK"];

		return $arError;
	}

	public static function GetLength($arProperty, $value)
	{
		return strlen(trim($value["VALUE"], "\n\r\t "));
	}

	public static function ConvertToDB($arProperty, $value)
	{
		$strMaskMarkup = "_";

		if(!empty($value["VALUE"]))
			$value["VALUE"] = trim($value["VALUE"], "\n\r\t ");

		$strMaskReplace = preg_replace("/\s/i", $strMaskMarkup, str_replace($arProperty["USER_TYPE_SETTINGS"]["REPLACE"], $strMaskMarkup, $arProperty["USER_TYPE_SETTINGS"]["MASK"]));
		$strMaskedValue = "";
		for ($i = 0, $len = strlen($strMaskReplace); $i < $len; $i++)
			if ($strMaskReplace[$i] == $strMaskMarkup)
				$strMaskedValue .= $value["VALUE"][$i];

		$value["VALUE"] = preg_replace('/[^\d]/', '', $strMaskedValue);

		return $value;
	}

	public static function ConvertFromDB($arProperty, $value)
	{
		if(empty($value["VALUE"]))
			return $value;

		self::_FormatGetSettings($arProperty);

		$sMask = $arProperty["USER_TYPE_SETTINGS"]["MASK"];
		$sNewValue = '';

		$iValue = 0;
		for($iMask = 0; $iMask < strlen($sMask); $iMask++)
		{
			$sNewValue .= ($sMask[$iMask] == $arProperty["USER_TYPE_SETTINGS"]["REPLACE"]) ? $value["VALUE"][$iValue++] : $sMask[$iMask];
		}
		$value["VALUE"] = $sNewValue;

		return $value;
	}

	public static function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
	{
		
		self::_FormatGetSettings($arProperty);

		$sInputId = 'phone_'.md5($strHTMLControlName["VALUE"]);

		$sReturn = '';
		if(class_exists("CDFAJQueryPluginManager"))
		{
			CDFAJQueryPluginManager::Init(array("maskedinput"));
		}
		else
		{
			$path = "/bitrix/templates/.default/js/vendor/jquery.maskedinput.js";
			if(file_exists($_SERVER["DOCUMENT_ROOT"]."/local/templates/.default/js/vendor/jquery.maskedinput.js"))
				$path = "/local/templates/.default/js/vendor/jquery.maskedinput.js";
			
			$sReturn .= '<script src="'.$path.'" type="text/javascript"></script>';
		}

		if(strlen($arProperty["USER_TYPE_SETTINGS"]["PREFIX"]))
			$sReturn .= '<input id="prefix_'.$sInputId.'" onfocus="BX(\''.$sInputId.'\').focus();" type="text" size="2" maxlength="2" class="phone-prefix" value="'.htmlspecialchars($arProperty["USER_TYPE_SETTINGS"]["PREFIX"]).'">';

		$sReturn .= '<input id="'.$sInputId.'" type="text" size="30" maxlength="'.strlen($arProperty["USER_TYPE_SETTINGS"]["MASK"]).'" class="phone-number" name="'.$strHTMLControlName["VALUE"].'" value="'.htmlspecialchars($value["VALUE"]).'">';
		$sReturn .= ($arProperty["WITH_DESCRIPTION"]=="Y") ? '&nbsp;<input type="text" size="30" maxlength="120" name="'.$strHTMLControlName["DESCRIPTION"].'" value="'.htmlspecialchars($value["DESCRIPTION"]).'">' : '';
		$sReturn .= '<script>jQuery(function($){
				if (typeof($.mask) != \'undefined\') {
					delete $.mask.definitions[\'9\'];
					delete $.mask.definitions[\'*\'];
					delete $.mask.definitions[\'a\'];
					$.mask.definitions[\''.$arProperty["USER_TYPE_SETTINGS"]["REPLACE"].'\']=\'[0-9]\';
					$("#'.$sInputId.'").mask("'.$arProperty["USER_TYPE_SETTINGS"]["MASK"].'",{placeholder:"'.$arProperty["USER_TYPE_SETTINGS"]["PLACEHOLDER"].'"});
					var p = jQuery("#prefix_'.$sInputId.'");
					if(!jQuery.browser.msie && jQuery.fn.placeholder && p.length)
						p.attr("placeholder", p.val()).val("").placeholder();
				}});
			</script>';

		return $sReturn;
	}

	public static function GetPublicEditHTML($arProperty, $value, $strHTMLControlName)
	{
		return self::GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName);
	}

	public static function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName)
	{
		if(strlen($value["VALUE"]))
			return str_replace(" ", "&nbsp;", htmlspecialcharsex($value["VALUE"]));

		return '&nbsp;';
	}

	public static function GetPublicViewHTML($arProperty, $value, $strHTMLControlName)
	{
		return self::GetAdminListViewHTML($arProperty, $value, $strHTMLControlName);
	}

	private static function _FormatGetSettings(&$arProperty)
	{
		if(
			array_key_exists("USER_TYPE_SETTINGS", $arProperty)
			&& is_string($arProperty["USER_TYPE_SETTINGS"])
			&& strlen($arProperty["USER_TYPE_SETTINGS"])
		)
			$arProperty["USER_TYPE_SETTINGS"] = unserialize($arProperty["USER_TYPE_SETTINGS"]);
	}

        private static function _FormatEditSettings(&$arProperty)
        {
                $arProperty["USER_TYPE_SETTINGS"]["MASK"] = trim($arProperty["USER_TYPE_SETTINGS"]["MASK"], "\n\r\t ");
                if(empty($arProperty["USER_TYPE_SETTINGS"]["MASK"]))
                	$arProperty["USER_TYPE_SETTINGS"]["MASK"] = self::DEFAULT_MASK;

                if(empty($arProperty["USER_TYPE_SETTINGS"]["PLACEHOLDER"]))
                	$arProperty["USER_TYPE_SETTINGS"]["PLACEHOLDER"] = self::DEFAULT_PLACEHOLDER;

                if(strlen($arProperty["USER_TYPE_SETTINGS"]["PLACEHOLDER"]) > 1)
                	$arProperty["USER_TYPE_SETTINGS"]["PLACEHOLDER"] = $arProperty["USER_TYPE_SETTINGS"]["PLACEHOLDER"][0];

                if(empty($arProperty["USER_TYPE_SETTINGS"]["REPLACE"]))
                	$arProperty["USER_TYPE_SETTINGS"]["REPLACE"] = self::DEFAULT_REPLACE;

                if(strlen($arProperty["USER_TYPE_SETTINGS"]["REPLACE"]) > 1)
                	$arProperty["USER_TYPE_SETTINGS"]["REPLACE"] = $arProperty["USER_TYPE_SETTINGS"]["REPLACE"][0];

		return $arProperty["USER_TYPE_SETTINGS"];
        }

	public static function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
        {
        	$arProperty["USER_TYPE_SETTINGS"] = self::_FormatEditSettings($arProperty);

		$arPropertyFields = array(
			"HIDE" => array("COL_COUNT"),
			"USER_TYPE_SETTINGS_TITLE" => 'Настройки сохранения и отображения'
		);

		return '
		<tr>
        		<td>Маска ввода:</td>
        		<td><input type="text" value="'.htmlspecialchars($arProperty["USER_TYPE_SETTINGS"]["MASK"]).'" size="30" maxlength="50" name="'.$strHTMLControlName["NAME"].'[MASK]" /></td>
                </tr>
                <tr>
        		<td>Символ числа:</td>
        		<td><input type="text" value="'.htmlspecialchars($arProperty["USER_TYPE_SETTINGS"]["REPLACE"]).'" size="30" maxlength="1" name="'.$strHTMLControlName["NAME"].'[REPLACE]" /></td>
                </tr>
                <tr>
        		<td>Заполнитель:</td>
        		<td><input type="text" value="'.htmlspecialchars($arProperty["USER_TYPE_SETTINGS"]["PLACEHOLDER"]).'" size="30" maxlength="1" name="'.$strHTMLControlName["NAME"].'[PLACEHOLDER]" /></td>
                </tr>
                <tr>
        		<td>Префикс номера:</td>
        		<td><input type="text" value="'.htmlspecialchars($arProperty["USER_TYPE_SETTINGS"]["PREFIX"]).'" size="2" maxlength="2" name="'.$strHTMLControlName["NAME"].'[PREFIX]" /></td>
                </tr>';
        }

        public static function PrepareSettings($arProperty)
        {
                return self::_FormatEditSettings($arProperty);
        }
}

AddEventHandler("iblock", "OnIBlockPropertyBuildList", Array("CDFAIBlockPropertyPhone", "GetUserTypeDescription"));
?>