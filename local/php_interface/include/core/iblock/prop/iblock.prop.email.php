<?
CDFAIncludeManager::IncludeLang(__FILE__);
/**
 * email iblock property
 * @author ivanko
 * @package iblock
 * @subpackage prop
 */
require_once("iblock.prop.string.php");
class CDFAIBlockPropertyEmail extends CDFAIBlockPropertyString
{
	public static function GetUserTypeDescription()
	{
		return array(
			"PROPERTY_TYPE"	=> "S",
			"USER_TYPE" => "ARTEmail",
			"DESCRIPTION" => "ART: E-mail",
			"GetPropertyFieldHtml" => array("CDFAIBlockPropertyEmail", "GetPropertyFieldHtml"),
			"GetPublicEditHTML" => array("CDFAIBlockPropertyEmail", "GetPublicEditHTML"),
			"CheckFields" => array("CDFAIBlockPropertyEmail", "CheckFields"),
		);
	}

	public static function CheckFields($arProperty, $value)
	{
		if(!empty($value["VALUE"]))
			$value["VALUE"] = trim($value["VALUE"], "\n\r\t ");

		$arError = array();
		if(!empty($value["VALUE"]) && !check_email($value["VALUE"]))
			$arError[] = GetMessage('DFA_PROP_EMAIL_VALID');

		return $arError;
	}

	public static function GetLength($arProperty, $value)
	{
		return strlen(trim($value["VALUE"], "\n\r\t "));
	}
}

AddEventHandler("iblock", "OnIBlockPropertyBuildList", Array("CDFAIBlockPropertyEmail", "GetUserTypeDescription"));
?>