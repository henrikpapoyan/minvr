<?
/**
* base string property
* @author ivanko
* @package iblock
* @subpackage prop
*/ 
class CDFAIBlockPropertyString
{
	public static function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
	{
		if($arProperty["ROW_COUNT"] > 1)
			$html = '<textarea rows="'.intval($arProperty["ROW_COUNT"]).'" cols="'.intval($arProperty["COL_COUNT"]).'" name="'.$strHTMLControlName["VALUE"].'">'.htmlspecialchars($value["VALUE"]).'</textarea>';
		else
			$html = '<input type="text" id="'.md5($strHTMLControlName["VALUE"]).'" size="'.intval($arProperty["COL_COUNT"]).'" name="'.$strHTMLControlName["VALUE"].'" value="'.htmlspecialchars($value["VALUE"]).'">';
		 
		return  $html;
	}
	
	public static function GetPublicEditHTML($arProperty, $value, $strHTMLControlName)
	{
		return static::GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName);
	}	
}
?>