<?
/**
 * Подключение файлов Core
 * */
/**
 * @ignore
 * */
define("ART_CORE_NAME", "ART.Core");
define("ART_CORE_PATH", __DIR__);

require_once("files/files.autoload.php");
require_once(".cache.php");
require_once(".tools.php");
require_once(".tools.debug.php");
require_once(".tools.iblock.php");

require_once(".tools.menu.adapter.php");
require_once("handlers/handlers.inc.php");
require_once("files/files.inc.php");
require_once("storage/storage.inc.php");
require_once("iblock/iblock.inc.php");

require_once("mail/mail.inc.php");
require_once("buffer/template.buffer.inc.php");