<?
/**
 * CSiteTemplateTools
 */

/**
 * Вспомагательный класс для работы с шаблонами
 * @package template
 */
class CSiteTemplateTools
{
	/**
	 * Проверить на AJAX страницу
	 *
	 * @access public
	 *
	 * @return bool При успешном выполнении вернет "true", в противном случае "false"
	 *
	 */
	public static function IsAJAXPage()
	{
		return self::IsJqueryAJAXRequest() || self::IsBitrixAJAXRequest();
	}

	/**
	 * Проверить наличие jQuery AJAX запроса
	 *
	 * @access public
	 *
	 * @return bool При успешном выполнении вернет "true", в противном случае "false"
	 *
	 */
	public static function IsJqueryAJAXRequest()
	{
		return $_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest";
	}

	/**
	 * Проверить наличие Bitrix AJAX запроса
	 *
	 * @access public
	 *
	 * @return bool При успешном выполнении вернет "true", в противном случае "false"
	 *
	 */
	public static function IsBitrixAJAXRequest()
	{
		return !self::IsJqueryAJAXRequest() && isset($_REQUEST[BX_AJAX_PARAM_ID]);
	}

	/**
	 * Проверить текущий URL, является ли он страницей административного раздела
	 *
	 * @access public
	 *
	 * @return bool Вернет "true" если URL принадлежит административному разделу, в противном случае "false"
	 *
	 */
	public static function IsAdminPage()
	{
		return CSite::InDir("/bitrix/admin") || CSite::InDir("/bitrix/tools");
	}

	/**
	 * Проверить является ли страница главной
	 *
	 * @access public
	 *
	 * @param mixed $uri Путь к странице, по умолчанию false - текущая страница
	 *
	 * @return bool Вернет "true" если текущий страница главная, в противном случае "false"
	 *
	 */
	public static function IsMainPage($uri=false)
	{
		if(false == $uri)
			$uri = $GLOBALS["APPLICATION"]->GetCurPage();

		if(!defined("ERROR_404") && ($uri == "/index.php" || in_array(str_replace(SITE_DIR, "", $uri), array("", "index.php", "/"))))
			return true;

		return false;
	}

	/**
	 * Проверить является ли текущий шаблон, шаблоном для печати
	 *
	 * @access public
	 *
	 * @return bool Вернет "true" если текущий шаблон для печати, в противном случае "false"
	 */
	public static function IsPrintPage()
	{
		return SITE_TEMPLATE_ID == "print";
	}

	/**
	 * Проверить является ли текущая страница ошибочной (404)
	 *
	 * @access public
	 *
	 * @return bool Вернет "true" если текущая страница - 404, иначе "false"
	 */
	public static function IsPage404()
	{
		return (defined("ERROR_404") && ERROR_404 == "Y");
	}

	/**
	 * Установить заголовок 404 для страницы
	 *
	 * @access public
	 *
	 */
	public static function Set404()
	{
		//@define("ERROR_404", "Y");
		//CHTTP::SetStatus("404 Not Found");
		return false;
	}

	/**
	 * Получить отформатированный размер файла
	 *
	 * @access public
	 *
	 * @param int $size Размер файла
	 *
	 * @return string Вернет отформатированный размер файла
	 * @param int $decimals Количество файлов после запятой для форматирования размера файла
	 * @param string $dec_point Разделитель целой и дробной части для форматирования размера файла
	 * @param string $thousands_sep Разделитель тысяч для форматирования размера файла

	 * @example
	 * <pre>
	 * $filesize = "200000";
	 * echo CSiteTemplateTools::GetFormatFileSize($filesize);
	 * </pre>
	 */
	public static function GetFormatFileSize($size, $decimals = 2, $dec_point = ".", $thousands_sep = ",")
	{
		$a = array(
			GetMessage("FILE_SIZE_b"),
			GetMessage("FILE_SIZE_Kb"),
			GetMessage("FILE_SIZE_Mb"),
			GetMessage("FILE_SIZE_Gb"),
			GetMessage("FILE_SIZE_Tb"),
		);

		$pos = 0;
		while($size>=1024)
		{
			$size /= 1024; $pos++;
		}
		return number_format($size, $decimals, $dec_point, $thousands_sep)."&nbsp;".$a[$pos];
	}

	/**
	 * Получить расширение файла
	 *
	 * @access public
	 *
	 * @param array $arFile Массив с данными файла
	 *
	 * @return string Вернет расширение файла
	 *
	 */
	public static function GetFileExtention($arFile)
	{
		if(!is_array($arFile))
			$arFile = CFile::GetFileArray($arFile);
		if(!is_array($arFile))
			return;

		if(!is_array($arFile))
			$arFile = array("FILE_NAME" => trim($arFile, ". \r\n\t"));

		$pos = bxstrrpos($arFile["FILE_NAME"], ".");
		return $pos !== false ? ToLower(substr($arFile["FILE_NAME"], $pos+1)) : "";			
	}

	/**
	 * Обрезать строку по словам
	 *
	 * @param string $text Строка
	 * @param int $limit Количество символов слева, по умолчанию 120
	 * @param bool $bForceCut Указывает на обрезание строки, без сохранения последнего слова, по умолчанию false
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * echo CSiteTemplateTools::format_substr($str, 80);
	 * </pre>
	 */
	public static function Truncate($text, $limit = 100, $bForceCut=false)
	{
		
		if(class_exists("CTextParser")) {
			$obParser = new CTextParser;
			$text = $obParser->html_cut($text, $limit);
		}
		else
		{
			$wraper = "|".md5(time())."|";
			if(strlen($text) > $limit)
			{
				$_text = wordwrap(strip_tags($text), $limit, $wraper, false);
				$_text = explode($wraper, $_text);
				$text = rtrim($_text[0], ".,:;!@#$%^&*()_-~");
				if($bForceCut && strlen($text) > $limit)
					$text = substr($text, 0, $limit);
					$text .= "...";
			}
		}
		return $text;		
	}

	/**
	 * Заменить URL константы
	 *
	 * @access public
	 *
	 * @param string $url URL, в которой необходимо произвести замену
	 * @param array $variableAliases Массив соответствий для замены вида: Array("SECTION_ID" => 19). По умолчанию пустой массив
	 * @param bool $cropParams Указывает на обрезание GET параметров в URL, по умолчанию пустой false
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * echo CSiteTemplateTools::PrepareUrl($url, Array("ELEMENT_CODE" => "example"));
	 * </pre>
	 *
	 */
	public static function PrepareUrl($url, $variableAliases = array(), $cropParams = false)
	{

		$find = array(
			"#LANGUAGE_ID#",
			"#SITE_DIR#",

			"//",
		);

		$replace = array(
			LANGUAGE_ID,
			SITE_DIR,

			"/",
		);

		if (!empty($variableAliases)) {
			$keys = array_keys($variableAliases);
			$values = array_values($variableAliases);

			foreach ($keys as $k => $v)
				$keys[$k] = "#".$v."#";

			$find = array_merge($find, $keys);
			$replace = array_merge($replace, $values);
		}

		$url = str_replace($find, $replace, $url);

		if ($cropParams)
			$url = substr($url, 0, strpos($url, "?"));
		return $url;
	}

	/**
	 * Заменить URL константы, учитывая протокол и имя сайта
	 *
	 * @access public
	 *
	 * @param string $url URL, в которой необходимо произвести замену
	 * @param array $variableAliases Массив соответствий для замены вида: Array("SECTION_ID" => 19). По умолчанию пустой массив
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * echo CSiteTemplateTools::PrepareFullUrl($url, Array("ELEMENT_CODE" => "example"));
	 * </pre>
	 *
	 */
	public static function PrepareFullUrl($url, $variableAliases = array())
	{
		return (CMain::IsHTTPS() ? "https://" : "http://").SITE_SERVER_NAME.self::PrepareUrl($url, $variableAliases);
	}

	/**
	 * Возвращает префикс протокола (http:// или https://) для текущего запроса
	 *
	 * @access public
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * echo CSiteTemplateTools::GetProtocolPrefix();
	 * </pre>
	 *
	 */
	public static function GetProtocolPrefix()
	{
		return (CMain::IsHTTPS() ? "https://" : "http://");
	}

	public static function getIncludeFilePath($rel_path)
	{
		$sDir = rtrim($rel_path, "/");
		$pos = strrpos($sDir, "/");
		
		$arTemplatesPathes = array(
			BX_PERSONAL_ROOT,
			"/local",
		);		
		
		if($pos !== false)
		{
			$orig_path = $rel_path;
			foreach(array(SITE_ID, LANGUAGE_ID) as $lang)
			{
				$rel_path = substr($sDir, 0, $pos+1).$lang.substr($orig_path, $pos);
				if(substr($rel_path, 0, 1) != "/")
				{
					foreach ($arTemplatesPathes as $templatePath)
					{
						$path = $templatePath."/templates/".SITE_TEMPLATE_ID."/".$rel_path;
						if(!file_exists($_SERVER["DOCUMENT_ROOT"].$path))
						{
							$path = $templatePath."/templates/.default/".$rel_path;
							if(!file_exists($_SERVER["DOCUMENT_ROOT"].$path))
							{
								$path = $templatePath."/templates/".SITE_TEMPLATE_ID."/".$rel_path;
								$module_id = substr($rel_path, 0, strpos($rel_path, "/"));
								if(strlen($module_id)>0)
								{
									$path = "/bitrix/modules/".$module_id."/install/templates/".$rel_path;
									if(!file_exists($_SERVER["DOCUMENT_ROOT"].$path))
										$path = $templatePath."/templates/".SITE_TEMPLATE_ID."/".$rel_path;
								}
							}
						}
					}
				}
				else
					$path = $rel_path;

				if(file_exists($_SERVER["DOCUMENT_ROOT"].$path))
					break;
			}
			if(!file_exists($_SERVER["DOCUMENT_ROOT"].$path))
				$rel_path = $orig_path;
		}

		return $rel_path;
	}

	/**
	 * Подключить какой либо файл
	 *
	 * @access public
	 *
	 * @param string $rel_path Путь к файлу
	 * @param array $arParams Массив параметров для подключаемого файла
	 * @param array $arFunctionParams Массив настроек данной функции
	 *
	 * @return void
	 */
	public static function IncludeFile($rel_path, $arParams = Array(), $arFunctionParams = Array())
	{
		$rel_path = self::getIncludeFilePath($rel_path);
		return $GLOBALS["APPLICATION"]->IncludeFile($GLOBALS["APPLICATION"]->GetTemplatePath($rel_path), $arParams, array_merge(array("MODE" => "php"), $arFunctionParams));
	}

	/**
	 * Подключить flash файл
	 *
	 * @access public
	 *
	 * @param int $w Ширина области
	 * @param int $h Высота области
	 * @param string $contentId ID контейнера DOM, по умолчанию пусто
	 * @param array $xmlParams XML Flash переменные-параметры, по умолчанию пустой массив
	 * @param array $arParams Параметры Flash объекта, по умолчанию пустой массив
	 * @param string $callbackFn JS callback функция
	 * @param string $flashShowCondition JS условие отображения состояния, по умолчанию true
	 * @param string $flashShowConditionElse Операции, при возращении условия $flashShowCondition false, по умолчанию пусто
	 *
	 * @return bool Вернут false при ошибке
	 *
	 * @author Lexx
	 *
	 * @example
	 * <pre>
	 * CSiteTemplateTools::IncludeFlash(200, 300, "flash");
	 * </pre>
	 *
	 */
	public static function IncludeFlash($w, $h, $contentId = "", $xmlParams = array(), $arParams = array(), $callbackFn = false, $flashShowCondition = "true", $flashShowConditionElse = "") {

		self::JSInclude("swfobject");

		global $APPLICATION;

		$arParamsDefaults = array(
			"wmode" => "transparent",
			"allowScriptAccess" => "always",
			"allowFullScreen"=> "false",
			"menu" => "false"
		);

		$arParams = array_merge($arParamsDefaults, $arParams);

		$dir = $APPLICATION->GetCurDir();
		$page = $APPLICATION->GetCurPage();

		if ($page != $dir) {
			$dir = str_replace($dir, "", $page);
			$xmlParams["path"] = $APPLICATION->GetCurDir();
		}

		if (empty($contentId)) {

			if (substr($dir, 0, strlen(SITE_DIR)) == SITE_DIR) $dir = substr($dir, strlen(SITE_DIR)-1);

			$flashId = str_replace(array("/","."), "_", trim($dir, "/"));
			if ($flashId == "") $flashId = "index";

		}
		else
			$flashId = $contentId;

		$flashPath = "/bitrix/templates/.default/flash/".$flashId."/";
		$flashVars = "";


		if (file_exists($_SERVER['DOCUMENT_ROOT'].$flashPath."flash.swf")) {
			$flashFileName = "flash";
		}
		elseif (file_exists($_SERVER['DOCUMENT_ROOT'].$flashPath."flash_".LANGUAGE_ID.".swf")) {
			$flashFileName = "flash_".LANGUAGE_ID;
		}
		else {
			p("Flash файл не найден: ".$_SERVER['DOCUMENT_ROOT'].$flashPath."flash.swf");
			return false;
		}

		foreach ($xmlParams as $k => $v)
			$xmlParams[$k] = $k."=".$v;

		$xmlParamsStr = implode("&", $xmlParams);
		if (!empty($xmlParamsStr))
			$xmlParamsStr = "?".$xmlParamsStr;

		if (file_exists($_SERVER['DOCUMENT_ROOT'].$flashPath.$flashFileName.".swf")) {

			$dataFileName = "data";
			foreach (array("xml", "php") as $ext) {
				if (file_exists($_SERVER['DOCUMENT_ROOT'].$flashPath.$dataFileName.".".$ext)) {
					$flashVars = "'path': '".($flashPath.$dataFileName.".".$ext.$xmlParamsStr)."'";
				}
				elseif (file_exists($_SERVER['DOCUMENT_ROOT'].$flashPath.$dataFileName."_".LANGUAGE_ID.".".$ext)) {
					$flashVars = "'path': '".($flashPath.$dataFileName."_".LANGUAGE_ID.".".$ext.$xmlParamsStr)."'";
				}
			}

			if (!empty($flashVars))
				$flashVars .= ", ";

			$flashVars .= "'lang': '".LANGUAGE_ID."'";

			if (!isset($GLOBALS["FLASH_SESSION_KEY"]))
				$GLOBALS["FLASH_SESSION_KEY"] = "ID".RandString(5);

			$flashVars .= ", 'flashkey': '".$GLOBALS["FLASH_SESSION_KEY"]."'";

			if (file_exists($_SERVER['DOCUMENT_ROOT'].$flashPath."translate"."_".LANGUAGE_ID.".xml")) {
				$flashVars .= ", 'translatePath': '".$flashPath."translate"."_".LANGUAGE_ID.".xml"."'";
			}

			$callbackFnStr = "";

			if (!empty($callbackFn)) {
				$functionName = "func_".ToLower(RandString(10));
				$callbackFnStr  = "function ".$functionName."(e) { ".$callbackFn." };";
				$callbackFn = $functionName;
			}

			$mTime = filemtime($_SERVER['DOCUMENT_ROOT'].$flashPath.$flashFileName.".swf");

			$str = '
				<script type="text/javascript">
					if ('.$flashShowCondition.') {
						var flashvars = {'.$flashVars.'};
						'.$callbackFnStr.'
						var params = '.trim(CUtil::PhpToJSObject($arParams)).';
						swfobject.embedSWF("'.($flashPath.$flashFileName.".swf?".$mTime.(debug()?"&nocache=".RandString():"")).'", "'.$flashId.'", "'.$w.'", "'.$h.'", "9.0.0", "http://swfobject.googlecode.com/svn/trunk/swfobject/expressInstall.swf", flashvars, params, null, '.($callbackFn?$callbackFn:"false").');
					}
					else {
						'.$flashShowConditionElse.'
					}
				</script>
			';

			$alternativeHTML = GetMessage("FLASH_NO_FLASH");

			if (file_exists($_SERVER['DOCUMENT_ROOT'].$flashPath."noflash"."_".LANGUAGE_ID.".html")) {
				$alternativeHTML = file_get_contents($_SERVER['DOCUMENT_ROOT'].$flashPath."noflash"."_".LANGUAGE_ID.".html");
			}

			if (empty($contentId)) $str .= '<div style="width: '.(strstr($w,"%")?$w:$w."px").'; height: '.(strstr($h,"%")?$h:$h."px").'; overflow: hidden" id="'.$flashId.'_parent"><div id="'.$flashId.'">'.$alternativeHTML.'</div></div>';

			echo $str;
		}
		else {
			p("Не найден ". $_SERVER['DOCUMENT_ROOT'].$flashPath);
			return false;
		}

	}

	/**
	 * Вывести панель управления Битрикса
	 *
	 * @access public
	 * @return string Вернет код панели управления
	 *
	 */
	public static function ShowPanel()
	{
		echo "<div id=\"bx-panel-wrapper\" style=\"position: relative; z-index: 100500 !important; top: 0 !important; left: 0 !important; width: 100% !important;\">",$GLOBALS["APPLICATION"]->ShowPanel(),"</div>";
	}

	/**
	 * Обернуть строку в ссылку
	 *
	 * @access public
	 *
	 * @param array $item Массив со значениями элемента
	 * @param string $linkKey Ключ массива, где хранится ссылка, по умолчанию DETAIL_PAGE_URL
	 * @param string $attr Атрибуты тега a, по умолчанию false
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * echo CSiteTemplateTools::GetIBlockItemNameLink($arItem);
	 * </pre>
	 *
	 */
	public static function GetIBlockItemNameLink(array $item, $linkKey = "DETAIL_PAGE_URL", $attr = '')
	{
		return self::MakeNameLink($item["NAME"], self::PrepareUrl($item[$linkKey]), $attr);
	}

	/**
	 * Обернуть строку используя двойные пробелы
	 *
	 * @access public
	 *
	 * @param string $name Строка, которую необходимо обернуть
	 * @param string $part1 Строка, которая будет находиться по левую строну $name
	 * @param string $part2 Строка, которая будет находиться по правую строну $name
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * echo CSiteTemplateTools::WrapName('Иван Иванов', '<a href="user.php">', '</a>');
	 * </pre>
	 *
	 */
	public static function WrapName($name, $part1, $part2)
	{
		$search = str_pad(" ", 2);

		$result = $part1.$name.$part2;

		$pos = strpos($name, $search);
		$count = substr_count($name, $search);

		if ($count==1) {
			if ($pos < strlen($name)/2)
				$result = '[a]'.str_replace($search, "[/a] ", $name);
			else
				$result = str_replace($search, " [a]", $name)."[/a]";
		}
		elseif ($count==2) {
			$name = trim(substr($name, 0, $pos+2))." [a]".substr($name, $pos+2);
			$result = str_replace($search, "[/a] ", $name);
		}
		return str_replace(array("[a]", "[/a]"), array($part1, $part2), $result);
	}

	/**
	 * Обернуть строку в ссылку
	 *
	 * @access public
	 *
	 * @param string $name Строка, которую необходимо обернуть
	 * @param string $link URL Ссылки
	 * @param string $attr Атрибуты тега a, по умолчанию false
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * echo CSiteTemplateTools::MakeNameLink('Иван Иванов', 'user.php');
	 * </pre>
	 *
	 */
	public static function MakeNameLink($name, $link, $attr = '')
	{
		return self::WrapName($name, '<a href="'.$link.'" '.$attr.'>', '</a>');
	}

	/**
	 * Получить по коду соответствующее сообщение на текущем языке. В функцию уже добавлена замена шаблонов LANGUAGE_ID и SITE_DIR. Это функция-обертка, позволяющая НЕ СТАВИТЬ символы # в массиве для замены
	 *
	 * @access public
	 *
	 * @param string $name Код сообщения
	 * @param array $aReplace Массив для замены шаблонов в строке
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * echo CSiteTemplateTools::GetMessage("PRICE_MESS", Array("PRICE"=>24000));
	 * </pre>
	 *
	 */
	public static function GetMessage($name, $aReplace=false)
	{
		global $MESS;
		$s = $MESS[$name];

		if(!is_array($aReplace))
			$aReplace = array();

		$aReplace["LANGUAGE_ID"] = LANGUAGE_ID;
		$aReplace["SITE_DIR"] = SITE_DIR;

		if(is_array($aReplace)) {
			foreach($aReplace as $search=>$replace)
				$s = str_replace("#".$search."#", $replace, $s);
		}

		return $s;
	}

	/**
	 * Получает строку с верным окончанием
	 *
	 * @access public
	 *
	 * @param int $number Число объектов
	 * @param string $form1 Окончание в единственном числе
	 * @param string $form2 Окончание в промежутке 2...4
	 * @param string $form5 Окончание в множественном числе
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * echo 'Я знаю '.CSiteTemplateTools::EndingsForm(5, 'иностранный язык', 'иностранных языка', 'иностранных языков');
	 * </pre>
	 *
	 */
	public static function EndingsForm($number, $form1, $form2, $form5)
	{
		$titles = array($form1, $form2, $form5);
		$cases = array (2, 0, 1, 1, 1, 2);
		return $titles[ ($number%100>4 && $number%100<20)? 2 : $cases[min($number%10, 5)] ];		
	}

	/**
	 * Получить данные о файле и папке, в которой он находится
	 *
	 * @access public
	 *
	 * @param mixed $mixed ID файла или массив с параметрами файла
	 * @param int $decimals Количество файлов после запятой для форматирования размера файла
	 * @param string $dec_point Разделитель целой и дробной части для форматирования размера файла
	 * @param string $thousands_sep Разделитель тысяч для форматирования размера файла
	 *
	 * @return array|bool Вернет массив значений при успешном выполнении, false при ошибке
	 *
	 */
	public static function GetFileInfo($mixed, $decimals = 2, $dec_point = ".", $thousands_sep = " ")
	{
		if (intval($mixed)>0) {
			if ($res = CFile::GetById($mixed)->Fetch()) {
				$res["SRC"] = CFile::GetPath($mixed);
			}
		}
		elseif(file_exists($_SERVER['DOCUMENT_ROOT'].$mixed)) {
			$res["SRC"] = $mixed;
		}

		if (!empty($res["SRC"])) {
			$res["INFO"] = pathinfo($_SERVER['DOCUMENT_ROOT'].$res["SRC"]);
			$res["EXTENSION"] = ToLower($res["INFO"]["extension"]);
			$res["FILE_SIZE"] = filesize($_SERVER['DOCUMENT_ROOT'].$res["SRC"]);
			$res["FILE_SIZE_EXT"] = self::GetFormatFileSize($res["FILE_SIZE"], $decimals, $dec_point, $thousands_sep);

			if(IsModuleInstalled("statistic"))
				$res["DOWNLOAD_LINK"] = htmlspecialcharsbx("/bitrix/redirect.php?event1=".urlencode("download")."&event2=".urlencode($res["SRC"])."&event3=".urlencode($res["ORIGINAL_NAME"])."&goto=".urlencode($res["SRC"]));
			else
				$res["DOWNLOAD_LINK"] = htmlspecialcharsbx($res["SRC"]);

			return $res;
		}
		return false;
	}

	/**
	 * Пробразует кавычки (") в &laquo; и &raquo;
	 *
	 * @access public
	 *
	 * @param string $str Строка, подлежащая кодированию
	 *
	 * @return string
	 *
	 */
	public static function formatQuotes($str)
	{
		return preg_replace('#"{1}([^"]+)"{1}#', '&laquo;$1&raquo;', str_replace("&quot;", "\"", $str));
	}

	/**
	 * Вывести уменьшенную картинку
	 *
	 * @access public
	 *
	 * @param mixed $strImage ID файла или массив с параметрами файла
	 * @param int $iMaxW Максимальная ширина картинки, по умолчанию 0
	 * @param int $iMaxH Максимальная высота картинки, по умолчанию 0
	 * @param string $sParams HTML атрибуты, по умолчанию null
	 * @param string $resizeType Тип масштабирования:<br/>BX_RESIZE_IMAGE_PROPORTIONAL - масштабирует с сохранением пропорций<br/>BX_RESIZE_IMAGE_PROPORTIONAL_ALT - масштабирует с сохранением пропорций, улучшенна обработка вертикальных картинок<br/>BX_RESIZE_IMAGE_EXACT - масштабирует в прямоугольник, без сохранения пропорций<br/>по умолчанию BX_RESIZE_IMAGE_PROPORTIONAL
	 * @param bool $bPopup Открывать ли при клике на изображении дополнительное popup окно с увеличенным изображением, по умолчанию false
	 * @param string $sPopupTitle Текст атрибута title у ссылки увеличивающей картинку, по умолчанию false
	 * @param array $arFilters Массив для постобработки картинки с помощью фильтров, по умолчанию false
	 * @param string $sPopupHtml popup addon html, по умолчанию пусто
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * echo CSiteTemplateTools::ShowImage($id_image, 300, 400);
	 * </pre>
	 */
	public static function ShowImage($strImage, $iMaxW=0, $iMaxH=0, $sParams=null, $resizeType=BX_RESIZE_IMAGE_PROPORTIONAL, $bPopup=false, $sPopupTitle=false, $arFilters=false, $sPopupHtml="")
	{
		$arImgParams = null;
		if(is_array($strImage))
			$arImgParams = $strImage;
		elseif(is_numeric($strImage))
			$arImgParams = CFile::GetFileArray($strImage);
		elseif(substr($strImage, 0, 1) == "/" || preg_match("#^https?://#", $strImage))
		{
			if(preg_match("#^https?://#", $strImage) ||  substr($strImage, 0, 2) == "//" )
			{
				if (substr($strImage, 0, 2))
					$strImage = (CMain::IsHTTPS()?"https:":"http:").$strImage;

				$arFile = CFile::MakeFileArray($strImage);
				$strImage = str_replace($_SERVER["DOCUMENT_ROOT"], "", $arFile["tmp_name"]);
			}

			$upload_dir = COption::GetOptionString("main", "upload_dir", "upload");
			if(substr($strImage, 0, strlen($upload_dir) + 2) == "/".$upload_dir."/")
			{
				$file = $_SERVER['DOCUMENT_ROOT'].$strImage;
				if(file_exists($file))
				{
					$path = pathinfo($file);
					$subdir = substr(removeDocRoot($path["dirname"]), strlen($upload_dir) + 2);
					$imageSize = CFile::GetImageSize($file);

					$arImgParams = array(
						"SRC" => $strImage,
						"FILE_NAME" => bx_basename($file),
						"SUBDIR" => $subdir,
						"WIDTH" => $imageSize[0],
						"HEIGHT" => $imageSize[1],
						"CONTENT_TYPE" => CFile::GetContentType($file, true)
					);
				}
			}
			else
				return CFile::ShowImage($strImage, $iMaxW, $iMaxH, ($bPopup ? "" : $sParams), false);
		}
		else
			return CFile::ShowImage($strImage, $iMaxW, $iMaxH, ($bPopup ? "" : $sParams), false);

		if(!$arImgParams)
			return "";

		$arFile = CFile::ResizeImageGet($arImgParams, array("width" => $iMaxW, "height" => $iMaxH), $resizeType, false, $arFilters);

		$result = CFile::ShowImage($arFile["src"], $iMaxW, $iMaxH, ($bPopup ? "" : $sParams), false);

		if($bPopup && $result)
		{
			if($sPopupTitle === false)
				$sPopupTitle = GetMessage('FILE_ENLARGE');

			$result = '<a title="'.htmlspecialcharsEx($sPopupTitle).'" '.$sParams.' href="'.$arImgParams["SRC"].'">'.$result.$sPopupHtml.'</a>';
		}

		return $result;
	}

	public static function showCaptcha()
	{
		$cc = htmlspecialchars($GLOBALS["APPLICATION"]->CaptchaGetCode());
		?><input type="hidden" name="captcha_sid" value="<? echo $cc ?>" /><?
		?><img src="/bitrix/tools/captcha.php?captcha_sid=<? echo $cc ?>" width="180" height="40" /><?
	}

	/**
	 * Получить ключ API Яндекс.Карты
	 *
	 * @access public
	 *
	 * @return string
	 *
	 * @example
	 *
	 */
	public static function GetYandexMapKey()
	{
		return CDFAYandexMap::GetInstance()->GetKey();
	}

	/**
	 * Инициализировать API Яндекс.Карты
	 *
	 * @access public
	 *
	 * @param string $key API ключ
	 * @param string $version версия API, по умолчанию 1.0
	 *
	 * @return void
	 *
	 * @example
	 * <pre>
	 * CSiteTemplateTools::InitYandexMapAPI($key_api);
	 * </pre>
	 *
	 */
	public static function InitYandexMapAPI($key=null, $version='1.0')
	{
		return CDFAYandexMap::GetInstance()->InitAPI($key, $version);
	}

	/**
	 * Получить данные о файле
	 *
	 * @access public
	 *
	 * @param mixed $arFile ID файла или массив с параметрами файла
	 *
	 * @return string
	 *
	 * @example
	 *
	 */
	public static function getDisplayFileInfo($arFile)
	{
		if(!is_array($arFile))
			$arFile = CFile::GetFileArray($arFile);
		if(!is_array($arFile))
			return;

		return '('.toUpper(self::GetFileExtention($arFile)).', '.self::GetFormatFileSize($arFile["FILE_SIZE"]).')';
	}

	/**
	 *
	 * Функция позволяет реализовать вместо стандартной постранички кнопку "Показать ещё"
	 * (вкупе с соответствующим шаблоном постранички, который, фактически, просто содержит ссылку на следующую относительно текущей страницу)
	 * @param $num
	 * @example
	 * <pre>
	 * в параметрах компонента "NEW_COUNT" => CDFASiteTemplateTools::Pager(20)
	 * <br>
	 * + не забыть выключить AJAX-режим и сохранение истории в строке браузера (AJAX_OPTION_HISTORY)
	 * </pre>
	 *
	 */
	public function Pager($num)
	{
		$num = intval($num);

		if ($num <= 0)
			$num = 20;

		$url = $GLOBALS["APPLICATION"]->GetCurPage();
		$navNum = $GLOBALS["NavNum"]+1;

		if (!self::IsPrintPage() && !self::IsAJAXPage() && !is_set($_REQUEST, "sort"))
			unset($_SESSION["ITEMS_PER_PAGE"]);

		if (isset($GLOBALS["PAGEN_".$navNum])) {
			$GLOBALS["NEW_PAGEN_".$navNum] = $GLOBALS["PAGEN_".$navNum];
			$num *= $GLOBALS["PAGEN_".$navNum];
			$_SESSION["ITEMS_PER_PAGE"][$url][$navNum] = $num;
			unset($GLOBALS["PAGEN_".$navNum]);
		}

		return $num;
	}


	/**
	 *
	 * Функция позволяет получить позицию элемента в списке с постраничной навигацией
	 * @param string $ID элемента
	 * @param array $arParams
	 * @param string $strNavNum = 1
	 * @return ?PAGEN_1=значение
	 * @example
	 * <pre>
	 * CDFASiteTemplateTools::GetFullListPageURLByElementID($ElementID, $arParams)
	 * </pre>
	 *
	 */
	public static function GetFullListPageURLByElementID($ID, array $arParams, $strNavNum = 1, $arFilter = array())
	{

		$filter = array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"IBLOCK_LID" => SITE_ID,
			"ACTIVE" => "Y",
			"CHECK_PERMISSIONS" => "Y",
		);

		if($arParams["CHECK_DATES"]!="N")
			$filter["ACTIVE_DATE"] = "Y";

		$rs = CIBlockElement::GetList(
			array(
				$arParams["SORT_BY1"] => $arParams["SORT_ORDER1"],
				$arParams["SORT_BY2"] => $arParams["SORT_ORDER2"]
			),
			array_merge($filter, $arFilter),
			false,
			false,
			array("ID")
		);

		while ($res = $rs->Fetch()) {

			$i++;
			if ($res["ID"] == $ID)
				break;
		}

		$page = ceil($i/$arParams["NEWS_COUNT"]);

		if ($page>1)
			return "?PAGEN_$strNavNum=".$page;

		return "";
	}
	
	/**
	 * Возвращает ID инфоблока по его коду
	 *
	 * @access public
	 *
	 * @param string $code Код инфоблока
	 * @param string $iblockType ID типа инфоблока, по умолчанию null
	 * @param string $site_id ID сайта, в котором искать инфоблок, по умолчанию SITE_ID
	 *
	 * @return int
	 *
	 * @example
	 * <pre>
	 * $code = "stickers";
	 * $iblockType = "services";
	 * $id = CSiteTemplateTools::GetIDByCode($code, $iblockType, $site_id);
	 * </pre>
	 *
	 */
	public static function GetIDByCode($code, $iblockType=null, $site_id=SITE_ID)
	{
		if(!CModule::IncludeModule("iblock"))
		{
			$GLOBALS["APPLICATION"]->ThrowException('Модуль инфоблоков не установлен');
			return false;
		}
		else
		{
			if(
				/*(defined("ADMIN_SECTION") && ADMIN_SECTION == true)
				 || */empty($site_id)
				|| !strlen($code)
				)
			return 0;
	
			$cache = new CARTCache(__CLASS__.'::'.__FUNCTION__.'|'.$site_id.'|'.$code.'|'.$iblockType);
			$result = $cache->init(false);
			if(null == $result)
			{
				$arFilter = array(
						"ACTIVE"	=> "Y",
						"SITE_ID"	=> $site_id,
						"CODE"		=> $code,
						"MIN_PERMISSION"=> "R"
				);

				if(null != $iblockType)
					$arFilter["TYPE"] = $iblockType;
					$result = 0;
					$rs = CIBlock::GetList(array(), $arFilter, false);
					if($ar = $rs->Fetch())
						$result = $ar["ID"];

						$cache->set($result);
			}
			return $result;
		}
	}

}
?>