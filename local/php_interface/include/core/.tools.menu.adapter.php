<?
/**
* build tree menu
* @package menu
*/
class CDFAMenuItemsAdapter
{
	private $index = 0;
	private $itemsTree;
	private $depthArrayKey;

	public function __construct($itemsTree, $depthArrayKey = "DEPTH_LEVEL")
	{
		$this->itemsTree = $itemsTree;
		$this->depthArrayKey = $depthArrayKey;
	}

	public function getChilds($currentDepth = 1)
	{
		$currentDepth = intval($currentDepth);

		$childs = array();
		while(is_array($this->itemsTree[$this->index]))
		{
			if($this->getDepth() == $currentDepth)
				$childs[] = $this->itemsTree[$this->index++];
			elseif($this->getDepth() > $currentDepth)
				$childs[count($childs) - 1]["CHILDS"] = $this->getChilds($this->getDepth());
			else
				break;
		}
		return $childs;
	}

	private function getDepth()
	{
		return $this->itemsTree[$this->index] ? $this->itemsTree[$this->index][$this->depthArrayKey] : 0;
	}
}
?>