<?
/**
 * CARTCache
 * */

/**
 * Объект для работы с кэшем
 *
 * @example
 * <pre>
 * $arFilter = array("IBLOCK_ID" => 1, "ACTIVE" => "Y");
 * $cache = new CARTCache($arFilter, 86400, "iblock_getlist");
 * $result = $cache->Init();
 *
 * if(null == $result)
 * {
 * 	$result = array();
 * 	$rs = CIBlockElement::GetList(array(), $arFilter);
 * 	if($result = $rs->Fetch())
 * 		$cache->set($result);
 * 	else
 * 		$cache->abort();
 * }
 * </pre>
 */
class CARTCache
{
	/**
	 * @access protected
	 * @var CPHPCache Экземпляр объекта для кэширования
	 * */
	protected $cache;

	/**
	 * @access protected
	 * @var bool Указывает на использование тэгированного кэша
	 * */
	protected $bUseTags = true;

	/**
	 * @access private
	 * @var string Идентификатор кэша
	 * */
	private $cid;

	/**
	 * @access private
	 * @var int Время кэширования
	 * */
	private $time = 3600;

	/**
	 * @access private
	 * @var string Расположение файлов кэша
	 * */
	private $path;

	/**
	 * Конструктор класса
	 *
	 * @access public
	 *
	 * @param mixed $cacheID Идентификатор кэша
	 * @param int $cacheTime Время кэширования
	 * @param string $path Расположение файлов кэша
	 * @param bool $bUseTags Указывает на использование тэгированного кэша
	 *
	 * @return void
	 * */
	public function __construct($cacheID, $cacheTime=3600, $path="art_cache_path", $bUseTags=null)
	{
		$this->cache = new CPHPCache();

		$this->setCacheID($cacheID);
		$this->time = intval($cacheTime);
		$this->path = preg_replace("/[^\da-z_]+/i", "", $path);
		if(null != $bUseTags)
			$this->setUseTags($bUseTags);
	}

	/**
	 * Устанавливает использовать ли тэгированный кэш
	 *
	 * @access public
	 *
	 * @param bool $value Указывает на использование тэгированного кэша
	 *
	 * @return void
	 * */
	public function setUseTags($value)
	{
		$this->bUseTags = $value == true;
	}

	/**
	 * Сообщает об использовании тэгированного кэша
	 *
	 * @access public
	 *
	 * @return bool
	 * */
	public function isUsedTags()
	{
		return $this->bUseTags;
	}

	/**
	 * Устанавливает идентификатор кэша
	 *
	 * @access public
	 *
	 * @param mixed $cacheID Идентификатор кэша
	 *
	 * @return void
	 * */
	public function setCacheID($cacheID)
	{
		if(is_array($cacheID))
		{
			$arCacheParams = array();
			foreach($cacheID as $key => $value) if(substr($key, 0, 1) != "~") $arCacheParams[$key] = $value;
			$this->cid = md5(serialize($arCacheParams));
		}
		else
		{
			$this->cid = $cacheID;
		}
	}

	/**
	 * Возвращает идентификатор кэша
	 *
	 * @access public
	 *
	 * $return string
	 * */
	public function getCacheID()
	{
		return $this->cid;
	}

	/**
	 * Возвращает время кэширования
	 *
	 * @access public
	 *
	 * @return int
	 * */
	public function getCacheTime()
	{
		return $this->time;
	}

	/**
	 * Возвращает расположение файлов кэша
	 *
	 * @access public
	 *
	 * @return string
	 * */
	public function getPath()
	{
		return $this->path;
	}

	/**
	 * Возвращает закэшированный объект
	 *
	 * @access public
	 *
	 * @return null|false|array
	 * */
	public function getVars()
	{
		if($this->cache->InitCache($this->getCacheTime(), $this->getCacheID(), $this->getPath()))
		{
			return $this->cache->GetVars();
		}
		return null;
	}

	/**
	 * Возвращает закэшированный контент
	 *
	 * @access public
	 *
	 * @return null|string
	 * */
	public function getContent()
	{
		if($this->cache->InitCache($this->getCacheTime(), $this->getCacheID(), $this->getPath()))
		{
			return $this->cache->content;
		}
		return null;
	}

	/**
	 * Устанавливает объект для кэширования.
	 * Останавливает кеширование.
	 *
	 * @access public
	 *
	 * @param mixed $data Объект для кэширования
	 *
	 * @return void
	 * */
	public function set($data=false)
	{
		$this->cache->EndDataCache($data);
		$this->stopTagCache();
	}

	/**
	 * Отменяет процесс кэширования данных
	 *
	 * @access public
	 *
	 * @return void
	 * */
	public function cancel()
	{
		$this->cache->AbortDataCache();
		$this->stopTagCache();
	}

	/**
	 * Псевдоним функции {@link CARTCache::cancel()}
	 *
	 * @access public
	 *
	 * @return void
	 * */
	public function abort()
	{
		return $this->cancel();
	}

	/**
	 * Начинает процесс кэширования данных.
	 * Запускает механизм тегированного кэширования.
	 *
	 * @access public
	 *
	 * @return bool
	 * */
	public function start()
	{
		if($this->cache->StartDataCache())
		{
			$this->startTagCache();
			return true;
		}
		return false;
	}

	/**
	 * Очищает файлы кеша по текущему идентификатору и пути хранения файлов
	 *
	 * @access public
	 *
	 * @return bool
	 * */
	public function clean()
	{
		return $this->cache->clean($this->getCacheID(), $this->getPath());
	}

	/**
	 * Устанавливает состояние процесса тэгированного кэширования
	 *
	 * @access public
	 *
	 * @param bool $state Состояние тэгированного кэширования
	 *
	 * @return bool
	 * */
	public function setTagCache($state=false)
	{
		if(!$this->isUsedTags())
			return true;

		global $CACHE_MANAGER;

		if(!defined("BX_COMP_MANAGED_CACHE") || !is_object($CACHE_MANAGER))
			return false;

		if($state)
			$CACHE_MANAGER->StartTagCache($this->getPath());
		else
			$CACHE_MANAGER->EndTagCache();

		return true;
	}

	/**
	 * Запускает процесс тэгированного кэширования
	 *
	 * @access public
	 *
	 * @return bool
	 * */
	public function startTagCache()
	{
		return $this->setTagCache(true);
	}

	/**
	 * Останавливает процесс тэгированного кэширования
	 *
	 * @access public
	 *
	 * @return bool
	 * */
	public function stopTagCache()
	{
		return $this->setTagCache(false);
	}

	/**
	 * Инициализация кэширования данных.
	 * Возвращает null, в случае, если закэшированные данные не найдены.
	 *
	 * @access public
	 *
	 * @param bool $bUseTags Указывает на использование тэгированного кэша
	 *
	 * @return null|mixed
	 * */
	public function init($bUseTags=true)
	{
		$this->setUseTags($bUseTags);
		$data = $this->getVars();

		$this->start();
		return $data;
	}

	/**
	 * Инициализация кэширования контента.
	 * Возвращает null, в случае, если закэшированный контент не найден.
	 *
	 * @access public
	 *
	 * @param bool $bUseTags Указывает на использование тэгированного кэша
	 *
	 * @return null|string
	 * */
	public function initContent($bUseTags=true)
	{
		$this->setUseTags($bUseTags);
		$data = $this->getContent();
		if(null != $data) return $data;

		$this->start();
		return null;
	}

	/**
	 * Регистрирует ключ для тэгированного кэша
	 *
	 * @access public
	 *
	 * @param string $tagName Ключ для тэгированного кэша
	 *
	 * @return void
	 * */
	public function registerTag($tagName)
	{
		if($this->isUsedTags())
			$GLOBALS["CACHE_MANAGER"]->RegisterTag($tagName);
	}
}

?>