<?
/**
 * CARTIBlockTools
 * */

/**
 * Содержит вспомогательные методы для работы с информационными блоками
 *
 * @package tools
 */
class CARTIBlockTools
{
	/**
	 * Копирует свойства из инфоблока в инфоблок
	 *
	 * @access public
	 *
	 * @param int $FROM_IBLOCK_ID ID инфоблока, из которого копируем
	 * @param int $TO_IBLOCK_ID ID инфоблока, в который копируем
	 * @param array $arProperties Массив с кодами свойств, подлежащие копированию
	 *
	 * @return void
	 *
	 */
	function syncIblockProperties($FROM_IBLOCK_ID, $TO_IBLOCK_ID, $arProperties=array())
	{
		if(!CModule::IncludeModule("iblock"))
		{
			$GLOBALS["APPLICATION"]->ThrowException('Модуль инфоблоков не установлен');
			return false;
		}

		$arUpdateProperties = array();
		$rsProperty = CIBlockProperty::GetList(array(), array("ACTIVE" => "Y", "IBLOCK_ID" => $FROM_IBLOCK_ID));
		while($arProperty = $rsProperty->Fetch())
		{
			if(in_array($arProperty["CODE"], $arProperties) || in_array($arProperty["ID"], $arProperties) || empty($arProperties))
			$arUpdateProperties[] = $arProperty;
		}
		foreach($arUpdateProperties as $arProperty)
		{
			$arProperty["IBLOCK_ID"] = $TO_IBLOCK_ID;
			$arProperty["XML_ID"] = "PROP_".$arProperty["ID"];

			$ibp = new CIBlockProperty;
			$rs = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $arProperty["IBLOCK_ID"], "XML_ID" => $arProperty["XML_ID"]));
			if($ar = $rs->Fetch())
			{
				$ibp->Update($ar["ID"], $arProperty);
			}
			else
			{
				if( !($ID = $ibp->Add($arProperty)) )
				{
					$GLOBALS["APPLICATION"]->ThrowException('Ошибка создния свойства: '.$ibp->LAST_ERROR);
				}
			}

			$arUpdateEnums = array();
			if($ID && $arProperty["PROPERTY_TYPE"] == 'L')
			{
				$rs = CIBlockPropertyEnum::GetList(array("SORT"=>"ASC"), array("PROPERTY_ID" => $arProperty["ID"]));
				while($ar = $rs->Fetch())
				{
					$ar["IBLOCK_ID"] = $arProperty["IBLOCK_ID"];
					$ar["PROPERTY_ID"] = $ID;
					$ar["XML_ID"] = "ENUM_".$ar["ID"];
					$arUpdateEnums[] = $ar;
				}
			}
			foreach($arUpdateEnums as $arEnum)
			{
				$ibpenum = new CIBlockPropertyEnum;
				$rs = CIBlockPropertyEnum::GetList(array(), array("PROPERTY_ID" => $ID, "XML_ID" => $arEnum["XML_ID"]));
				if($ar = $rs->Fetch())
				{
					$ibpenum->Update($ar["ID"], $arEnum);
				}
				else
				{
					if( !($ibpenum->Add($arEnum)) )
					{
						$GLOBALS["APPLICATION"]->ThrowException('Ошибка добавления значения свойства типа список свойства');
					}
				}
			}
		}
	}

	/**
	 * Копирует элемент инфоблока в инфоблок
	 *
	 * @access public
	 *
	 * @param int $ID ID элемента
	 * @param int $TO_IBLOCK_ID ID инфоблока, в который копируем элемент
	 * @param bool $bSyncProps Указывает на копирование свойств элемента, по умолчанию false
	 * @param bool $bCheckUniq Проверять на уникальность при добавлении, по умолчанию true
	 *
	 * @return int
	 *
	 * @example
	 * <pre>
	 * $ID = 12;
	 * $TO_BID = 3;
	 * $bSyncProps = true;
	 * $pid = CARTIBlockTools::syncIblockElement($ID, $TO_BID, $bSyncProps);
	 * </pre>
	 *
	 */
	public static function syncIblockElement($ID, $TO_IBLOCK_ID, $bSyncProps=false, $bCheckUniq=true)
	{
		if(!CModule::IncludeModule("iblock"))
		{
			$GLOBALS["APPLICATION"]->ThrowException('Модуль инфоблоков не установлен');
			return false;
		}
		$arElement = CIBlockElement::GetList(
			array(),
			array(
				"ID" => intval($ID)
			),
			false,
			false,
			array(
				"ID",
				"IBLOCK_ID",
				"NAME",
				"CODE",
				"PREVIEW_TEXT",
				"PREVIEW_TEXT_TYPE",
				"DETAIL_TEXT",
				"DETAIL_TEXT_TYPE",
				"DATE_CREATE",
				"CREATED_BY",
				"DATE_ACTIVE_FROM",
				"DATE_ACTIVE_TO",
				"BP_PUBLISHED"
			)
		)->Fetch();

		if(!$arElement)
		{
			$GLOBALS["APPLICATION"]->ThrowException('Элемент не найден');
			return false;
		}
		$arIblock = CIBlock::GetByID($TO_IBLOCK_ID)->Fetch();
		if(!$arIblock)
		{
			$GLOBALS["APPLICATION"]->ThrowException('Инфоблок назначения не найден');
			return false;
		}
		if($bSyncProps)
			self::syncIblockProperties($arElement["IBLOCK_ID"], $arIblock["ID"]);

		$bBizproc = CModule::IncludeModule('bizproc') && $arIblock["BIZPROC"] == "Y";
		$bWorkflow = CModule::IncludeModule("workflow") && $arIblock["WORKFLOW"] == "Y";

		$arFields = $arElement;
		unset($arFields["ID"]);

		$arFields["IBLOCK_ID"] = $arIblock["ID"];
		$arFields["XML_ID"] = 'IB_ITEM_'.$arElement["ID"];
		$arFields["PROPERTY_VALUES"] = array();

		$arUpdateProperties = array();
		$rsProperty = CIBlockProperty::GetList(array(), array("ACTIVE" => "Y", "IBLOCK_ID" => $TO_IBLOCK_ID));
		while($arProperty = $rsProperty->Fetch())
		{
			// Копирование в пределах одного ИБ
			if($arElement["IBLOCK_ID"] == $TO_IBLOCK_ID)
				$arProperty["XML_ID"] = "PROP_".$arProperty["ID"];

			if(!empty($arProperty["XML_ID"]))
			{
				if($arProperty["PROPERTY_TYPE"] == 'L')
				{
					$arProperty["VALUES"] = array();
					$rs = CIBlockPropertyEnum::GetList(array(), array("PROPERTY_ID" => $arProperty["ID"]));
					while($ar = $rs->Fetch())
					$arProperty["VALUES"][$ar["ID"]] = $ar["VALUE"];
				}
				$arUpdateProperties[$arProperty["XML_ID"]] = $arProperty;
			}
		}
		$dbPropV = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["ID"], "sort", "asc", array("ACTIVE" => "Y", "EMPTY" => "N"));
		while($arPropV = $dbPropV->Fetch())
		{
			if(is_set($arUpdateProperties, "PROP_".$arPropV["ID"]))
			{
				$arUpdateProperty = $arUpdateProperties["PROP_".$arPropV["ID"]];
				if($arUpdateProperty["PROPERTY_TYPE"] == "F")
				{
					$arPropV["VALUE"] = CFile::CopyFile($arPropV["VALUE"]);
				}
				elseif($arUpdateProperty["PROPERTY_TYPE"] == "L")
				{
					if(($val = array_search($arPropV["VALUE_ENUM"], $arUpdateProperty["VALUES"])) !== false)
						$arPropV["VALUE"] = $val;
				}

				if($arUpdateProperty["MULTIPLE"] == "Y")
				{
					if(!is_set($arFields["PROPERTY_VALUES"], $arUpdateProperty["ID"]))
						$arFields["PROPERTY_VALUES"][$arUpdateProperty["ID"]] = array();
					$arFields["PROPERTY_VALUES"][$arUpdateProperty["ID"]][] = array(
						"VALUE" => $arPropV["VALUE"],
						"DESCRIPTION" => $arPropV["DESCRIPTION"],
					);
				}
				else
				{
					$arFields["PROPERTY_VALUES"][$arUpdateProperty["ID"]] = array(
						"VALUE" => $arPropV["VALUE"],
						"DESCRIPTION" => $arPropV["DESCRIPTION"],
					);
				}
			}
		}

		$ID = false;
		$el = new CIBlockElement;
		$rs = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $arFields["IBLOCK_ID"], "XML_ID" => $arFields["XML_ID"], "SHOW_NEW" => "Y"), false, false, array("ID", "IBLOCK_ID"));
		if($bCheckUniq && ($arDBElement = $rs->Fetch()))
		{
			$ID = $arDBElement["ID"];
			if($bWorkflow)
				$ID = CIBlockElement::WF_GetLast($arDBElement["ID"]);

			$res = $el->Update($ID, $arFields, $bWorkflow, true, true);
			if(!$res)
			{
				$GLOBALS["APPLICATION"]->ThrowException('Ошибка обновления элемента #'.$ID.'. '.$el->LAST_ERROR);
				return false;
			}
		}
		else
		{
			$ID = $el->Add($arFields, $bWorkflow, true, true);
			$res = $ID > 0;
			if(!$res)
			{
				$GLOBALS["APPLICATION"]->ThrowException('Ошибка добавления элемента. '.$el->LAST_ERROR);
				return false;
			}
		}

		if($bBizproc)
		{
			define("MODULE_ID", "iblock");
			define("ENTITY", "CIBlockDocument");
			CBPDocument::AddDocumentToHistory(array(MODULE_ID, ENTITY, $ID), $arFields["NAME"], $GLOBALS["USER"]->GetID());
		}
		return $ID;
	}

	/**
	 * Копирует элемент инфоблока в инфоблок
	 *
	 * @access public
	 *
	 * @param int $ID ID элемента
	 * @param int $TO_IBLOCK_ID ID инфоблока, в который копируем элемент
	 * @param bool $bSyncProps Указывает на копирование свойств элемента, по умолчанию false
	 * @param bool $bCheckUniq Проверять на уникальность при добавлении, по умолчанию true
	 *
	 * @return int
	 *
	 * @example
	 * <pre>
	 * $ID = 12;
	 * $TO_BID = 3;
	 * $bSyncProps = true;
	 * $pid = CARTIBlockTools::copyIblockElement($ID, $TO_BID, $bSyncProps);
	 * </pre>
	 *
	 */
	public static function copyIblockElement($ID, $TO_IBLOCK_ID, $bSyncProps=false, $bCheckUniq=true)
	{
		return self::syncIblockElement($ID, $TO_IBLOCK_ID, false, false);
	}

	/**
	 * Возвращает ID свойства по его коду
	 *
	 * @access public
	 *
	 * @param string $CODE Код свойства
	 * @param int $IBLOCK_ID ID инфоблока, которому принадлежит свойство
	 *
	 * @return int
	 *
	 * @example
	 * <pre>
	 * $code = "PHONE";
	 * $BID = 1;
	 * $pid = CARTIBlockTools::GetPropertyIDByCode($code, $BID);
	 * </pre>
	 *
	 */
	public static function GetPropertyIDByCode($CODE, $IBLOCK_ID)
	{
		static $arCache = array();
		$uniq = $CODE.'|'.$IBLOCK_ID;

		if(!array_key_exists($uniq, $arCache) && CModule::IncludeModule("iblock"))
		{
			$arProp = CIBlockProperty::GetList(array(), Array("ACTIVE" => "Y", "IBLOCK_ID" => $IBLOCK_ID, "CODE" => $CODE))->Fetch();
			if(!$arProp)
			{
				throw new exception('property not found');
			}
			$arCache[$uniq] = $arProp["ID"];
		}
		return $arCache[$uniq];
	}

	/**
	 * Возвращает значение множественного свойства по его ID из общего массива свойств
	 *
	 * @access public
	 *
	 * @param int $PID ID свойства
	 * @param array $arFields Массив с набором значений свойств
	 *
	 * @return array
	 *
	 */
	public static function GetMultyplePropertyValue($PID, $arFields)
	{
		$res = false;

		if(!is_array($arFields))
			return false;

		if(!is_set($arFields, "PROPERTY_VALUES"))
			$arFields["PROPERTY_VALUES"] = $arFields;

		foreach ($arFields["PROPERTY_VALUES"][$PID] as $arValue) {

			if(array_key_exists("VALUE", $arValue) && !empty($arValue["VALUE"]))
			{
				$res[] = $arValue["VALUE"];
			}
			elseif(is_array($arValue))
			{
				foreach($arValue as $arr)
				{
					$val = is_set($arr, "VALUE") ? $arr["VALUE"] : $arr;
					if (!empty($val))
						$res[] = $val;
					break;
				}
			}
			elseif(!empty($arValue))
			$res[] = $arValue;
		}

		return $res;
	}

	/**
	 * Возвращает значение свойства по его ID из общего массива свойств
	 *
	 * @access public
	 *
	 * @param int $PID ID свойства
	 * @param array $arFields Массив с набором значений свойств
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * $pid = 100;
	 * $arFields = Array("99" => "Тестовая строка", "100" => "example@example.ru","101" => Array("VALUE" => "+7  (111) 111-11-11"));
	 * $value = CARTIBlockTools::GetSinglePropertyValue($pid, $arFields);
	 * </pre>
	 *
	 */
	public static function GetSinglePropertyValue($PID, $arFields)
	{
		$res = false;

		if(!is_array($arFields))
		return false;

		if(!is_set($arFields, "PROPERTY_VALUES"))
		$arFields["PROPERTY_VALUES"] = $arFields;

		if(array_key_exists("VALUE", $arFields["PROPERTY_VALUES"][$PID]))
		{
			$res = $arFields["PROPERTY_VALUES"][$PID]["VALUE"];
		}
		elseif(is_array($arFields["PROPERTY_VALUES"][$PID]))
		{
			foreach($arFields["PROPERTY_VALUES"][$PID] as $arValue)
			{
				$res = is_set($arValue, "VALUE") ? $arValue["VALUE"] : $arValue;
				break;
			}
		}
		elseif(is_set($arFields["PROPERTY_VALUES"], $PID))
			$res = $arFields["PROPERTY_VALUES"][$PID];

		return $res;
	}

	/**
	 * Устанавливает значение свойства в массиве свойств
	 *
	 * @access public
	 *
	 * @param int $PID ID свойства
	 * @param mixed $mixValue Новое значение свойства
	 * @param array $arFields Массив с набором значений свойств
	 *
	 * @return bool
	 *
	 * @example
	 * <pre>
	 * $pid = 100;
	 * $sNewVAlue = example2@example.ru
	 * $arFields = Array(
	 * 		"99" => "Тестовая строка",
     *  	"100" => "example@example.ru",
     *      "101" => Array(
     *      	"VALUE" => "+7  (111) 111-11-11"
     *      )
	 * );
	 * $bRes = CARTIBlockTools::SetSinglePropertyValue($pid, $arFields);
	 * </pre>
	 *
	 */
	public static function SetSinglePropertyValue($PID, $mixValue, &$arFields)
	{
		if(!is_array($arFields["PROPERTY_VALUES"]))
			return false;

		if(array_key_exists("VALUE", $arFields["PROPERTY_VALUES"][$PID]))
		{
			$arFields["PROPERTY_VALUES"][$PID]["VALUE"] = $mixValue;
		}
		elseif(is_array($arFields["PROPERTY_VALUES"][$PID]))
		{
			foreach($arFields["PROPERTY_VALUES"][$PID] as $i => $arValue)
			{
				if(is_set($arValue, "VALUE"))
				$arFields["PROPERTY_VALUES"][$PID][$i]["VALUE"] = $mixValue;
				else
				$arFields["PROPERTY_VALUES"][$PID][$i] = $mixValue;

				break;
			}
		}
		elseif(is_set($arFields["PROPERTY_VALUES"], $PID))
			$arFields["PROPERTY_VALUES"][$PID] = $mixValue;
		else
			return false;

		return true;
	}

	/**
	 * Возвращает символьный код секции-родителя
	 *
	 * @access public
	 *
	 * @param int $SECTION_ID ID секции
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * $SID = 100;
	 * $sParentCode = CARTIBlockTools::GetParentSectionCode($SID);
	 * </pre>
	 *
	 */
	public static function GetParentSectionCode($SECTION_ID)
	{
		global $DB;
		static $arParentSectionCache = array();

		$SECTION_ID = intval($SECTION_ID);
		if($SECTION_ID > 0)
		{
			if(!array_key_exists($SECTION_ID, $arParentSectionCache))
			{
				$strSql = "
					SELECT BS.CODE
					FROM b_iblock_section M, b_iblock_section BS
					WHERE M.ID = ".$SECTION_ID."
					AND M.IBLOCK_ID = BS.IBLOCK_ID
					AND M.IBLOCK_SECTION_ID = BS.ID
				";
				$res = $DB->Query($strSql);
				$arParentSectionCache[$SECTION_ID] = $res->Fetch();
			}
			if(is_array($arParentSectionCache[$SECTION_ID]))
				return $arParentSectionCache[$SECTION_ID]["CODE"];
		}
		return "";
	}

	/**
	 * Возвращает URL по переданному шаблону и полям объекта
	 *
	 * @access public
	 *
	 * @param string $url Адрес страницы
	 * @param array $arr Массив с данными для замены
	 * @param bool $server_name Указывает на обработку макроса #SERVER_NAME#, по умолчанию false
	 * @param mixed $arrType Указывает на тип объекта, false (по умолчанию) | S (секция) | E (элемент)
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * $url = "http://test-adres.ru/#SECTION_ID#/#ELEMENT_ID#/";
	 * $arr = Array("ELEMENT_ID" => 102, "SECTION_ID" => 22);
	 * $sParentCode = CARTIBlockTools::ReplaceIblockUrl($url, $arr);
	 * </pre>
	 *
	 */
	public static function ReplaceIblockUrl($url, $arr, $server_name = false, $arrType = false)
	{
		if(!$arrType)
		{
			if(array_key_exists("GLOBAL_ACTIVE", $arr))
				$arrType = "S";
			else
				$arrType = "E";
		}
		if($arrType === "S")
		{
			$url = str_replace("#ID#", "#SECTION_ID#", $url);
			$url = str_replace("#CODE#", "#SECTION_CODE#", $url);
			$url = str_replace("#PARENT_CODE#", "#PARENT_SECTION_CODE#", $url);
		}
		if($server_name)
		{
			$url = str_replace("#LANG#", $arr["LANG_DIR"], $url);
			if((defined("ADMIN_SECTION") && ADMIN_SECTION===true) || !defined("BX_STARTED"))
			{
				$lcache = &$GLOBALS["ALX_LCACHE_CACHE"];
				if(!is_array($lcache))
					$lcache = array();
				if(!is_set($lcache, $arr["LID"]))
				{
					$db_lang = CLang::GetByID($arr["LID"]);
					$arLang = $db_lang->Fetch();
					$lcache[$arr["LID"]] = $arLang;
				}
				$arLang = $lcache[$arr["LID"]];
				$url = str_replace("#SITE_DIR#", $arLang["DIR"], $url);
				$url = str_replace("#SERVER_NAME#", $arLang["SERVER_NAME"], $url);
			}
			else
			{
				$url = str_replace("#SITE_DIR#", SITE_DIR, $url);
				$url = str_replace("#SERVER_NAME#", SITE_SERVER_NAME, $url);
			}
		}

		global $DB;

		static $arSectionCache = Array();
		static $arParentSectionCache = Array();

		$arSearch = array(
		/*Theese come from GetNext*/
			"#SITE_DIR#",
			"#ID#",
			"#CODE#",
			"#EXTERNAL_ID#",
			"#IBLOCK_TYPE_ID#",
			"#IBLOCK_ID#",
			"#IBLOCK_CODE#",
			"#IBLOCK_EXTERNAL_ID#",
		/*And theese was born during components 2 development*/
			"#ELEMENT_ID#",
			"#ELEMENT_CODE#",
			"#SECTION_ID#",
			"#SECTION_CODE#",
			"#PARENT_SECTION_CODE#"
		);
		$arReplace = array(
		$arr["LANG_DIR"],
		intval($arr["ID"]) > 0? intval($arr["ID"]): "",
		urlencode($arr["CODE"]),
		urlencode($arr["EXTERNAL_ID"]),
		urlencode($arr["IBLOCK_TYPE_ID"]),
		intval($arr["IBLOCK_ID"]) > 0? intval($arr["IBLOCK_ID"]): "",
		urlencode($arr["IBLOCK_CODE"]),
		urlencode($arr["IBLOCK_EXTERNAL_ID"]),
		);

		if($arrType === "E")
		{
			$arReplace[] = intval($arr["ID"]) > 0? intval($arr["ID"]): "";
			$arReplace[] = urlencode($arr["CODE"]);
			#Deal with symbol codes
			$SECTION_CODE = "";
			$SECTION_ID = intval($arr["IBLOCK_SECTION_ID"]);
			if(strpos($url, "#SECTION_CODE#") !== false && $SECTION_ID > 0)
			{
				if(!array_key_exists($SECTION_ID, $arSectionCache))
				{
					$res = $DB->Query("SELECT CODE FROM b_iblock_section WHERE ID = ".$SECTION_ID);
					$arSectionCache[$SECTION_ID] = $res->Fetch();
				}
				if(is_array($arSectionCache[$SECTION_ID]))
				$SECTION_CODE = $arSectionCache[$SECTION_ID]["CODE"];
			}
			$arReplace[] = $SECTION_ID > 0? $SECTION_ID: "";
			$arReplace[] = urlencode($SECTION_CODE);
		}
		elseif($arrType === "S")
		{
			$arReplace[] = "";
			$arReplace[] = "";
			$arReplace[] = intval($arr["ID"]) > 0? intval($arr["ID"]): "";
			$arReplace[] = urlencode($arr["CODE"]);
		}
		else
		{
			$arReplace[] = intval($arr["ELEMENT_ID"]) > 0? intval($arr["ELEMENT_ID"]): "";
			$arReplace[] = urlencode($arr["ELEMENT_CODE"]);
			$arReplace[] = intval($arr["IBLOCK_SECTION_ID"]) > 0? intval($arr["IBLOCK_SECTION_ID"]): "";
			$arReplace[] = urlencode($arr["SECTION_CODE"]);
		}
		if(strpos($url, "#PARENT_SECTION_CODE#") !== false)
		{
			if($arrType === "E")
				$SECTION_ID = intval($arr["IBLOCK_SECTION_ID"]);
			elseif($arrType === "S")
				$SECTION_ID = intval($arr["ID"]);

			$PARENT_SECTION_CODE = "";
			if($SECTION_ID > 0)
			{
				$PARENT_SECTION_CODE = self::GetParentSectionCode($SECTION_ID);
			}
			$arReplace[] = urlencode($PARENT_SECTION_CODE);
		}
		else
		{
			$arReplace[] = "";
		}

		$url = str_replace($arSearch, $arReplace, $url);
		return preg_replace("'/+'s", "/", $url);
	}

	/**
	 * Возвращает массив, в котором для ключа URL добавлен GET параметр с родительской секцией.
	 *
	 * @access public
	 *
	 * @param array $arFields Массив с полями элемента
	 *
	 * @return array
	 *
	 * @example
	 * <pre>
	 * AddEventHandler("search", "BeforeIndex", array("CARTIBlockTools", "BeforeIndexHandler"));
	 * </pre>
	 *
	 */
	public static function BeforeIndexHandler($arFields)
	{
		if(substr($arFields["ITEM_ID"], 0, 1) !== 'S')
		{
			parse_str($arFields["URL"], $arr);
			$SECTION_ID = intval($arr["IBLOCK_SECTION_ID"]);
		}
		else
		{
			$SECTION_ID = intval(substr($arFields["ITEM_ID"], 1));
		}
		if($SECTION_ID)
			$arFields["URL"].= "&PARENT_SECTION_CODE=".self::GetParentSectionCode($SECTION_ID);

		return $arFields;
	}

	/**
	 * Возвращает обработанную URL, убрав из нее #MODEL#
	 *
	 * @access public
	 *
	 * @param array $arFields Массив с полями элемента
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * AddEventHandler("search", "OnSearchGetURL", array("CARTIBlockTools", "OnSearchGetURL"));
	 * </pre>
	 *
	 */
	public static function OnSearchGetURL($arFields)
	{
		global $DB;
		static $arIBlockCache = array();

		if($arFields["MODULE_ID"] !== "iblock" || substr($arFields["URL"], 0, 1) !== "=")
			return $arFields["URL"];

		$url = str_replace("#MODEL#", "", $arFields["URL"]);
			return $url;

		return $arFields["URL"];
	}

	/**
	 * Возвращает разницу между полями "Начало активности" и "Окончание активности"
	 *
	 * @access public
	 *
	 * @param array $arItem Массив с полями элемента
	 * @param bool $capitalize Указывает на отмену приведения строки к нижнему регистру, по умолчанию false
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * $arItem = Array("ID" => 13, "IBLOCK_ID" => 1, "ACTIVE" => "Y", "ACTIVE_FROM" => "01.01.2013 12:00:00", "ACTIVE_TO" => "07.01.2013 19:30:00");
	 * $sInterval = CARTIBlockTools::GetItemDateInterval($arItem);
	 * </pre>
	 *
	 */
	public static function GetItemDateInterval(array $arItem, $capitalize = false)
	{
		foreach(array("ACTIVE_FROM", "ACTIVE_TO") as $key)
		{
			if(!is_set($arItem, $key) && is_set($arItem, "DATE_".$key))
				$arItem[$key] = $arItem["DATE_".$key];
		}
		return CDFADateTools::GetInterval($arItem["ACTIVE_FROM"], $arItem["ACTIVE_TO"], $capitalize);
	}

	/**
	 * Возвращает предыдущий и следующий элементы инфоблока, относительно переданного элемента по фильтру arFilter
	 *
	 * @access public
	 *
	 * @param string $id ID элемента
	 * @param array $arOrder Массив с полями для сортировки
	 * @param array $arFilter Массив с полями для фильтрации
	 *
	 * @return array
	 *
	 * @example
	 * <pre>
	 * $id = 100;
	 * $arOrder = Array("ID" => "ASC");
	 * $arFilter = Array("IBLOCK_ID" => $BID,"ACTIVE" => "Y", "DATE_ACTIVE" => "Y");
	 * $arResult = CARTIBlockTools::GetItemSiblings($id, $arOrder, $arFilter);
	 * </pre>
	 *
	 */
	public static function GetItemSiblings($id, array $arOrder, array $arFilter, array $arSelect = array("ID", "IBLOCK_ID", "DETAIL_PAGE_URL", "NAME"))
	{
		$result = array("PREV" => false, "CURRENT" => false, "NEXT" => false);

		$i = 0;
		$rs = CIBlockElement::GetList(
			$arOrder,
			$arFilter,
			false,
			array(
				"nElementID" => $id,
				"nPageSize" => 1
			),
			$arSelect
		);
		while($ar = $rs->GetNext())
		{
			if($i == 0 && $ar["ID"] != $id)
				$result["PREV"] = $ar;
			elseif($i == 1 && $ar["ID"] != $id)
				$result["NEXT"] = $ar;
			elseif($i == 2)
				$result["NEXT"] = $ar;

			if($ar["ID"] == $id)
				$result["CURRENT"] = $ar;
			$i++;
		}

		return $result;
	}

	/**
	 * Возвращает ID инфоблока по его коду
	 *
	 * @access public
	 *
	 * @param string $code Код инфоблока
	 * @param string $iblockType ID типа инфоблока, по умолчанию null
	 * @param string $site_id ID сайта, в котором искать инфоблок, по умолчанию SITE_ID
	 *
	 * @return int
	 *
	 * @example
	 * <pre>
	 * $code = "stickers";
	 * $iblockType = "services";
	 * $id = CARTIBlockTools::GetIDByCode($code, $iblockType, $site_id);
	 * </pre>
	 *
	 */
	public static function GetIDByCode($code, $iblockType=null, $site_id=SITE_ID)
	{
		if(!CModule::IncludeModule("iblock"))
		{
			$GLOBALS["APPLICATION"]->ThrowException('Модуль инфоблоков не установлен');
			return false;
		}
		else
		{
			if(
				/*(defined("ADMIN_SECTION") && ADMIN_SECTION == true)
				|| */empty($site_id)
				|| !strlen($code)
			)
				return 0;

			$cache = new CDFACache(__CLASS__.'::'.__FUNCTION__.'|'.$site_id.'|'.$code.'|'.$iblockType);
			$result = $cache->init(false);
			if(null == $result)
			{
				$arFilter = array(
					"ACTIVE"	=> "Y",
					"SITE_ID"	=> $site_id,
					"CODE"		=> $code,
					"MIN_PERMISSION"=> "R"
				);

				if(null != $iblockType)
					$arFilter["TYPE"] = $iblockType;
				$result = 0;
				$rs = CIBlock::GetList(array(), $arFilter, false);
				if($ar = $rs->Fetch())
					$result = $ar["ID"];

				$cache->set($result);
			}
			return $result;
		}
	}
	/**
	 * Возвращает ID типа инфоблока по его коду
	 *
	 * @access public
	 *
	 * @param string $code Код инфоблока
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * $code = "stickers";
	 * $type = CARTIBlockTools::GetTypeByCode($code);
	 * </pre>
	 *
	 */
	public static function GetTypeByCode($code)
	{
		if(!CModule::IncludeModule("iblock"))
		{
			$GLOBALS["APPLICATION"]->ThrowException('Модуль инфоблоков не установлен');
			return false;
		}
		else
		{
			$cache = new CDFACache(__CLASS__.'::'.__FUNCTION__.'|'.SITE_ID.'|'.$code);
			$result = $cache->init(false);

			if(null == $result)
			{
				$arFilter = array(
					"ACTIVE"	=> "Y",
					"SITE_ID"	=> SITE_ID,
					"CODE"		=> $code,
					"MIN_PERMISSION"=> "R"
				);
				$result = 0;
				$rs = CIBlock::GetList(array(), $arFilter, false);
				if($ar = $rs->Fetch())
					$result = $ar["IBLOCK_TYPE_ID"];

				$cache->set($result);
			}
		}
		return $result;
	}

	public static function GetSectionIDByCode($code, $iblockID)
	{
		if(!CModule::IncludeModule("iblock"))
		{
			$GLOBALS["APPLICATION"]->ThrowException('Модуль инфоблоков не установлен');
			return false;
		}

		if(!intval($iblockID))
		{
			$GLOBALS["APPLICATION"]->ThrowException('Не указан iblockID');
			return false;
		}

		$cache = new CDFACache(__CLASS__.'::'.__FUNCTION__.'|'.SITE_ID.'|'.$code.'|'.$iblockID);
		$result = $cache->init(false);

		if(null == $result)
		{
			$arFilter = array(
					"ACTIVE"	=> "Y",
					"MIN_PERMISSION"=> "R",
					"CODE" => $code,
					"IBLOCK_ID" =>  $iblockID
			);
			$result = 0;
			$rs = CIBlockSection::GetList(array(), $arFilter, false, Array("ID"));
			if($ar = $rs->Fetch())
				$result = $ar["ID"];

			$cache->set($result);
		}
		return $result;
	}

}