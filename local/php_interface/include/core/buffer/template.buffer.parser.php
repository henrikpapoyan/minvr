<?
/**
 * CDFATemplateBufferParser
 */
AddEventHandler("main", "OnEndBufferContent", array("CDFATemplateBufferParser", "OnEndBufferContent"));

/**
 * Класс для работы с содержимым буфера.<br/>
 * Регистрация содержимого, поиск и замена тегов соответствующего содержания.<br/>
 * Замена выполняется на отложенных функциях.
 *
 * @example
 * <pre>
 * &lt;?
 * // class for replace &lt;include id="webform"&gt; tag bitrix:form.result.new content
 * class CDeferredWebform extends ADFATemplateBufferComponent
 * {
 * 		public function GetID()
 * 		{
 * 			return 'webform';
 * 		}
 *
 * 		public function __construct()
 * 		{
 * 			parent::__construct(
 * 				"bitrix:form.result.new",
 * 				"",
 * 				array(
 * 					"SEF_MODE" => "N",
 * 					"SEF_FOLDER" => "/",
 * 					"CACHE_TYPE" => "A",
 * 					"CACHE_TIME" => "3600",
 * 					"LIST_URL" => "result_list.php",
 * 					"EDIT_URL" => "result_edit.php",
 * 					"SUCCESS_URL" => "",
 * 					"VARIABLE_ALIASES" => array(
 * 						"WEB_FORM_ID" => "WEB_FORM_ID",
 * 						"RESULT_ID" => "RESULT_ID",
 * 					)
 * 				)
 * 			);
 * 		}
 *
 *  	public function GetContent(array $params, $tagContent)
 * 		{
 * 			if(!is_set($params, "id"))
 * 				return '';
 * 			return $this->GetComponentContent(array("WEB_FORM_ID" => intval($params["id"])));
 * 		}
 * }
 *
 * CDFATemplateBufferParser::GetInstance()->Register(new CDeferredWebform());
 *
 * // this tag was be replaced by webform content with ID 2
 * ?&gt;
 * &lt;include id="webform" data-id="2" /&gt;
 * </pre>
 *
 * @author ivanko
 * @package template
 * @subpackage buffer
 **/
class CDFATemplateBufferParser
{
	/**
	 * @access private
	 * @static
	 * @var array Массив для хранения зарегистрированных в системе парсеров
	 **/
	private $ob = array();

	/**
	 * @access private
	 * @static
	 * @var object Переменная хранит экземпляра класса, по умолчанию null
	 **/
	private static $instance = null;

	/**
	 * @access protected
	 * @static
	 * @var string Регулярное выражение для поиска тегов
	 **/
	protected static $tagRegExp = '#<(#TAG#) id=["\']?([^"\']+)["\']?([^>]*)>#is';


	/**
	 * @access protected
	 * @static
	 * @var string Регулярное выражение для выборки атрибутов-параметров data-*
	 **/
	protected static $paramsRegExp = '/data-([^= ]+)=["\']?([^>"\' ]*)["\']?/is';

	/**
	 * Возвращает экземпляр класса CDFATemplateBufferParser
	 *
	 * @access public
	 *
	 * @return object
	 *
	 */
	public static function GetInstance()
	{
		if(self::$instance == null)
		{
			$a = __CLASS__;
			self::$instance = new $a();
		}
		return self::$instance;
	}

	/**
	 * Конструктор класса
	 *
	 * @access private
	 */
	private function __construct() {}

	/**
	 * Функция вызываемая при клонировании класса
	 *
	 * @access private
	 */
	private function __clone() {}

	/**
	 * Регистрация парсера контента в системе
	 *
	 * @access public
	 *
	 * @param IDFATemplateBufferContent $ob Объект, подлежащий регистрации в системе и соответсвующий интерфейсу IDFATemplateBufferContent
	 *
	 * @return void
	 *
	 */
	public function Register(IDFATemplateBufferContent $ob)
	{
		if(!is_set($this->ob, $ob->getID()))
			$this->ob[$ob->getID()] = $ob;
	}

	/**
	 * Получить зарегистрированный парсер по ID
	 *
	 * @access public
	 *
	 * @param int $id ID парсера
	 *
	 * @return object
	 *
	 */
	public function GetByID($id)
	{
		return is_set($this->ob, $id) ? $this->ob[$id] : null;
	}

	/**
	 * Парсинг контента, поиск и замена тегов
	 *
	 * @access public
	 *
	 * @param string $content Контент, подлежащий парсингу
	 * @param string $findTag Имя тега для поиска, по умолчанию "include"
	 *
	 * @return string
	 *
	 */
	public function Parse($content, $findTag="include")
	{
		if(count($this->ob) && ($matches = $this->FindTag($content, $findTag)))
		{
			foreach($matches[0] as $key => $match)
			{
				$entityContent = $this->GetByID($matches[2][$key]);
				if(null != $entityContent)
				{
					// если использовался закрываюий тег то удалим его
					$content = str_replace("</$findTag>", "", $content);
					$content = str_replace($match, $entityContent->GetContent($this->FindParams($matches[3][$key]), $matches[4][$key]), $content);
				}
			}
		}
		return $content;
	}

	/**
	 * Поиск тега и добавление параметров
	 *
	 * @access public
	 *
	 * @param string $content Контент
	 * @param string $findTag Имя тега для поиска, по умолчанию "include"
	 * @param string $id ID атрибута для тега, по умолчанию null
	 * @param string|array $name Имя параметра или массив с имена параметров
	 * @param string $value Значение параметра $name, при условии что $name имеет тим string. По умолчанию null
	 *
	 * @return void
	 *
	 */
	public function addParam(&$content, $findTag="include", $id, $name, $value=null)
	{
		$arParams = is_array($name) ? $name : array($name => $value);
		if($matches = $this->FindTag($content, $findTag))
		{
			foreach($matches[0] as $key => $match)
			{
				if($matches[2][$key] == $id)
				{
					$arTmp = array();
					foreach(array_replace_recursive($this->FindParams($matches[3][$key]), $arParams) as $key => $val)
						$arTmp[] = 'data-'.$key.'="'.$val.'"';
					$replace = '<'.$findTag.' id="'.$id.'" '.join(' ', $arTmp).'>';

					$content = str_replace($match, $replace, $content);
				}
			}
		}
	}

	/**
	 * Поиск тега со специальным ID
	 *
	 * @access public
	 *
	 * @param string $content Контент, подлежащий парсингу
	 * @param string $findTag Имя тега для поиска, по умолчанию "include"
	 * @param string $id ID атрибута для тега, по умолчанию null
	 *
	 * @return bool Вернет true, если найдет tag с указанным id, в противном случае false
	 *
	 * @example
	 *
	 */
	public function TagExists($content, $findTag="include", $id=null)
	{
		if(preg_match($this->GetRegExp($findTag), $content, $matches))
		{
			if(null == $id || $id == $matches[2])
				return true;
		}
		return false;
	}

	/**
	 * Получить регулярное выражение, для поиска контейнера
	 *
	 * @access protected
	 *
	 * @param string $findTag Имя тега для замены в регулярном выражении
	 *
	 * @return string
	 *
	 * @example
	 *
	 */
	protected function GetRegExp($findTag)
	{
		return str_replace('#TAG#', $findTag, self::$tagRegExp);
	}

	/**
	 * Поиск по тегу в контенте
	 *
	 * @access protected
	 *
	 * @param string $content Контент
	 * @param string $findTag Имя тега для замены в регулярном выражении
	 *
	 * @return array
	 *
	 */
	protected function FindTag($content, $findTag)
	{
		if(
			(preg_match_all($this->GetRegExp($findTag), $content, $matches))
			&& !empty($matches[0])
		)
			return $matches;

		return null;
	}

	/**
	 * Поиск параметров в тегированном контенте
	 *
	 * @access protected
	 *
	 * @param string $tagContent Тегированный контент
	 *
	 * @return array
	 *
	 */
	protected function FindParams($tagContent)
	{
		$result = array();
		if(preg_match_all(self::$paramsRegExp, $tagContent, $tags))
			$result = array_combine($tags[1], $tags[2]);

		return $result;
	}

	/**
	 * Функция для события OnEndBufferContent
	 *
	 * @access public
	 *
	 * @param string $content Контент
	 *
	 * @return void
	 *
	 * @example
	 * <pre>
	 * AddEventHandler("main", "OnEndBufferContent", array("CDFATemplateBufferParser", "OnEndBufferContent"));
	 * </pre>
	 *
	 */
	public static function OnEndBufferContent(&$content)
	{
		if(!CSiteTemplateTools::IsAdminPage())
			$content = self::GetInstance()->Parse($content);
	}
}
?>