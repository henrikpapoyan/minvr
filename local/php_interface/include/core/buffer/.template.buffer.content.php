<?
/**
 * IDFATemplateBufferContent
 */
/**
 * Интерфейс для работы с буфферизированным контентом
 *
 * @author ivanko
 * @package template
 * @subpackage buffer
 *
 */
interface IDFATemplateBufferContent
{
	/**
	 * Получить ID буфера
	 * @access public
	 * @return string
	 */
	public function GetID();

	/**
	 * Получить контент, результат выполнения компонента, по тегу контента
	 *
	 * @access public
	 *
	 * @param array $params Параметры компоненты
	 * @param string $tagContent Тег контента
	 *
	 * @return string
	 */
	public function GetContent(array $params, $tagContent);
}
?>