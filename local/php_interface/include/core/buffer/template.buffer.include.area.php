<?

/**
 * CDFATemplateBufferIncludeArea
 */

/**
 * @ignore
 */
use Bitrix\Main\Component\ParametersTable;
require_once(".template.buffer.content.php");

/**
 * Класс для работы с отложенными включаемыми областями<br>
 * Создан для облегчения подключения в основном текстовых включаемых областей. <br>При этом включаемая область будет всегда отображатся с собственной рамкой в режиме правки, даже если включаемая область находится внутри шаблона другого компонента.
 * <br>
 * <br>
 * Пример использования:<br>
 * Просто в необходимое для включаемой области место вставте следующий код<br>
 * <code>
 * <include id="include-area" data-path="header/company_name.php" data-<paramName>="<paramValue>">
 * </code><br>
 * если в параметре необходимо передать массив или <b>cтроку с пробелами</b> то необходимо его сериализовать и зашифровать в BASE64
 * <code>
 * <include id="include-area" data-path="header/company_name.php" data-parameterName="<?=base64_encode(serialize(<массив или строка>))?>">
 * </code>
 * Если нужно скрыть рамку редактирования у включаемой области, то необходимо задать параметр data-SHOW_BORDER='N' или data-SHOW_BORDER='false'
 * <code>
 * <include id="include-area" data-path="header/company_name.php" data-SHOW_BORDER="N">
 * </code>
 * При этом подключаемый файл будет автоматически создан, если он не существовал.
 * @author Duzz
 * @package template
 * @subpackage buffer
 */

class CDFATemplateBufferincludeArea implements IDFATemplateBufferContent
{
	public function GetID()
	{
		return "include-area";
	}

	public function GetIncludeAreaContent($arParams=array())
	{
		ob_start();
		$rel_path = $arParams["path"];
		$rel_path = "includes/".$rel_path;
		$rel_path = CSiteTemplateTools::getIncludeFilePath($rel_path);

		//создадим файл, если его не существует
		if($GLOBALS["APPLICATION"]->GetTemplatePath($rel_path) === false)
		{
			$arRelPath = explode("/",$rel_path);
			$fileName = array_pop($arRelPath);

			$local_dir = $_SERVER["DOCUMENT_ROOT"]."/local/";

			if(file_exists($local_dir))
				$rootDir = $local_dir;
			else
				$rootDir = $_SERVER["DOCUMENT_ROOT"].BX_PERSONAL_ROOT;

			$dirPath = $rootDir."/templates/.default/".implode("/", $arRelPath)."/";
			CheckDirPath($dirPath);
			if(!file_exists($dirPath.$fileName))
				file_put_contents($dirPath.$fileName, $arParams["default-value"]);
		}

		$arFunctionParams = array_merge(array("SHOW_BORDER" => true), $arFunctionParams);
		//  http://dev.1c-bitrix.ru/api_help/main/reference/cmain/includefile.php
		if($arParams["SHOW_BORDER"]=='false'||$arParams["SHOW_BORDER"]=='N')
			$arFunctionParams["SHOW_BORDER"] = false;
			
		CSiteTemplateTools::IncludeFile($rel_path, $arParams, $arFunctionParams);
		return ob_get_clean();
	}

	public function GetContent(array $params, $tagContent)
	{

		foreach ($params as $key => $value)
		{
			$orig_value = $value;

			$value = base64_decode($value);
			if(unserialize($value) !== false) // является ли строка сериализованным значением
				$value = unserialize($value);
			else
				$value = $orig_value;

			$arTmpParams[$key] = $value;
		}
		$params = $arTmpParams;

		if(empty($params["path"]))
			return "";

		return $this->GetIncludeAreaContent($params);
	}
}
CDFATemplateBufferParser::GetInstance()->Register(new CDFATemplateBufferincludeArea);
?>