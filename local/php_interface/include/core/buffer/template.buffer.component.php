<?

/**
 * ADFATemplateBufferComponent
 */

/**
 * @ignore
 */
require_once(".template.buffer.content.php");

/**
 * Базовый абстрактный класс для работы с буферизацией компонентов
 *
 * @author ivanko
 * @package template
 * @subpackage buffer
 * @abstract
 */

abstract class ADFATemplateBufferComponent implements IDFATemplateBufferContent
{
	/**
	 * @access protected
	 * @var string Переменная хранит название компонента
	 **/
	protected $componentName;

	/**
	 * @access protected
	 * @var string Переменная хранит название шаблона компонента
	 **/
	protected $componentTemplate;

	/**
	 * @access protected
	 * @var array Переменная хранит массив с параметрами компонента
	 **/
	protected $componentParams;

	/**
	 * Конструктор класса
	 *
	 * @access public
	 *
	 * @param string $componentName Название компонента
	 * @param string $componentTemplate Шаблон компонента
	 * @param array $componentParams Параметры компонента, по умолчанию пустой массив
	 *
	 * @return object
	 *
	 */
	public function __construct($componentName, $componentTemplate, $componentParams=array())
	{
		$this->componentName = $componentName;
		$this->componentTemplate = $componentTemplate;
		$this->componentParams = $componentParams;
	}

	/**
	 * Получить название компонента
	 *
	 * @access public
	 *
	 * @return string
	 *
	 */
	public function getComponentName()
	{
		return $this->componentName;
	}

	/**
	 * Получить шаблон компонента
	 *
	 * @access public
	 *
	 * @return string
	 *
	 */
	public function getComponentTemplate()
	{
		return $this->componentTemplate;
	}

	/**
	 * Получить параметры компонента
	 *
	 * @access public
	 *
	 * @return array
	 *
	 */
	public function getComponentParams()
	{
		return $this->componentParams;
	}

	/**
	 * Получить контент, результат выполнения компонента
	 *
	 * @access public
	 *
	 * @param array $arParams Параметры компоненты
	 *
	 * @return string Вернет буферизированный контент, результат выполнения компонента
	 *
	 */
	public function GetComponentContent($arParams=array())
	{
		global $APPLICATION;
		ob_start();

		$APPLICATION->IncludeComponent(
			$this->getComponentName(),
			$this->getComponentTemplate(),
			array_replace_recursive($this->getComponentParams(), $arParams),
			false,
			array("HIDE_ICONS" => "Y")
		);

		return ob_get_clean();
	}

	/**
	 * Получить контент, результат выполнения компонента, по тегу контента
	 *
	 * @access public
	 *
	 * @param array $params Параметры компоненты
	 * @param string $tagContent Контент
	 *
	 * @return string Вернет буферизированный контент, результат выполнения компонента
	 *
	 */
	public function GetContent(array $params, $tagContent)
	{
		return $this->GetComponentContent();
	}
}
?>