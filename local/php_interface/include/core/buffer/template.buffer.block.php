<?
/**
 * CDFATemplateBufferBlock
 */

/**
 * Добавляем события OnEndBufferContent
 */
AddEventHandler("main", "OnEndBufferContent", array("CDFATemplateBufferBlock", "OnEndBufferContent"), 1500);


/**
 * Абстрактный класс для работы с буферизацией контента через событие OnEndBufferContent
 *
 * @author ivanko
 * @package template
 * @subpackage buffer
 * @abstract
 */
abstract class CDFATemplateBufferBlock implements IDFATemplateBufferContent
{
	/**
	 * @access private
	 * @static
	 * @var array Переменная хранит массив с буферизированным контентом
	 **/
	private static $content = array();

	/**
	 * Сохранить контент по тегу
	 *
	 * @access public
	 *
	 * @param array $params Параметры компоненты
	 * @param string $tagContent Буферизированный контент
	 *
	 * @return void
	 *
	 */
	public function GetContent(array $params, $tagContent)
	{
		if(strlen($tagContent) > 0)
			self::$content[$this->GetID()][] = $tagContent;
		return '';
	}

	/**
	 * Добавить в основной контент, буферизированный контент
	 *
	 * @access public
	 *
	 * @param string $content Основной контент
	 * @param string $text Буферизированный контент
	 *
	 * @return string
	 *
	 */
	public function replaceContent(&$content, $text)
	{
		return str_replace('<!-- BUFFER_CONTENT -->', $text, $content);
	}

	/**
	 * Вставить буфферизированный контент, в основной. Метод служит для события OnEndBufferContent
	 *
	 * @access public
	 *
	 * @param string $content Контент
	 *
	 * @return string
	 *
	 */
	public static function OnEndBufferContent(&$content)
	{
		// important. WYSIWYG add closing tag </include> and all is craches. Deleting this tag
		$content = str_replace("</include>", "", $content);

		$parser = CDFATemplateBufferParser::GetInstance();
		foreach(self::$content as $id => $ar)
		{
			$entity = $parser->GetByID($id);
			if((null != $entity) && ($entity instanceof CDFATemplateBufferBlock))
				$content = $entity->replaceContent($content, join("\n", $ar));
		}
	}
}
?>