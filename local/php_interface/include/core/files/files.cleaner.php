<?
/**
 * CDFAFolderCleaner
 * */

/**
* temp folder cleaner
* @author ivanko
* @package files
*/
class CDFAFolderCleaner
{
	private $DOC_ROOT;

	private $bRecurse = false;

	private $arList = array();

	function __construct($DOC_ROOT=false, $bRecurseDelete=false)
	{
		if($DOC_ROOT == false)
			$DOC_ROOT = $_SERVER["DOCUMENT_ROOT"];
		$this->DOC_ROOT	= trim($DOC_ROOT);

		if($bRecurseDelete === true)
			$this->bRecurse = true;
	}

	public function getDeletedList()
	{
		return $this->arList;
	}

	public function getDelFilesCount()
	{
		return count($this->arList);
	}

	public function getDocRoot()
	{
		return $this->DOC_ROOT;
	}

	protected function delete($absPath)
	{
		if(!$this->isValidPath($absPath))
			continue;
		$this->arList[] = $absPath;
		@unlink($absPath);
	}

	protected function isValidPath($absPath)
	{
		if(!file_exists($absPath))
			return false;

		foreach(array("/bitrix/modules", "/bitrix/php_interface", "/upload/iblock") as $pathTmp)
			if(stripos($absPath, $this->DOC_ROOT.$pathTmp) === 0)
				return false;

		return true;
	}

	protected function prepareMask($mask)
	{
		$mask = str_replace(
			array("\\", ".",  "?", "*",   "'"),
			array("/",  "\.", ".", ".*?", "\'"),
			$mask
		);
		return "'^".$mask."$'";
	}

	public function cleanDir($dir, $fileMask, $lifeTime=false)
	{
		$fileMask = preg_replace("#\/?/#", "\/", $fileMask);

		if(!$this->isValidPath($this->DOC_ROOT.$dir))
			return;

		$handler = opendir($this->DOC_ROOT.$dir);
		while(false !== ($file = readdir($handler)))
		{
			$absPath = $this->DOC_ROOT.$dir.'/'.$file;

			if(in_array($file, array('.', '..')))
				continue;

			if(!$this->isValidPath($absPath))
				continue;

			if(is_dir($absPath))
			{
				if($this->bRecurse)
					$this->cleanDir($dir.'/'.$file, $fileMask, $lifeTime);

				rmdir($absPath);
			}
			elseif(preg_match($this->prepareMask($fileMask), $file, $m))
			{
				if($lifeTime == false || (time() > (filemtime($absPath) + $lifeTime)))
					$this->delete($absPath);
			}
		}
		closedir($handler);
	}
}

?>