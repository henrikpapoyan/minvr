<?
/**
 * CDFAIncludeManager
 * */

/**
 * @copyright 2011 DEFA
 * @author ivanko
 * @package files
 */
class CDFAIncludeManager
{
	/**
	 * Подключить соответсвующий языковой файл
	 *
	 * @access public
	 *
	 * @param string $path Путь к файлу, для которого необходимо подключить яз. файл 
	 * @param bool $bReturnArray Указывает на необходимость вернуть массив с данными, по умолчанию false
	 * @param bool $bFileChecked Указывает на проверку существования файла, по умолчанию false
	 * 
	 * @return mixed Возвращает массив при установленной переменной $bReturnArray или true при удачном выполнении
	 *
	 * @examples
	 * <pre>
	 * $path = "/bitrix/templates/index/header.php";
	 * $MESS = CDFATools::IncludeLang($path, true);
	 * </pre>
	 */
	public static function IncludeLang($path, $bReturnArray=false, $bFileChecked=false)
	{
		return __IncludeLang(dirname($path)."/lang/".LANGUAGE_ID."/".basename($path), $bReturnArray, $bFileChecked);
	}

	public static function AddAutoloadClass($sClassName, $sClassPath)
	{
		self::AddAutoloadClasses(array(array($sClassName, $sClassPath)));
	}

	/**
	 * add class to autoload
	 * @param string $path abs folder path
	 * @param array $arParams classes to load: array("CDFAJSONTools" => "json.tools.php")
	 * @example
	 * CDFAIncludeManager::AddAutoloadClasses(__DIR__, array(
	 * 	"CDFAJSONTools" => "json.tools.php"
	 * ));
	*/
	public static function AddAutoloadClasses($path, $arParams=array())
	{
		foreach($arParams as $key => $val)
		{
			$relPath = Rel2Abs($path, $val);
			$arParams[$key] = substr($relPath, strlen(self::GetRealDocRoot()));
		}
		CModule::AddAutoloadClasses("", $arParams);
	}

	public static function GetRealDocRoot()
	{
		$root = $_SERVER["DOCUMENT_ROOT"];
		if(is_link($root.'/bitrix'))
			$root = dirname(realpath(readlink($root.'/bitrix')));
		
		return $root;
	}
}
?>