<?
/**
 * CDFADirList
 * */

/**
* @author ivanko
* @package files
*/
class CDFADirList
{
	public static function GetList($arOrder = Array(), $arFilter = Array(), $arAllowFiles=array())
	{
		global $APPLICATION;
		$arRes = Array();

		if(!is_set($arFilter, "DOC_ROOT"))
			return new CDBResult;

		$relPath = $arFilter["DOC_ROOT"];
		unset($arFilter["DOC_ROOT"]);

		$fullpath = $_SERVER["DOCUMENT_ROOT"].$relPath;

		if(file_exists($fullpath) && is_dir($fullpath))
		{
			$handle  = @opendir($fullpath);
			while(false !== ($folder = @readdir($handle)))
			{
				$path = $fullpath."/".$folder;
				if($folder == "." || $folder == "..") continue;

				$arDescription = Array("ID" => $folder, "IS_DIR" => is_dir($path));

				if(is_set($arFilter, "ID") && $arFilter["ID"] != $arDescription["ID"]) continue;

				if($arDescription["IS_DIR"])
				{
					$arDescription["DESCRIPTION"] = "";
					if(file_exists($path."/.description.php"))
					include($path."/.description.php");
				}

				if(!is_set($arDescription, "NAME"))
					$arDescription["NAME"] = $arDescription["ID"];

				foreach($arAllowFiles as $key=>$val)
					if(file_exists($path."/".$val))
						$arDescription[$key] = $relPath."/".$folder."/".$val;

				$arRes[$folder] = $arDescription;
			}
		}

		$db_res = new CDBResult();
		ksort($arRes);
		$db_res->InitFromArray($arRes);

		return $db_res;
	}
	public static function GetByID($ID)
	{
		return self::GetList(array(), array("ID" => $ID));
	}
}
?>