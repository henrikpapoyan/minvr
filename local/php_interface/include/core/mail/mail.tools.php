<?
/**
 * CDFAMailTools
 */

/**
 * Вспомогательный класс для работы с почтой
 * @package mail
 * @author ivanko
 **/
class CDFAMailTools
{
	/**
	 * Получить ID почтового события по ID почтового шаблона
	 *
	 * @access public
	 *
	 * @param int $ID ID почтового шаблона
	 *
	 * @return string|bool
	 *
	 * @example
	 * <pre>
	 * $template_id = 25;
	 * $event_id = CDFAMailTools::getEventIDByTemplateID($template_id);
	 * </pre>
	 */
	public static function getEventIDByTemplateID($ID)
	{
		static $arTemplates = array();
		if(!is_set($arTemplates, $ID))
			$arTemplates[$ID] = CEventMessage::GetByID($ID)->Fetch();

		return is_array($arTemplates[$ID]) ? $arTemplates[$ID]["EVENT_NAME"] : false;
	}

	/**
	 * Обработать почтовый шаблон, заменить строки вида "#*****#" на необходимые значения
	 *
	 * @access public
	 *
	 * @param array $arFields Массив с полями почтового события
	 * @param array $arParams Массив с параметрами почтового шаблона
	 *
	 * @return mixed
	 *
	 * @example
	 * <pre>
	 * CDFAMailTools::injectIblockTemplate(&$arFields, &$arParams);
	 * </pre>
	 */
	public static function injectIblockTemplate(&$arFields, &$arParams)
	{
		if(!class_exists("CDFAIBlockMailTemplate"))
			return;
		$eid = self::getEventIDByTemplateID($arParams["ID"]);
		if($eid == false)
			return;

		static $arIblockItems = array();
		if(!is_set($arIblockItems, $eid))
		{
			$arIblockItems[$eid] = array();

			$ob = new CDFAIBlockMailTemplate();

			$rs = $ob->getListByTypeID($eid, true);
			while($ar = $rs->GetNext())
				$arIblockItems[$eid][$ar["CODE"]] = $ar["MESSAGE"];
		}

		if(empty($arIblockItems[$eid]))
			return;

		foreach($arIblockItems[$eid] as $key=>$val)
			$arFields[$key] = CComponentEngine::MakePathFromTemplate($val, $arFields);
	}

	/**
	 * Получить данные пользователя
	 *
	 * @access public
	 *
	 * @param int $ID ID пользователя
	 *
	 * @return array
	 *
	 * @example
	 * <pre>
	 * $arUser = CDFAMailTools::GetUserInfo(25);
	 * </pre>
	 */
	public static function GetUserInfo($ID)
	{
		if($arUser = CUser::GetByID($ID)->Fetch())
		{
			if(isset($arUser["PERSONAL_PHONE"]))
				$arUser["PERSONAL_PHONE"] = preg_replace("/[^0-9]+/", "", $arUser["PERSONAL_PHONE"]);
		}
		return $arUser;
	}

	/**
	 * Отправляет содержимое элемента инфоблока по почте
	 *
	 * @access public
	 *
	 * @param mixed $ID ID элемента инфоблока
	 * @param string $EVENT_MESSAGE Тип почтового события
	 * @param string $SITE_ID ID сайта
	 * @param array $arEvents Массив c полями для почтового шаблона, по умолчанию пустой массив
	 * @param bool $bAttachFiles Указывает на прикрепление файлов в письме, по умолчанию true
	 *
	 * @return mixed
	 *
	 * @example
	 * <pre>
	 * $id = 1000;
	 * $event = "SEND_ELEMENT";
	 * CDFAMailTools::SendIBlockItem($id, $event, SITE_ID);
	 * </pre>
	 */
	public static function SendIBlockItem($ID, $EVENT_MESSAGE, $SITE_ID, $arEvents=array(), $bAttachFiles=true)
	{
		$strNameTemplate = '#LAST_NAME# #NAME# #SECOND_NAME#';

		$arElement = false;
		if(empty($EVENT_MESSAGE) || !CModule::IncludeModule("iblock"))
			return $arElement;

		if(is_array($ID) && is_set($ID, "PROPERTIES"))
		{
			$arElement = $ID;
			$arProperties = $ID["PROPERTIES"];
		}
		else
		{
			$obElement = CIBlockElement::GetByID($ID);
			if($rsElement = $obElement->GetNextElement())
			{
				$arElement = $rsElement->GetFields();
				$arProperties = $rsElement->GetProperties();
			}
		}
		if(!is_array($arElement))
			return $arElement;

		foreach (array("NAME", "PREVIEW_TEXT", "DETAIL_TEXT") as $code)
		{
			if (!is_set($arElement[$code."_TYPE"]) || $arElement[$code."_TYPE"] == "text")
				$arElement[$code] = html_entity_decode($arElement[$code], ENT_QUOTES, SITE_CHARSET);
		}

		$arEventFields = $arElement;

		if(is_set($arElement, "CREATED_BY"))
		{
			$arUser = self::GetUserInfo($arElement["CREATED_BY"]);
			if($arUser)
			{
				$arUser["FORMAT_NAME"] = CUser::FormatName($strNameTemplate, $arUser, true);
				foreach($arUser as $k => $v)
					$arEventFields["CREATED_BY_".$k] = $v;
			}
		}

		if(is_set($arProperties, "USER_ID"))
		{
			$arUser = self::GetUserInfo($arProperties["USER_ID"]["VALUE"]);
			if($arUser)
			{
				$arUser["FORMAT_NAME"] = CUser::FormatName($strNameTemplate, $arUser, false);
				foreach($arUser as $k => $v)
				$arEventFields["USER_".$k] = $v;
			}
		}

		if(is_set($arElement, "DATE_CREATE"))
		{
			$arEventFields["DATE_CREATE"] = ConvertTimeStamp(MakeTimeStamp($arElement["DATE_CREATE"]));
		}

		$events = GetModuleEvents("defa", "OnSendIBlockItemGetPropertiesEvent");
		while ($arEvent = $events->Fetch())
			$rsEvent = ExecuteModuleEventEx($arEvent, array(&$arProperties));

		foreach($arProperties as $pid => $prop)
		{
			$arProp = CIBlockFormatProperties::GetDisplayValue($arElement, $prop, "news_out");

			$arEventFields["PROP_".$pid] = html_entity_decode((is_array($arProp["DISPLAY_VALUE"])) ? join(" / ", $arProp["DISPLAY_VALUE"]) : $arProp["DISPLAY_VALUE"], ENT_QUOTES, SITE_CHARSET);

			if(in_array($pid, array("USER_PERSONAL_PHONE")))
			{
				$arEventFields["PROP_".$pid] = preg_replace("/[^0-9]+/", "", $arEventFields["PROP_".$pid]);
			}

			if($arProp["PROPERTY_TYPE"] == "L")
				$arProp["VALUE"] = $arProp["VALUE_XML_ID"];

			$arEventFields["~PROP_".$pid] = html_entity_decode((is_array($arProp["VALUE"])) ? join(" / ", $arProp["VALUE"]) : $arProp["VALUE"], ENT_QUOTES, SITE_CHARSET);

			if(($arProp["PROPERTY_TYPE"] == "E") && !empty($arProp["VALUE"]))
			{
				$ob = CIBlockElement::GetByID($arProp["VALUE"]);
				if($rs = $ob->GetNextElement())
				{
					$ar = $rs->GetFields();
					foreach($ar as $key=>$val)
					{
						if(substr($key, 0, 1) != '~')
						$arEventFields["PROP_".$pid."_".$key] = html_entity_decode($val, ENT_QUOTES, SITE_CHARSET);
					}
					foreach($rs->GetProperties() as $arrProp)
					{
						$arrProp = CIBlockFormatProperties::GetDisplayValue($ar, $arrProp, "news_out");
						$arEventFields["PROP_".$pid."_PROP_".$arrProp["CODE"]] = html_entity_decode((is_array($arrProp["DISPLAY_VALUE"])) ? join(", ", $arrProp["DISPLAY_VALUE"]) : $arrProp["DISPLAY_VALUE"], ENT_QUOTES, SITE_CHARSET);
					}
				}
			}
			if($bAttachFiles && ($arProp["PROPERTY_TYPE"] == "F") && !empty($arProp["VALUE"]))
			{
				$arEventFields["PROP_".$pid] = array();
				if(is_array($arProp["VALUE"]))
				{
					foreach($arProp["VALUE"] as $file)
					{
						if(intval($file))
							$arEventFields["PROP_".$pid][] = '[FILE]'.CFile::GetPath($file).'[/FILE]';
					}
				}
				elseif(intval($arProp["VALUE"]))
					$arEventFields["PROP_".$pid][] = '[FILE]'.CFile::GetPath(intval($arProp["VALUE"])).'[/FILE]';

				$arEventFields["PROP_".$pid] = join(' ', $arEventFields["PROP_".$pid]);
			}
		}

		if(CModule::IncludeModule("workflow") && (CIBlock::GetArrayByID($arEventFields["IBLOCK_ID"], "WORKFLOW") != "N"))
		{
			/**
			 * если сталкиваемся с документооборотом, то
			 * подменяем ID последних изменённых данных на родительский ID
			 * */
			if(CIBlockElement::WF_GetLast($arEventFields["ID"]) == $arEventFields["ID"])
				$arEventFields["ID"] = $arEventFields["~ID"] = $arEventFields["WF_PARENT_ELEMENT_ID"];
		}

		foreach($arEventFields as $k => $v)
		{
			if(is_array($v))
				$arEventFields[$k] = join(" / ", $v);
			if(substr($k, 0, 1) != '~')
				$arEventFields[$k] = strip_tags($arEventFields[$k]);
		}

		$arEventFields = array_merge($arEventFields, $arEvents);
		unset($arEventFields["PROPERTIES"]);

		$events = GetModuleEvents("defa", "OnBeforeSendIBlockItem");
		while($arEvent = $events->Fetch())
		{
			$bEventRes = ExecuteModuleEventEx($arEvent, array($arElement["IBLOCK_ID"], &$EVENT_MESSAGE, &$SITE_ID, &$arEventFields));
			if($bEventRes === false)
				return;
		}

		CEvent::Send($EVENT_MESSAGE, $SITE_ID, $arEventFields);
	}
}
?>