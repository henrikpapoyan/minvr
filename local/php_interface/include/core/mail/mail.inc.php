<?
require_once("mail.php");
require_once("mail.tools.php");

function custom_mail($to, $subject, $message, $additional_headers = "", $additional_parameters = "")
{

	if (defined("DFA_DEV_MODE"))
		$to = ONLY_EMAIL;

	$events = GetModuleEvents("defa", "OnBeforeCustomMailEvent");

	while ($arEvent = $events->Fetch()) {
		$rsEvent = ExecuteModuleEventEx($arEvent, array(&$to, &$subject, &$message, &$additional_headers, &$additional_parameters));
		if (is_bool($rsEvent))	// this means that event function
			return $rsEvent;	// sended email by herself
	}

	$strAttachmentTags = CDFAMail::getAttachmentTags();
	if (!empty($strAttachmentTags)) {
		$arAllPaths = CDFAMail::replaceTag($message, explode(',', $strAttachmentTags));

		$aPaths = array();
		foreach ($arAllPaths as $path)
			if (CDFAMail::checkFile($path))
				$aPaths[] = $path;

		if (!empty($aPaths)) {
			$contentType = CDFAMail::getContentTypeFromHeadersString($additional_headers);
			$charset = CDFAMail::getCharsetFromHeadersString($additional_headers);
			$obMail = new CDFAMail($contentType, $charset);

			$obMail->addHeadersString($additional_headers);
			$obMail->addMessage($message);

			foreach($aPaths as $sPath)
				$obMail->addAttachment($sPath);

			$additional_headers = $obMail->getHeadersString();
			$message = $obMail->getMessageString();
		}
	}

	return @mail($to, $subject, $message, $additional_headers, $additional_parameters);
}
?>