<?
/**
 * CDFAMailTools
 */

/**
 * Класс для работы с почтой
 * @package mail
 * @author ivanko
 * @example
 * <pre>
 * $oMail = CDFAMail("html");
 * $oMail->addMessage($text);
 * $oMail->send("example@example.ru", "example2@example.ru", "Тестовая тема");
 * </pre>
 **/
class CDFAMail
{
	/**
	 * @access private
	 * @var string Переменная хранит разделитель заголовков
	 **/
	private $eol;

	/**
	 * @access private
	 * @var string Переменная хранит границу между фрагментами, который содержится в заголовке Content-Type
	 **/
	private $boundary;

	/**
	 * @access private
	 * @var string Переменная хранить сообщение
	 **/
	private $message = '';

	/**
	 * @access private
	 * @var string Переменная хранит тип тела сообщения
	 **/
	private $messageType;

	/**
	 * @access private
	 * @var string Переменная хранит кодировку
	 **/
	private $charset;

	/**
	 * @access private
	 * @var string Переменная хранит массив, с заголовками письма
	 **/
	private $arHeaders = array();

	/**
	 * @access private
	 * @var string Переменная хранит массив, с вложениями
	 **/
	private $parts = array();

	/**
	 * Конструктор класса
	 *
	 * @param string $messageType Тип тела сообщения, по умолчанию text
	 * @param string $charset Кодировка, по умолчанию пусто
	 *
	 * @return void
	 */
	function __construct($messageType='text', $charset='')
	{
		$this->messageType = $messageType;
		$this->charset = $charset;
		$this->boundary = "----------".uniqid("");
		$this->eol = CEvent::GetMailEOL();
	}

	/**
	 * Получить Content-Type из заголовков письма
	 *
	 * @access public
	 *
	 * @param string $sHeaders Строка с заголовками письма
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * CDFAMail::getContentTypeFromHeadersString($headers);
	 * </pre>
	 */
	public static function getContentTypeFromHeadersString($sHeaders)
	{
		$result = '';
		if(preg_match('/Content-Type:\s?([^;]+);/i', $sHeaders, $matches))
			$result = $matches[1];
		return $result;
	}

	/**
	 * Получить кодировку из заголовков письма
	 *
	 * @access public
	 *
	 * @param string $sHeaders Строка с заголовками письма
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * CDFAMail::getCharsetFromHeadersString($headers);
	 * </pre>
	 */
	public static function getCharsetFromHeadersString($sHeaders)
	{
		$result = '';
		if(preg_match('/charset=(.+?)\n/i', $sHeaders, $matches))
						$result = $matches[1];
		return $result;
	}

	/**
	 * Получить раскрывающийся список типов файлов
	 *
	 * @access public
	 *
	 * @param string $name Атрибут name текстового поля
	 * @param string $value Активное значение, по умолчанию пусто
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * CDFAMail::GetFileTypesDropDownList("droplist");
	 * </pre>
	 */
	public static function GetFileTypesDropDownList($name, $value='')
	{
		$arTypes = array(
			'' => '',
			'jpg, gif, bmp, png, jpeg' => 'Изображения',
			'mp3, wav, midi, snd, au, wma' => 'Звуки',
			'mpg, avi, wmv, mpeg, mpe' => 'Видео',
			'doc, docx, xls, xlsx, pdf, txt, rtf' => 'Документы',
			'zip, rar, gz, 7z' => 'Архивы',
		);

		$res = '<input type="text" size="30" maxlength="255" id="'.md5($name).'" name="'.htmlspecialcharsEx($name).'" value="'.htmlspecialcharsEx($value).'">'."\n".
				'<select onchange="if(this.selectedIndex != 0) document.getElementById(\''.md5($name).'\').value = this[this.selectedIndex].value">'."\n";
		foreach($arTypes as $key=>$val)
			$res .= '<option value="'.$key.'"'.($key == $value ? ' selected="selected"' : '').'>'.$val.'</option>'."\n";
		$res .= '</select>';

		return $res;
	}

	/**
	 * Проверить файл на разрешенный формат
	 *
	 * @access public
	 *
	 * @param string $file Строка с названием файла
	 * @param array|string $arAllowExt Массив или строка с разрешениями файлов
	 *
	 * @return bool
	 *
	 * @example
	 * <pre>
	 * CDFAMail::checkFile("/upload/example.jpg", Array("jpg", "gif", "bmp", "png", "jpeg"));
	 * </pre>
	 */
	public static function checkFile($file, $arAllowExt=array())
	{
		if(empty($arAllowExt))
			$arAllowExt = COption::GetOptionString(
				'defa.core',
				'attachment_allow_extentions',
				'jpg, gif, bmp, png, jpeg, mp3, wav, midi, snd, '.
				'au, wma, mpg, avi, wmv, mpeg, mpe, doc, docx, xls, '.
				'xlsx, pdf, txt, rtf, zip, rar, gz, 7z'
			);

		$ext = substr(toLower(strrchr($file, ".")), 1);
		if(!is_array($arAllowExt))
			$arAllowExt = explode(',', $arAllowExt);

		$events = GetModuleEvents("defa", "OnMailCheckFile");
		while($arEvent = $events->Fetch())
		{
			$rsEvent = ExecuteModuleEventEx($arEvent, array($file, $ext, &$arAllowExt));
			if($rsEvent === false || $rsEvent === true)
				return $rsEvent;
		}
		foreach($arAllowExt as $key=>$val)
			$arAllowExt[$key] = trim($val);

		return in_array($ext, $arAllowExt);
	}

	/**
	 * Заменить теги в тексте
	 *
	 * @access public
	 *
	 * @param string $content Текст, подверженный обработке
	 * @param array $arTags Массив с тегами
	 *
	 * @return array
	 *
	 * @example
	 * <pre>
	 * CDFAMail::replaceTag(&$content, $arTags);
	 * </pre>
	 */
	public static function replaceTag(&$content, $arTags)
	{
		if(!is_array($arTags))
			$arTags = array($arTags);

		$arResult = array();
		foreach($arTags as $openTag)
		{
			$offset = 0;
			$closeTag = substr($openTag, 0, 1) . '/' . substr($openTag, 1);
			while (($startPosition = strpos($content, $openTag, $offset))!== false)
			{
				$endPosition = strpos($content, $closeTag, $startPosition + strlen($openTag));
				if ($endPosition === false)
					break;

				$topContent = substr($content, 0, $startPosition);
				$arResult[] = substr(
					$content,
					$startPosition + strlen($openTag),
					$endPosition - $startPosition - strlen($openTag)
				);
				$bottomContent = substr($content, $endPosition + strlen($closeTag));
				$content = $topContent.$bottomContent;
				$offset = $startPosition;
			}
		}
		return $arResult;
	}

	/**
	 * Записать текст сообщения
	 *
	 * @access public
	 *
	 * @param string $str Текст сообщения
	 *
	 * @return void
	 */
	public function addMessage($str)
	{
		$this->message = $str;
	}

	/**
	 * Получить текст сообщения
	 *
	 * @access public
	 *
	 * @param string $str Текст сообщения
	 *
	 * @return string
	 */
	public function getMessage($str)
	{
		return $this->message;
	}

	/**
	 * Добавить параметр в заголовок
	 *
	 * @access private
	 *
	 * @param string $param Параметр
	 * @param string $value Значение параметра
	 *
	 * @return void
	 */
	private function addHeader($param, $value)
	{
		$this->arHeaders[$param] = $value;
	}

	/**
	 * Добавить параметры в заголовок
	 *
	 * @access private
	 *
	 * @param array $arHeaders Массив с параметрами, в формате Array("параметр" => "значение")
	 *
	 * @return void
	 */
	private function addHeaders($arHeaders)
	{
		foreach($arHeaders as $key=>$val)
			$this->addHeader($key, $val);
	}

	/**
	 * Добавить заголовок с помощью строки
	 *
	 * @access public
	 *
	 * @param string $sHeaders Строка с заголовком
	 *
	 * @return void
	 */
	public function addHeadersString($sHeaders)
	{
		$arHeaders = explode($this->eol, $sHeaders);
		foreach($arHeaders as $sHeader)
			if(preg_match('/(\S+):\s?(.+)/', $sHeader, $matches))
				$this->addHeader($matches[1], $matches[2]);
	}

	/**
	 * Получить строку с заголовками
	 *
	 * @access public
	 *
	 * @return string
	 */
	public function getHeadersString()
	{
		$this->addHeaders(array(
			"MIME-Version" => '1.0',
			//"Content-Type" => (!empty($this->parts) ? 'multipart/related; boundary="'.$this->boundary.'"' : 'text/plain').(!empty($this->charset) ? '; charset='.$this->charset : ''),
			"Content-Type" => (!empty($this->parts) ? 'multipart/mixed; boundary="'.$this->boundary.'"' : 'text/plain').(!empty($this->charset) ? '; charset='.$this->charset : ''),
			//"Content-Transfer-Encoding" => '8bit',
		));
		$arr = array();
		foreach($this->arHeaders as $key=>$val)
			$arr[] = join(': ', array($key, $val));
		return join($this->eol, $arr);
	}

	/**
	 * Добавить в письмо вложение
	 *
	 * @access public
	 *
	 * @param string $path Путь к файлу, по умолчанию пусто
	 * @param string $name Название файла, по умолчанию пусто
	 * @param string $type Тип файла, по умолчанию пусто
	 *
	 * @return void
	 */
	public function addAttachment($path="", $name = "", $type="")
	{
		//$path = Rel2Abs($_SERVER["DOCUMENT_ROOT"], $path);
		if($aFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$path))
		{
			$filename = $_SERVER["DOCUMENT_ROOT"].$path;
			$handle = fopen($filename, "rb");
			$file = fread($handle, filesize($filename));
			fclose($handle);

			$this->parts[] = array(
				"BODY" => $file,
				"NAME" => !empty($name) ? $name : $aFile["name"],
				"CONTENT_TYPE" => !empty($type) ? $type : $aFile["type"],
			);
		}
	}

	/**
	 * Получить строку с инфомацией о вложениях
	 *
	 * @access public
	 *
	 * @return string
	 */
	public function getAttachmentString()
	{
		$res = "";
		foreach($this->parts as $arPart)
		{
			$ar = array(
				$this->eol.'--'.$this->boundary,
				//'Content-Type: '.$arPart["CONTENT_TYPE"].'; name="'.$arPart["NAME"].'"',
				'Content-Type: application/octet-stream; name="'.$arPart["NAME"].'"',
				'Content-Disposition:attachment; filename="'.basename($arPart["NAME"]).'"',
				'Content-Transfer-Encoding: base64'.$this->eol,
				//'Content-ID: <'.md5($arPart["NAME"]).'>'.$this->eol,
				chunk_split(base64_encode($arPart["BODY"])).$this->eol
			);
			$res .= join($this->eol, $ar);
		}
		if(!empty($res))
			$res .= $this->eol.'--'.$this->boundary.'--'.$this->eol;
		return $res;
	}

	/**
	 * Получить сообщение (вместе с вложениями)
	 *
	 * @access public
	 *
	 * @return string
	 */
	public function getMessageString()
	{
		$message = $this->getMessage();
		$sAttach = $this->getAttachmentString();
		if(!empty($sAttach))
			$message =
				"--".$this->boundary.$this->eol.
				"Content-Type: ".(in_array($this->messageType, array("html", "text/html")) ? "text/html" : "text/plain").(!empty($this->charset) ? '; charset='.$this->charset : '').$this->eol.
				"Content-Transfer-Encoding: 8bit".$this->eol.$this->eol.
				$message.$this->eol.
				$sAttach;
		return $message;
	}

	/**
	 * Отправить почтовое сообщнение
	 *
	 * @access public
	 *
	 * @param string $to E-mail, на который необходимо отправить посьмо
	 * @param string $from E-mail - от кого, по умолчанию пусто
	 * @param string $subject Тема сообщения, по умолчанию пусто
	 * @param string $additional_headers Дополнительные параметры, по умолчанию пусто
	 *
	 * @return bool Возвращает при успешной отправке true, при ошибке false
	 */
	public function send($to, $from="", $subject="", $additional_headers="")
	{
		if(!empty($additional_headers))
						$this->addHeadersString($additional_headers);
		$this->addHeader('From', $from);

		return @mail($to, $subject, $this->getMessageString(), $this->getHeadersString());
	}

	/**
	 * Получить теги вложений
	 *
	 * @access public
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * CDFAMail::getAttachmentTags();
	 * </pre>
	 */
	public static function getAttachmentTags()
	{
		return COption::GetOptionString('defa.tools', 'attachment_tags', '[FILE]');
	}

	/**
	 * Получить тег вложений
	 *
	 * @access public
	 *
	 * @return string
	 *
	 * @example
	 * <pre>
	 * CDFAMail::getAttachmentTag();
	 * </pre>
	 */
	public static function getAttachmentTag()
	{
		$ar = explode(',', self::getAttachmentTags());
		return $ar[0];
	}
}
?>
