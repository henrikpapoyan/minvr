<?
/**
* Base data storage class
* @package storage
* @author ivanko
*/
class CDFABaseEntityStorage implements IDFADataStorage
{
	private $manager;

	public function __construct($param, $bUseDatabase=false)
	{
		if(is_object($param))
		{
			if(!($param instanceof IDFADataStorageManager))
				throw new InvalidArgumentException("data storage must be instance of IDFADataStorageManager");
			$this->manager = $param;
		}
		else
		{
			if($bUseDatabase)
				$this->manager = new CDFAEntityStorageManager($param);
			else
				$this->manager = new CDFASessionStorageManager($param);
		}
	}

	public function getUserID()
	{
		if(CModule::IncludeModule("statistic"))
		{
			return intval($_SESSION["SESS_GUEST_ID"]);
		}
		elseif (is_set($_SESSION, "fixed_session_id"))
		{
			return $_SESSION["fixed_session_id"];
		}
		throw new Exception("unsupport action");
	}

	public function setValue($name, $value, $category=false)
	{
		$arFilter = array("NAME" => $name, "USER_ID" => $this->getUserID());
		if(false != $category)
			$arFilter["CATEGORY"] = $category;

		$arFields = array_merge($arFilter, array("VALUE" => $value));

		$rs = $this->manager->GetList($arFilter);
		if($ar = $rs->Fetch())
		{
			if(null === $value)
				return $this->manager->Delete($ar["ID"]);

			$this->manager->Update($ar["ID"], $arFields);
			return $ar["ID"];
		}
		if(null != $value)
			return $this->manager->Add($arFields);

		return true;
	}

	public function getValue($name, $category=false)
	{
		$result = null;
		$rs = $this->manager->GetList(array("NAME" => $name, "USER_ID" => $this->getUserID(), "CATEGORY" => $category));
		if($ar = $rs->Fetch())
			$result = $ar["VALUE"];
		return $result;
	}

	public function GetList($arFilter)
	{
		return $this->manager->GetList(array_merge($arFilter, array("USER_ID" => $this->getUserID())));
	}
}
?>