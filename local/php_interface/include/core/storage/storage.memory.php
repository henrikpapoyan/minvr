<?
class CDFAMemoryParams
{
	protected
		$sSessParamsKey,
		$sExpiresTimeKey,
		$nParamsTime;

	public function __construct($arParams=array())
	{
		$this->sSessParamsKey = isset($arParams["SESS_KEY"]) && strlen($arParams["SESS_KEY"]) ? $arParams["SESS_KEY"] : 'MEMORY_PARAMS';
		$this->sExpiresTimeKey = isset($arParams["EXPIRE_KEY"]) && strlen($arParams["EXPIRE_KEY"]) ? $arParams["EXPIRE_KEY"] : 'TIME_RESERVED';
		$this->nParamsTime = isset($arParams["TIME"]) ? intval($arParams["TIME"]) : 0;

		foreach($this->_GetParamsKeys() as $sName)
		{
			if(
				!isset($_SESSION[$this->sSessParamsKey][$sName])
				&& $mixValue = $this->_GetCookie($sName)
			)
				$_SESSION[$this->sSessParamsKey][$sName] = $mixValue;
		}

		$nExpires = $this->_Get($this->sExpiresTimeKey);
		if($nExpires && $nExpires < time())
			$this->Clear();
	}

	protected function _GetParamsKeys()
	{
		return array($this->sExpiresTimeKey);
	}

	protected function _GetExpiresTime()
	{
		if(!$nExpires = $this->_Get($this->sExpiresTimeKey))
		{
			$nExpires = ($this->nParamsTime)
				? (time() + $this->nParamsTime)
				: (time() + 60 * 60 * 24 * 365 * 10);
			$this->_Set($this->sExpiresTimeKey, $nExpires, $nExpires);
		}

		return (int) $nExpires;
	}

	protected function _GetCookie($sName)
	{
		if(!isset($GLOBALS["APPLICATION"]) || empty($GLOBALS["APPLICATION"]))
			$GLOBALS["APPLICATION"] = new CMain();

		$mixValue = $GLOBALS["APPLICATION"]->get_cookie($sName);
		
		$bArray = unserialize(gzInflate(base64_decode($mixValue))) !== false;
		return $bArray ? unserialize(gzInflate(base64_decode($mixValue))) : $mixValue;
	}

	protected function _SetCookie($sName, $mixValue, $nExpired = false)
	{
		if(!isset($GLOBALS["APPLICATION"]) || empty($GLOBALS["APPLICATION"]))
			$GLOBALS["APPLICATION"] = new CMain();

		$mixValue = is_array($mixValue) || is_object($mixValue) ? base64_encode(gzDeflate(serialize($mixValue))) : $mixValue; 
		return $GLOBALS["APPLICATION"]->set_cookie($sName, $mixValue, ($nExpired ? $nExpired : $this->_GetExpiresTime()));
	}

	protected function _Get($sName)
	{
		if (isset($_SESSION[$this->sSessParamsKey][$sName]))
			return $_SESSION[$this->sSessParamsKey][$sName]; 
		if($sReturn = $this->_GetCookie($sName))
			return $sReturn;
		return false;
	}

	protected function _Set($sName, $mixValue, $nExpired = false)
	{
		$_SESSION[$this->sSessParamsKey][$sName] = $mixValue;
		$this->_SetCookie($sName, $mixValue, $nExpired);
	}

	public function Get($mixName = false)
	{
		if(!$mixName)
			$mixName = $this->_GetParamsKeys();

		if(is_array($mixName))
		{
			$arReturn = array();
			foreach($mixName as $sName)
				$arReturn[$sName] = $this->_Get($sName);
			return $arReturn;
		}

		return $this->_Get($mixName);
	}

	public function Set($mixName=false, $mixValue='')
	{
		if(!$mixName)
			return false;

		if(is_array($mixName))
		{
			foreach($mixName as $sName => $mixValue)
				$this->_Set($sName, $mixValue);
		}
		else
			$this->_Set($mixName, $mixValue);

		return true;
	}

	public function Clear($mixName=false)
	{
		if(!$mixName)
			$mixName = $this->_GetParamsKeys();

		if(!is_array($mixName))
			$mixName = array($mixName);

		foreach($mixName as $sName)
		{
			if(isset($_SESSION[$this->sSessParamsKey][$sName]))
				unset($_SESSION[$this->sSessParamsKey][$sName]);
				
			$this->_SetCookie($sName, 0, (time() - 100));
		}
	}
}
?>