<?
/**
 * session user storage implementation
 * @package storage
 * @author ivanko
 */
class CDFASessionStorageManager implements IDFADataStorageManager
{
	private $data;

	public function __construct($uid)
	{
		$this->data = &$_SESSION[__CLASS__][$uid];
		if(!is_array($this->data))
			$this->data = array();
	}

	private function CheckFields(&$arFields, $action, $id=null)
	{
		$arErrors = array();

		if((is_set($arFields, "USER_ID") || $action == "ADD") && !intval($arFields["USER_ID"]))
			$arErrors[] = array(
				"id"	=>	"invalid_user_id",
				"text"	=>	"invalid user id"
			);

		if((is_set($arFields, "NAME") || $action == "ADD") && !strlen($arFields["NAME"]))
			$arErrors[] = array(
				"id"	=>	"invalid_name",
				"text"	=>	"invalid entry name"
			);

		if((is_set($arFields, "VALUE") || $action == "ADD") && empty($arFields["VALUE"]))
			$arErrors[] = array(
				"id"	=>	"invalid_value",
				"text"	=>	"invalid value"
			);

		if($action == "UPDATE" && (!is_numeric($id) || !isset($this->data[$id - 1])))
			$arErrors[] = array(
				"id"	=>	"entiry_not_found",
				"text"	=>	"entiry not found"
			);
		
		if(!empty($aMsg))
		{
			$e = new CAdminException(array_reverse($arErrors));
			$GLOBALS["APPLICATION"]->ThrowException($e);
			return false;
		}
		return true;
	}

	private function CompareEntry($arFields, $arFilter)
	{
		$result = true;
		foreach($arFilter as $key => $val)
		{
			if(is_set($arFields, $key))
			{
				if(is_array($val))
				{
					if(!is_array($arFields[$key]))
						$arFields[$key] = array($arFields[$key]);
					if(count($val) > 0 && count(array_intersect($arFields[$key], $val)) <= 0)
						$result = false;
				}
				else
				{
					if($arFields[$key] !== $val)
						$result = false;
				}
			}
		}
		return $result;
	}

	public function GetList($arFilter=array())
	{
		$res = array();
		foreach($this->data as $id => $arFields)
		{
			if(is_array($arFields))
			{
				$arFields["ID"] = $id + 1;
				if($this->CompareEntry($arFields, $arFilter))
					$res[] = $arFields;
			}
		}
		$result = new CDBResult();
		$result->InitFromArray($res);
		return $result;
	}

	public function Add(array $arFields)
	{
		if($this->CheckFields($arFields, "ADD"))
		{
			$this->data[] = $arFields;
			return count($this->data);
		}
		return false;
	}

	public function Update($ID, array $arFields)
	{
		if($this->CheckFields($arFields, "UPDATE", $ID))
		{
			$this->data[$ID - 1] = $arFields;
			return $ID;
		}
		return false;
	}

	public function Delete($id)
	{
		$id -= 1;
		if(is_set($this->data, $id))
		{
			$this->data[$id] = null;
			return true;
		}
		return false;
	}
}
?>