<?
/**
 * entity user storage implementation
 * @package storage
 * @author ivanko
 */
class CDFAEntityStorageManager implements IDFADataStorageManager
{
	protected $manager;

	public function __construct($iblock_id)
	{
		$this->manager = new CDFAEntityManager(
			$iblock_id,
			array(
				"USER_ID"	=>	"CODE",
				"NAME"		=>	"NAME",
				"VALUE"		=>	"PREVIEW_TEXT",
				"CATEGORY"	=>	"DETAIL_TEXT"
			)
		);
	}

	public function GetList($arFilter=array())
	{
		return $this->manager->GetList(array(), $arFilter);
	}

	public function Add(array $arFields)
	{
		return $this->manager->Add($arFields);
	}

	public function Update($ID, array $arFields)
	{
		return $this->manager->Update($ID, $arFields);
	}

	public function Delete($id)
	{
		return $this->manager->Delete($id);
	}
}
?>