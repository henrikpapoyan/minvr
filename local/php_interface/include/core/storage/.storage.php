<?
/**
 * Base user data storage interface
 * @package storage
 * @author ivanko
 */
interface IDFADataStorage
{
	/**
	 * gets current user uid
	 * @return int
	 */
	public function getUserID();
	
	/**
	 * save key => value pair
	 * @param string $name
	 * @param mixed $value
	 * @param string $category
	 */
	public function setValue($name, $value, $category=false);
	
	/**
	 * gets stored user data
	 * @param string $name
	 * @param string $category
	 */
	public function getValue($name, $category=false);

	/**
	 * gets user stored data
	 * @param array $arFilter
	 * @return CDBResult
	 */
	public function GetList($arFilter);
}

interface IDFADataStorageManager
{
	public function Add(array $arFields);
	
	public function Update($ID, array $arFields);
	
	/**
	 * gets stored data
	 * @param array $arFilter
	 * @return CDBResult
	 */
	public function GetList($arFilter);
	
	public function Delete($ID);
}
?>