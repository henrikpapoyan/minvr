<? 
require_once(".storage.php");
CDFAIncludeManager::AddAutoloadClasses(__DIR__, array(
	"CDFAMemoryParams" => "storage.memory.php",
	"CDFABaseEntityStorage" => "storage.php",
	"CDFAEntityStorageManager" => "storage.manager.entity.php",
	"CDFASessionStorageManager" => "storage.manager.session.php",
));
?>