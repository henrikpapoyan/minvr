<?php
class imageMagick {

    function __construct($filePath)
    {
        $this->file = $filePath;
        $this->img = new imagick();
    }

    function readFirstPagePDF()
    {
        $this->img = new imagick($_SERVER['DOCUMENT_ROOT'] . $this->file . '[0]');
    }

    function convertToJPG()
    {
        $this->img->setCompression(Imagick::COMPRESSION_JPEG);
        $this->img->setCompressionQuality(60);
        $this->img->setImageFormat('jpeg');
    }

    function saveFile($path)
    {
        $this->img->writeImage($_SERVER['DOCUMENT_ROOT'].$path);
        $this->img->clear();
        $this->img->destroy();
    }
}