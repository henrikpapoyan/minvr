<?
class CARTSiteTemplateTools extends CSiteTemplateTools
{
	private static $instance = null;

	private function __construct()
	{

	}

	public static function getInstance()
	{
		if(self::$instance == null)
			self::$instance = new self();
		return self::$instance;
	}

	public static function GetPrintPageURL()
	{
		return $GLOBALS["APPLICATION"]->GetCurPageParam("print=Y", array("print"));
	}

	public static function IncludeFile($rel_path, $arParams = Array(), $arFunctionParams = Array())
	{
		parent::IncludeFile("includes/".$rel_path, $arParams, array_merge(array("SHOW_BORDER" => false), $arFunctionParams));
	}

	public static function IncludeArea($rel_path, $arParams = Array(), $arFunctionParams = Array())
	{
		self::IncludeFile($rel_path, $arParams, array_merge(array("SHOW_BORDER" => true), $arFunctionParams));
	}

	public function ShowH1()
	{
		$GLOBALS['APPLICATION']->AddBufferContent(array(&$this,'GetH1'));
	}

	public function GetH1()
	{

		if(!CSiteTemplateTools::IsMainPage() && $GLOBALS["APPLICATION"]->GetProperty("SHOW_H1") != "N")
		{
			$title = $GLOBALS["APPLICATION"]->GetTitle();
			
			if($GLOBALS["APPLICATION"]->GetProperty("H1"))
				$title = $GLOBALS["APPLICATION"]->GetProperty("H1");
			
			return "<h1 class=\"site-head\">".$title."</h1>";
			
		}

		return "";
	}

	public function ShowDirTitle()
	{
		$GLOBALS['APPLICATION']->AddBufferContent(array(&$this,'GetDirTitle'));
	}

	public function GetDirTitle()
	{

		do {

			$sSectionName = "";

			$sRealFilePath = $_SERVER["SCRIPT_FILENAME"];

			if (is_set($_SERVER, "REAL_FILE_PATH"))
			{
				$arDirPath = explode("/", dirname($_SERVER["REAL_FILE_PATH"]));
				if(!empty($arDirPath[1]))
					$sRealFilePath = $_SERVER["DOCUMENT_ROOT"]."/".$arDirPath[1]."/";
			}
			elseif (is_set($_SERVER, "SCRIPT_NAME"))
			{
				$arDirPath = explode("/", dirname($_SERVER["SCRIPT_NAME"]));
				if(!empty($arDirPath[1]))
					$sRealFilePath = $_SERVER["DOCUMENT_ROOT"]."/".$arDirPath[1]."/";
			}

			$sSectionFile = $sRealFilePath."/.section.php";

			if (file_exists($sSectionFile))
			{
				include $sSectionFile;

				if (!empty($sSectionName))
				{
					$title = $sSectionName;

					break;
				}
			}

			$title = $GLOBALS["APPLICATION"]->GetTitle();
		}
		while(false);

		return '<div class="title-text">'.$title.'</div>';

	}

	public function ShowClassForBody()
	{
		$GLOBALS["APPLICATION"]->AddBufferContent(array(&$this, "GetClassForBody"));
	}

	public function GetClassForBody()
	{
		$class = $GLOBALS["APPLICATION"]->GetProperty("CLASS_FOR_BODY");

		if (!empty($class))
			return $class;

		return "";
	}


	public function ShowClassForPageContent()
	{
		$GLOBALS["APPLICATION"]->AddBufferContent(array(&$this, "GetClassForPageContent"));
	}

	public function GetClassForPageContent()
	{
		$class = $GLOBALS["APPLICATION"]->GetProperty("CLASS_FOR_PAGE_CONTENT");

		if (!empty($class))
			return $class;

		return "";
	}
}

?>